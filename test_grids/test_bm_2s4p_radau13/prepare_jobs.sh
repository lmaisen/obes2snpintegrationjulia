#!/bin/bash
mkdir -p /cobra/u/lmaisen/Julia/GridResults/test_bm_2s4p_radau13/Jobs
mkdir -p /cobra/u/lmaisen/Julia/GridResults/test_bm_2s4p_radau13/Lines
mkdir -p /cobra/u/lmaisen/Julia/GridResults/test_bm_2s4p_radau13/Logs
mkdir -p /cobra/u/lmaisen/Julia/GridResults/test_bm_2s4p_radau13/Source
mkdir -p /cobra/u/lmaisen/Julia/GridResults/test_bm_2s4p_radau13/Metadata
mkdir -p /cobra/u/lmaisen/Julia/GridResults/test_bm_2s4p_radau13/Scripts
mkdir -p /cobra/u/lmaisen/Julia/GridResults/test_bm_2s4p_radau13/Stats
mkdir -p /cobra/u/lmaisen/Julia/GridResults/test_bm_2s4p_radau13/OBE
zip -r -q /cobra/u/lmaisen/Julia/GridResults/test_bm_2s4p_radau13/Source/obes2snpintegrationjulia.zip /cobra/u/lmaisen/Julia/obes2snpintegrationjulia -x '*.git*'
git -C /cobra/u/lmaisen/Julia/obes2snpintegrationjulia/.git rev-parse HEAD > /cobra/u/lmaisen/Julia/GridResults/test_bm_2s4p_radau13/Source/obes2snpintegrationjulia.sha
cp -R /cobra/u/lmaisen/Julia/obes2snpintegrationjulia/H2SnPOBEs/src/OBEs/bm2s4p12CDon/* /cobra/u/lmaisen/Julia/GridResults/test_bm_2s4p_radau13/OBE
chmod 777 2022-06-27-02-50-47_post.sh
chmod 777 submit_jobs.cmd
cp Jobs/job_1.job /cobra/u/lmaisen/Julia/GridResults/test_bm_2s4p_radau13/Jobs/job_1.job
cp Metadata/test_bm_2s4p_radau13_grids.dat /cobra/u/lmaisen/Julia/GridResults/test_bm_2s4p_radau13/Metadata/test_bm_2s4p_radau13_grids.dat
cp Metadata/test_bm_2s4p_radau13_scans.dat /cobra/u/lmaisen/Julia/GridResults/test_bm_2s4p_radau13/Metadata/test_bm_2s4p_radau13_scans.dat
cp Scripts/build_jobs.py /cobra/u/lmaisen/Julia/GridResults/test_bm_2s4p_radau13/Scripts/build_jobs.py
./submit_jobs.cmd

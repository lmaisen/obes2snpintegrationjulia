# OBE DMS (discrete momentum space) equations for 2S-6P1/2 with cross-damping terms
# Using hydrogen energies from Horbatsch2016 and fundamental constants from CODATA2018 (to convert intensity)
# Assuming two laser beams with collinear linear polarizations
# Rabi frequency: Real parts OmRp0, OmRm0 and imaginary parts OmIp0, OmIm0 for beam with positive, negative propagation direction, respectively, with dimension of intensity^(1/2) and unit (W/m^2)^(1/2)
# x represents time t (According to Numerical Recipes)

import numpy as np

#OmR1 = -397496.07535679*OmRm0
#OmI1 = -397496.07535679*OmIm0
#OmR2 = -397496.07535679*OmRp0
#OmI2 = -397496.07535679*OmIp0
#OmR3 = -397496.07535679*OmRp0
#OmI3 = -397496.07535679*OmIp0
#OmR4 = -397496.07535679*OmRm0
#OmI4 = -397496.07535679*OmIm0
#OmR5 = -397496.07535679*OmRp0
#OmI5 = -397496.07535679*OmIp0
#OmR6 = -397496.07535679*OmRm0
#OmI6 = -397496.07535679*OmIm0
#OmR7 = -397496.07535679*OmRm0
#OmI7 = -397496.07535679*OmIm0
#OmR8 = -397496.07535679*OmRp0
#OmI8 = -397496.07535679*OmIp0
#OmR9 = -397496.07535679*OmRm0
#OmI9 = -397496.07535679*OmIm0
#OmR10 = -397496.07535679*OmRp0
#OmI10 = -397496.07535679*OmIp0
#OmR11 = -397496.07535679*OmRp0
#OmI11 = -397496.07535679*OmIp0
#OmR12 = -397496.07535679*OmRm0
#OmI12 = -397496.07535679*OmIm0

#cdef np.ndarray[DTYPE_t, ndim=1] dydt=np.zeros(94, dtype=DTYPE)
#dydt[0] = -(OmR1*y[20]) + OmI1*y[21] - OmR2*y[22] + OmI2*y[23]
#dydt[1] = -2.44928427874793e7*y[1] + OmR3*y[28] + OmI3*y[29] + OmR7*y[30] + OmI7*y[31]
#dydt[2] = 2.44928427874793e7*y[1] + 2.44928427874793e7*y[3]
#dydt[3] = -2.44928427874793e7*y[3] + OmR5*y[40] + OmI5*y[41] + OmR9*y[42] + OmI9*y[43]
#dydt[4] = -(OmR3*y[28]) - OmI3*y[29] - OmR4*y[50] + OmI4*y[51] + 764399.758100854*y[5]
#dydt[5] = OmR1*y[20] - OmI1*y[21] + OmR11*y[56] + OmI11*y[57] - 2.44928427874793e7*y[5]
#dydt[6] = 2.33286062491707e7*y[5]
#dydt[7] = -(OmR5*y[40]) - OmI5*y[41] + 399836.78020768*y[5] - OmR6*y[64] + OmI6*y[65]
#dydt[8] = -(OmR7*y[30]) - OmI7*y[31] - OmR8*y[70] + OmI8*y[71] + 764399.758100854*y[9]
#dydt[9] = OmR2*y[22] - OmI2*y[23] + OmR12*y[74] + OmI12*y[75] - 2.44928427874793e7*y[9]
#dydt[10] = 2.33286062491707e7*y[9]
#dydt[11] = -(OmR9*y[42]) - OmI9*y[43] - OmR10*y[78] + OmI10*y[79] + 399836.78020768*y[9]
#dydt[12] = -(OmR11*y[56]) - OmI11*y[57]
#dydt[13] = -2.44928427874793e7*y[13] + OmR4*y[50] - OmI4*y[51]
#dydt[14] = 2.44928427874793e7*y[13] + 2.44928427874793e7*y[15]
#dydt[15] = -2.44928427874793e7*y[15] + OmR6*y[64] - OmI6*y[65]
#dydt[16] = -(OmR12*y[74]) - OmI12*y[75]
#dydt[17] = -2.44928427874793e7*y[17] + OmR8*y[70] - OmI8*y[71]
#dydt[18] = 2.44928427874793e7*y[17] + 2.44928427874793e7*y[19]
#dydt[19] = -2.44928427874793e7*y[19] + OmR10*y[78] - OmI10*y[79]
#dydt[20] = (OmR1*y[0])/2. - 1.22464213937396e7*y[20] + Delta*y[21] - BetaK*VrecSqr*y[21] + 2*BetaK*Vrec*vx*y[21] + (OmR11*y[24])/2. + (OmI11*y[25])/2. - (OmR2*y[54])/2. + (OmI2*y[55])/2. - (OmR1*y[5])/2.
#dydt[21] = -0.5*(OmI1*y[0]) - Delta*y[20] + BetaK*VrecSqr*y[20] - 2*BetaK*Vrec*vx*y[20] - 1.22464213937396e7*y[21] - (OmI11*y[24])/2. + (OmR11*y[25])/2. + (OmI2*y[54])/2. + (OmR2*y[55])/2. + (OmI1*y[5])/2.
#dydt[22] = (OmR2*y[0])/2. - 1.22464213937396e7*y[22] + Delta*y[23] - BetaK*VrecSqr*y[23] - 2*BetaK*Vrec*vx*y[23] + (OmR12*y[26])/2. + (OmI12*y[27])/2. - (OmR1*y[54])/2. - (OmI1*y[55])/2. - (OmR2*y[9])/2.
#dydt[23] = -0.5*(OmI2*y[0]) - Delta*y[22] + BetaK*VrecSqr*y[22] + 2*BetaK*Vrec*vx*y[22] - 1.22464213937396e7*y[23] - (OmI12*y[26])/2. + (OmR12*y[27])/2. + (OmI1*y[54])/2. - (OmR1*y[55])/2. + (OmI2*y[9])/2.
#dydt[24] = -0.5*(OmR11*y[20]) + (OmI11*y[21])/2. - 4*BetaK*VrecSqr*y[25] + 4*BetaK*Vrec*vx*y[25] - (OmR1*y[56])/2. - (OmI1*y[57])/2. - (OmR2*y[72])/2. - (OmI2*y[73])/2.
#dydt[25] = -0.5*(OmI11*y[20]) - (OmR11*y[21])/2. + 4*BetaK*VrecSqr*y[24] - 4*BetaK*Vrec*vx*y[24] + (OmI1*y[56])/2. - (OmR1*y[57])/2. + (OmI2*y[72])/2. - (OmR2*y[73])/2.
#dydt[26] = -0.5*(OmR12*y[22]) + (OmI12*y[23])/2. - 4*BetaK*VrecSqr*y[27] - 4*BetaK*Vrec*vx*y[27] - (OmR1*y[58])/2. - (OmI1*y[59])/2. - (OmR2*y[74])/2. - (OmI2*y[75])/2.
#dydt[27] = -0.5*(OmI12*y[22]) - (OmR12*y[23])/2. + 4*BetaK*VrecSqr*y[26] + 4*BetaK*Vrec*vx*y[26] + (OmI1*y[58])/2. - (OmR1*y[59])/2. + (OmI2*y[74])/2. - (OmR2*y[75])/2.
#dydt[28] = -0.5*(OmR3*y[1]) - 1.22464213937396e7*y[28] - Delta*y[29] - BetaK*VrecSqr*y[29] + 2*BetaK*dpd1*VrecSqr*y[29] + 2*BetaK*Vrec*vx*y[29] - (OmR4*y[32])/2. + (OmI4*y[33])/2. + (OmR7*y[48])/2. + (OmI7*y[49])/2. + (OmR3*y[4])/2.
#dydt[29] = -0.5*(OmI3*y[1]) + Delta*y[28] + BetaK*VrecSqr*y[28] - 2*BetaK*dpd1*VrecSqr*y[28] - 2*BetaK*Vrec*vx*y[28] - 1.22464213937396e7*y[29] - (OmI4*y[32])/2. - (OmR4*y[33])/2. + (OmI7*y[48])/2. - (OmR7*y[49])/2. + (OmI3*y[4])/2.
#dydt[30] = -0.5*(OmR7*y[1]) - 1.22464213937396e7*y[30] - Delta*y[31] - BetaK*VrecSqr*y[31] - 2*BetaK*dpd1*VrecSqr*y[31] - 2*BetaK*Vrec*vx*y[31] - (OmR8*y[34])/2. + (OmI8*y[35])/2. + (OmR3*y[48])/2. - (OmI3*y[49])/2. + (OmR7*y[8])/2.
#dydt[31] = -0.5*(OmI7*y[1]) + Delta*y[30] + BetaK*VrecSqr*y[30] + 2*BetaK*dpd1*VrecSqr*y[30] + 2*BetaK*Vrec*vx*y[30] - 1.22464213937396e7*y[31] - (OmI8*y[34])/2. - (OmR8*y[35])/2. + (OmI3*y[48])/2. + (OmR3*y[49])/2. + (OmI7*y[8])/2.
#dydt[32] = (OmR4*y[28])/2. + (OmI4*y[29])/2. - 2.44928427874793e7*y[32] - 4*BetaK*VrecSqr*y[33] + 4*BetaK*dpd1*VrecSqr*y[33] + 4*BetaK*Vrec*vx*y[33] + (OmR3*y[50])/2. - (OmI3*y[51])/2. + (OmR7*y[68])/2. - (OmI7*y[69])/2.
#dydt[33] = -0.5*(OmI4*y[28]) + (OmR4*y[29])/2. + 4*BetaK*VrecSqr*y[32] - 4*BetaK*dpd1*VrecSqr*y[32] - 4*BetaK*Vrec*vx*y[32] - 2.44928427874793e7*y[33] + (OmI3*y[50])/2. + (OmR3*y[51])/2. + (OmI7*y[68])/2. + (OmR7*y[69])/2.
#dydt[34] = (OmR8*y[30])/2. + (OmI8*y[31])/2. - 2.44928427874793e7*y[34] - 4*BetaK*VrecSqr*y[35] - 4*BetaK*dpd1*VrecSqr*y[35] - 4*BetaK*Vrec*vx*y[35] + (OmR3*y[52])/2. - (OmI3*y[53])/2. + (OmR7*y[70])/2. - (OmI7*y[71])/2.
#dydt[35] = -0.5*(OmI8*y[30]) + (OmR8*y[31])/2. + 4*BetaK*VrecSqr*y[34] + 4*BetaK*dpd1*VrecSqr*y[34] + 4*BetaK*Vrec*vx*y[34] - 2.44928427874793e7*y[35] + (OmI3*y[52])/2. + (OmR3*y[53])/2. + (OmI7*y[70])/2. + (OmR7*y[71])/2.
#dydt[36] = 2.44928427874793e7*y[32] - 4*BetaK*VrecSqr*y[37] + 4*BetaK*Vrec*vx*y[37] + 2.44928427874793e7*y[44]
#dydt[37] = 2.44928427874793e7*y[33] + 4*BetaK*VrecSqr*y[36] - 4*BetaK*Vrec*vx*y[36] + 2.44928427874793e7*y[45]
#dydt[38] = 2.44928427874793e7*y[34] - 4*BetaK*VrecSqr*y[39] - 4*BetaK*Vrec*vx*y[39] + 2.44928427874793e7*y[46]
#dydt[39] = 2.44928427874793e7*y[35] + 4*BetaK*VrecSqr*y[38] + 4*BetaK*Vrec*vx*y[38] + 2.44928427874793e7*y[47]
#dydt[40] = -0.5*(OmR5*y[3]) - 1.22464213937396e7*y[40] - Delta*y[41] - BetaK*VrecSqr*y[41] + 2*BetaK*dpd2*VrecSqr*y[41] + 2*BetaK*Vrec*vx*y[41] - (OmR6*y[44])/2. + (OmI6*y[45])/2. + (OmR9*y[62])/2. + (OmI9*y[63])/2. + (OmR5*y[7])/2.
#dydt[41] = -0.5*(OmI5*y[3]) + Delta*y[40] + BetaK*VrecSqr*y[40] - 2*BetaK*dpd2*VrecSqr*y[40] - 2*BetaK*Vrec*vx*y[40] - 1.22464213937396e7*y[41] - (OmI6*y[44])/2. - (OmR6*y[45])/2. + (OmI9*y[62])/2. - (OmR9*y[63])/2. + (OmI5*y[7])/2.
#dydt[42] = (OmR9*y[11])/2. - (OmR9*y[3])/2. - 1.22464213937396e7*y[42] - Delta*y[43] - BetaK*VrecSqr*y[43] - 2*BetaK*dpd2*VrecSqr*y[43] - 2*BetaK*Vrec*vx*y[43] - (OmR10*y[46])/2. + (OmI10*y[47])/2. + (OmR5*y[62])/2. - (OmI5*y[63])/2.
#dydt[43] = (OmI9*y[11])/2. - (OmI9*y[3])/2. + Delta*y[42] + BetaK*VrecSqr*y[42] + 2*BetaK*dpd2*VrecSqr*y[42] + 2*BetaK*Vrec*vx*y[42] - 1.22464213937396e7*y[43] - (OmI10*y[46])/2. - (OmR10*y[47])/2. + (OmI5*y[62])/2. + (OmR5*y[63])/2.
#dydt[44] = (OmR6*y[40])/2. + (OmI6*y[41])/2. - 2.44928427874793e7*y[44] - 4*BetaK*VrecSqr*y[45] + 4*BetaK*dpd2*VrecSqr*y[45] + 4*BetaK*Vrec*vx*y[45] + (OmR5*y[64])/2. - (OmI5*y[65])/2. + (OmR9*y[76])/2. - (OmI9*y[77])/2.
#dydt[45] = -0.5*(OmI6*y[40]) + (OmR6*y[41])/2. + 4*BetaK*VrecSqr*y[44] - 4*BetaK*dpd2*VrecSqr*y[44] - 4*BetaK*Vrec*vx*y[44] - 2.44928427874793e7*y[45] + (OmI5*y[64])/2. + (OmR5*y[65])/2. + (OmI9*y[76])/2. + (OmR9*y[77])/2.
#dydt[46] = (OmR10*y[42])/2. + (OmI10*y[43])/2. - 2.44928427874793e7*y[46] - 4*BetaK*VrecSqr*y[47] - 4*BetaK*dpd2*VrecSqr*y[47] - 4*BetaK*Vrec*vx*y[47] + (OmR5*y[66])/2. - (OmI5*y[67])/2. + (OmR9*y[78])/2. - (OmI9*y[79])/2.
#dydt[47] = -0.5*(OmI10*y[42]) + (OmR10*y[43])/2. + 4*BetaK*VrecSqr*y[46] + 4*BetaK*dpd2*VrecSqr*y[46] + 4*BetaK*Vrec*vx*y[46] - 2.44928427874793e7*y[47] + (OmI5*y[66])/2. + (OmR5*y[67])/2. + (OmI9*y[78])/2. + (OmR9*y[79])/2.
#dydt[48] = -0.5*(OmR7*y[28]) - (OmI7*y[29])/2. - (OmR3*y[30])/2. - (OmI3*y[31])/2. - 4*BetaK*dpd1*VrecSqr*y[49] - 4*BetaK*Vrec*vx*y[49] - (OmR8*y[52])/2. + (OmI8*y[53])/2. + 764399.758100854*y[54] - (OmR4*y[68])/2. + (OmI4*y[69])/2.
#dydt[49] = -0.5*(OmI7*y[28]) + (OmR7*y[29])/2. + (OmI3*y[30])/2. - (OmR3*y[31])/2. + 4*BetaK*dpd1*VrecSqr*y[48] + 4*BetaK*Vrec*vx*y[48] - (OmI8*y[52])/2. - (OmR8*y[53])/2. + 764399.758100854*y[55] + (OmI4*y[68])/2. + (OmR4*y[69])/2.
#dydt[50] = -0.5*(OmR4*y[13]) - (OmR3*y[32])/2. - (OmI3*y[33])/2. + (OmR4*y[4])/2. - 1.22464213937396e7*y[50] + Delta*y[51] - 3*BetaK*VrecSqr*y[51] + 2*BetaK*dpd1*VrecSqr*y[51] + 2*BetaK*Vrec*vx*y[51]
#dydt[51] = (OmI4*y[13])/2. + (OmI3*y[32])/2. - (OmR3*y[33])/2. - (OmI4*y[4])/2. - Delta*y[50] + 3*BetaK*VrecSqr*y[50] - 2*BetaK*dpd1*VrecSqr*y[50] - 2*BetaK*Vrec*vx*y[50] - 1.22464213937396e7*y[51]
#dydt[52] = -0.5*(OmR3*y[34]) - (OmI3*y[35])/2. + (OmR8*y[48])/2. + (OmI8*y[49])/2. - 1.22464213937396e7*y[52] + Delta*y[53] - 3*BetaK*VrecSqr*y[53] - 6*BetaK*dpd1*VrecSqr*y[53] - 6*BetaK*Vrec*vx*y[53] - (OmR4*y[82])/2. - (OmI4*y[83])/2.
#dydt[53] = (OmI3*y[34])/2. - (OmR3*y[35])/2. - (OmI8*y[48])/2. + (OmR8*y[49])/2. - Delta*y[52] + 3*BetaK*VrecSqr*y[52] + 6*BetaK*dpd1*VrecSqr*y[52] + 6*BetaK*Vrec*vx*y[52] - 1.22464213937396e7*y[53] + (OmI4*y[82])/2. - (OmR4*y[83])/2.
#dydt[54] = (OmR2*y[20])/2. - (OmI2*y[21])/2. + (OmR1*y[22])/2. - (OmI1*y[23])/2. - 2.44928427874793e7*y[54] - 4*BetaK*Vrec*vx*y[55] + (OmR12*y[58])/2. + (OmI12*y[59])/2. + (OmR11*y[72])/2. + (OmI11*y[73])/2.
#dydt[55] = -0.5*(OmI2*y[20]) - (OmR2*y[21])/2. + (OmI1*y[22])/2. + (OmR1*y[23])/2. + 4*BetaK*Vrec*vx*y[54] - 2.44928427874793e7*y[55] - (OmI12*y[58])/2. + (OmR12*y[59])/2. + (OmI11*y[72])/2. - (OmR11*y[73])/2.
#dydt[56] = (OmR11*y[12])/2. + (OmR1*y[24])/2. - (OmI1*y[25])/2. - 1.22464213937396e7*y[56] - Delta*y[57] - 3*BetaK*VrecSqr*y[57] + 2*BetaK*Vrec*vx*y[57] - (OmR11*y[5])/2.
#dydt[57] = (OmI11*y[12])/2. + (OmI1*y[24])/2. + (OmR1*y[25])/2. + Delta*y[56] + 3*BetaK*VrecSqr*y[56] - 2*BetaK*Vrec*vx*y[56] - 1.22464213937396e7*y[57] - (OmI11*y[5])/2.
#dydt[58] = (OmR1*y[26])/2. - (OmI1*y[27])/2. - (OmR12*y[54])/2. + (OmI12*y[55])/2. - 1.22464213937396e7*y[58] - Delta*y[59] - 3*BetaK*VrecSqr*y[59] - 6*BetaK*Vrec*vx*y[59] + (OmR11*y[80])/2. - (OmI11*y[81])/2.
#dydt[59] = (OmI1*y[26])/2. + (OmR1*y[27])/2. - (OmI12*y[54])/2. - (OmR12*y[55])/2. + Delta*y[58] + 3*BetaK*VrecSqr*y[58] + 6*BetaK*Vrec*vx*y[58] - 1.22464213937396e7*y[59] + (OmI11*y[80])/2. + (OmR11*y[81])/2.
#dydt[60] = 2.33286062491707e7*y[54] - 4*BetaK*dpd1*VrecSqr*y[61] - 4*BetaK*Vrec*vx*y[61]
#dydt[61] = 2.33286062491707e7*y[55] + 4*BetaK*dpd1*VrecSqr*y[60] + 4*BetaK*Vrec*vx*y[60]
#dydt[62] = -0.5*(OmR9*y[40]) - (OmI9*y[41])/2. - (OmR5*y[42])/2. - (OmI5*y[43])/2. + 399836.78020768*y[54] - 4*BetaK*dpd2*VrecSqr*y[63] - 4*BetaK*Vrec*vx*y[63] - (OmR10*y[66])/2. + (OmI10*y[67])/2. - (OmR6*y[76])/2. + (OmI6*y[77])/2.
#dydt[63] = -0.5*(OmI9*y[40]) + (OmR9*y[41])/2. + (OmI5*y[42])/2. - (OmR5*y[43])/2. + 399836.78020768*y[55] + 4*BetaK*dpd2*VrecSqr*y[62] + 4*BetaK*Vrec*vx*y[62] - (OmI10*y[66])/2. - (OmR10*y[67])/2. + (OmI6*y[76])/2. + (OmR6*y[77])/2.
#dydt[64] = -0.5*(OmR6*y[15]) - (OmR5*y[44])/2. - (OmI5*y[45])/2. - 1.22464213937396e7*y[64] + Delta*y[65] - 3*BetaK*VrecSqr*y[65] + 2*BetaK*dpd2*VrecSqr*y[65] + 2*BetaK*Vrec*vx*y[65] + (OmR6*y[7])/2.
#dydt[65] = (OmI6*y[15])/2. + (OmI5*y[44])/2. - (OmR5*y[45])/2. - Delta*y[64] + 3*BetaK*VrecSqr*y[64] - 2*BetaK*dpd2*VrecSqr*y[64] - 2*BetaK*Vrec*vx*y[64] - 1.22464213937396e7*y[65] - (OmI6*y[7])/2.
#dydt[66] = -0.5*(OmR5*y[46]) - (OmI5*y[47])/2. + (OmR10*y[62])/2. + (OmI10*y[63])/2. - 1.22464213937396e7*y[66] + Delta*y[67] - 3*BetaK*VrecSqr*y[67] - 6*BetaK*dpd2*VrecSqr*y[67] - 6*BetaK*Vrec*vx*y[67] - (OmR6*y[86])/2. - (OmI6*y[87])/2.
#dydt[67] = (OmI5*y[46])/2. - (OmR5*y[47])/2. - (OmI10*y[62])/2. + (OmR10*y[63])/2. - Delta*y[66] + 3*BetaK*VrecSqr*y[66] + 6*BetaK*dpd2*VrecSqr*y[66] + 6*BetaK*Vrec*vx*y[66] - 1.22464213937396e7*y[67] + (OmI6*y[86])/2. - (OmR6*y[87])/2.
#dydt[68] = -0.5*(OmR7*y[32]) - (OmI7*y[33])/2. + (OmR4*y[48])/2. - (OmI4*y[49])/2. - 1.22464213937396e7*y[68] + Delta*y[69] - 3*BetaK*VrecSqr*y[69] + 6*BetaK*dpd1*VrecSqr*y[69] + 6*BetaK*Vrec*vx*y[69] - (OmR8*y[82])/2. + (OmI8*y[83])/2.
#dydt[69] = (OmI7*y[32])/2. - (OmR7*y[33])/2. - (OmI4*y[48])/2. - (OmR4*y[49])/2. - Delta*y[68] + 3*BetaK*VrecSqr*y[68] - 6*BetaK*dpd1*VrecSqr*y[68] - 6*BetaK*Vrec*vx*y[68] - 1.22464213937396e7*y[69] + (OmI8*y[82])/2. + (OmR8*y[83])/2.
#dydt[70] = -0.5*(OmR8*y[17]) - (OmR7*y[34])/2. - (OmI7*y[35])/2. - 1.22464213937396e7*y[70] + Delta*y[71] - 3*BetaK*VrecSqr*y[71] - 2*BetaK*dpd1*VrecSqr*y[71] - 2*BetaK*Vrec*vx*y[71] + (OmR8*y[8])/2.
#dydt[71] = (OmI8*y[17])/2. + (OmI7*y[34])/2. - (OmR7*y[35])/2. - Delta*y[70] + 3*BetaK*VrecSqr*y[70] + 2*BetaK*dpd1*VrecSqr*y[70] + 2*BetaK*Vrec*vx*y[70] - 1.22464213937396e7*y[71] - (OmI8*y[8])/2.
#dydt[72] = (OmR2*y[24])/2. - (OmI2*y[25])/2. - (OmR11*y[54])/2. - (OmI11*y[55])/2. - 1.22464213937396e7*y[72] - Delta*y[73] - 3*BetaK*VrecSqr*y[73] + 6*BetaK*Vrec*vx*y[73] + (OmR12*y[80])/2. + (OmI12*y[81])/2.
#dydt[73] = (OmI2*y[24])/2. + (OmR2*y[25])/2. - (OmI11*y[54])/2. + (OmR11*y[55])/2. + Delta*y[72] + 3*BetaK*VrecSqr*y[72] - 6*BetaK*Vrec*vx*y[72] - 1.22464213937396e7*y[73] + (OmI12*y[80])/2. - (OmR12*y[81])/2.
#dydt[74] = (OmR12*y[16])/2. + (OmR2*y[26])/2. - (OmI2*y[27])/2. - 1.22464213937396e7*y[74] - Delta*y[75] - 3*BetaK*VrecSqr*y[75] - 2*BetaK*Vrec*vx*y[75] - (OmR12*y[9])/2.
#dydt[75] = (OmI12*y[16])/2. + (OmI2*y[26])/2. + (OmR2*y[27])/2. + Delta*y[74] + 3*BetaK*VrecSqr*y[74] + 2*BetaK*Vrec*vx*y[74] - 1.22464213937396e7*y[75] - (OmI12*y[9])/2.
#dydt[76] = -0.5*(OmR9*y[44]) - (OmI9*y[45])/2. + (OmR6*y[62])/2. - (OmI6*y[63])/2. - 1.22464213937396e7*y[76] + Delta*y[77] - 3*BetaK*VrecSqr*y[77] + 6*BetaK*dpd2*VrecSqr*y[77] + 6*BetaK*Vrec*vx*y[77] - (OmR10*y[86])/2. + (OmI10*y[87])/2.
#dydt[77] = (OmI9*y[44])/2. - (OmR9*y[45])/2. - (OmI6*y[62])/2. - (OmR6*y[63])/2. - Delta*y[76] + 3*BetaK*VrecSqr*y[76] - 6*BetaK*dpd2*VrecSqr*y[76] - 6*BetaK*Vrec*vx*y[76] - 1.22464213937396e7*y[77] + (OmI10*y[86])/2. + (OmR10*y[87])/2.
#dydt[78] = (OmR10*y[11])/2. - (OmR10*y[19])/2. - (OmR9*y[46])/2. - (OmI9*y[47])/2. - 1.22464213937396e7*y[78] + Delta*y[79] - 3*BetaK*VrecSqr*y[79] - 2*BetaK*dpd2*VrecSqr*y[79] - 2*BetaK*Vrec*vx*y[79]
#dydt[79] = -0.5*(OmI10*y[11]) + (OmI10*y[19])/2. + (OmI9*y[46])/2. - (OmR9*y[47])/2. - Delta*y[78] + 3*BetaK*VrecSqr*y[78] + 2*BetaK*dpd2*VrecSqr*y[78] + 2*BetaK*Vrec*vx*y[78] - 1.22464213937396e7*y[79]
#dydt[80] = -0.5*(OmR11*y[58]) - (OmI11*y[59])/2. - (OmR12*y[72])/2. - (OmI12*y[73])/2. - 8*BetaK*Vrec*vx*y[81]
#dydt[81] = (OmI11*y[58])/2. - (OmR11*y[59])/2. - (OmI12*y[72])/2. + (OmR12*y[73])/2. + 8*BetaK*Vrec*vx*y[80]
#dydt[82] = (OmR4*y[52])/2. - (OmI4*y[53])/2. + (OmR8*y[68])/2. - (OmI8*y[69])/2. - 2.44928427874793e7*y[82] - 8*BetaK*dpd1*VrecSqr*y[83] - 8*BetaK*Vrec*vx*y[83]
#dydt[83] = (OmI4*y[52])/2. + (OmR4*y[53])/2. - (OmI8*y[68])/2. - (OmR8*y[69])/2. + 8*BetaK*dpd1*VrecSqr*y[82] + 8*BetaK*Vrec*vx*y[82] - 2.44928427874793e7*y[83]
#dydt[84] = 2.44928427874793e7*y[82] - 8*BetaK*Vrec*vx*y[85] + 2.44928427874793e7*y[86]
#dydt[85] = 2.44928427874793e7*y[83] + 8*BetaK*Vrec*vx*y[84] + 2.44928427874793e7*y[87]
#dydt[86] = (OmR6*y[66])/2. - (OmI6*y[67])/2. + (OmR10*y[76])/2. - (OmI10*y[77])/2. - 2.44928427874793e7*y[86] - 8*BetaK*dpd2*VrecSqr*y[87] - 8*BetaK*Vrec*vx*y[87]
#dydt[87] = (OmI6*y[66])/2. + (OmR6*y[67])/2. - (OmI10*y[76])/2. - (OmR10*y[77])/2. + 8*BetaK*dpd2*VrecSqr*y[86] + 8*BetaK*Vrec*vx*y[86] - 2.44928427874793e7*y[87]
#dydt[88] = 1.97236098296205e7*y[13] + 1.97236098296205e7*y[15] + 1.97236098296205e7*y[17] + 1.97236098296205e7*y[19] + 1.97236098296205e7*y[1] + 1.97236098296205e7*y[3] + 1.97236098296205e7*y[5] + 1.97236098296205e7*y[9]
#dydt[89] = 1.16423653830853e6*y[5] + 1.16423653830853e6*y[9]
#dydt[90] = 1.97236098296205e7*y[5] + 1.97236098296205e7*y[9]
#dydt[91] = 1.97236098296205e7*y[13] + 1.97236098296205e7*y[15] + 1.97236098296205e7*y[17] + 1.97236098296205e7*y[19] + 1.97236098296205e7*y[1] + 1.97236098296205e7*y[3]
#dydt[92] = 1.97236098296205e7*y[13] + 1.97236098296205e7*y[17] + 1.97236098296205e7*y[1]
#dydt[93] = 1.97236098296205e7*y[15] + 1.97236098296205e7*y[19] + 1.97236098296205e7*y[3]

num_eqs = 94
num_signals = 6
num_states_to_save = 23
backdecay_on = True

# Dictionary states contains dictionary with quantum numbers and metadata on state i as states[i]
states = {}
states[1] = {'n': 2, 'P': 0, 'J': 0.5, 'F': 0., 'm': 0., 'Energy': -5.164939028111531e15, 'Momentum': '0', 'NBackdecays': 0}
states[2] = {'n': 6, 'P': 1, 'J': 0.5, 'F': 1., 'm': 0., 'Energy': -5.73877655518769e14, 'Momentum': 'dpd1', 'NBackdecays': 1}
states[3] = {'n': 1, 'P': 0, 'J': 0.5, 'F': 0., 'm': 0., 'Energy': -2.0659665722838864e16, 'Momentum': '0', 'NBackdecays': 1}
states[4] = {'n': 6, 'P': 1, 'J': 0.5, 'F': 1., 'm': 1., 'Energy': -5.73877655518769e14, 'Momentum': 'dpd2', 'NBackdecays': 1}
states[5] = {'n': 1, 'P': 0, 'J': 0.5, 'F': 0., 'm': 0., 'Energy': -2.0659665722838864e16, 'Momentum': 'dpd2', 'NBackdecays': 1}
states[6] = {'n': 2, 'P': 0, 'J': 0.5, 'F': 0., 'm': 0., 'Energy': -5.164939028111531e15, 'Momentum': '-1 + dpd1', 'NBackdecays': 1}
states[7] = {'n': 6, 'P': 1, 'J': 0.5, 'F': 1., 'm': 0., 'Energy': -5.73877655518769e14, 'Momentum': '-1', 'NBackdecays': 0}
states[8] = {'n': 1, 'P': 0, 'J': 0.5, 'F': 0., 'm': 0., 'Energy': -2.0659665722838864e16, 'Momentum': '-1 + dpd1', 'NBackdecays': 0}
states[9] = {'n': 2, 'P': 0, 'J': 0.5, 'F': 1., 'm': 1., 'Energy': -5.164939028111531e15, 'Momentum': '-1 + dpd2', 'NBackdecays': 1}
states[10] = {'n': 2, 'P': 0, 'J': 0.5, 'F': 0., 'm': 0., 'Energy': -5.164939028111531e15, 'Momentum': '1 + dpd1', 'NBackdecays': 1}
states[11] = {'n': 6, 'P': 1, 'J': 0.5, 'F': 1., 'm': 0., 'Energy': -5.73877655518769e14, 'Momentum': '1', 'NBackdecays': 0}
states[12] = {'n': 1, 'P': 0, 'J': 0.5, 'F': 0., 'm': 0., 'Energy': -2.0659665722838864e16, 'Momentum': '1 + dpd1', 'NBackdecays': 0}
states[13] = {'n': 2, 'P': 0, 'J': 0.5, 'F': 1., 'm': 1., 'Energy': -5.164939028111531e15, 'Momentum': '1 + dpd2', 'NBackdecays': 1}
states[14] = {'n': 2, 'P': 0, 'J': 0.5, 'F': 0., 'm': 0., 'Energy': -5.164939028111531e15, 'Momentum': '-2', 'NBackdecays': 0}
states[15] = {'n': 6, 'P': 1, 'J': 0.5, 'F': 1., 'm': 0., 'Energy': -5.73877655518769e14, 'Momentum': '-2 + dpd1', 'NBackdecays': 1}
states[16] = {'n': 1, 'P': 0, 'J': 0.5, 'F': 0., 'm': 0., 'Energy': -2.0659665722838864e16, 'Momentum': '-2', 'NBackdecays': 1}
states[17] = {'n': 6, 'P': 1, 'J': 0.5, 'F': 1., 'm': 1., 'Energy': -5.73877655518769e14, 'Momentum': '-2 + dpd2', 'NBackdecays': 1}
states[18] = {'n': 1, 'P': 0, 'J': 0.5, 'F': 0., 'm': 0., 'Energy': -2.0659665722838864e16, 'Momentum': '-2 + dpd2', 'NBackdecays': 1}
states[19] = {'n': 2, 'P': 0, 'J': 0.5, 'F': 0., 'm': 0., 'Energy': -5.164939028111531e15, 'Momentum': '2', 'NBackdecays': 0}
states[20] = {'n': 6, 'P': 1, 'J': 0.5, 'F': 1., 'm': 0., 'Energy': -5.73877655518769e14, 'Momentum': '2 + dpd1', 'NBackdecays': 1}
states[21] = {'n': 1, 'P': 0, 'J': 0.5, 'F': 0., 'm': 0., 'Energy': -2.0659665722838864e16, 'Momentum': '2', 'NBackdecays': 1}
states[22] = {'n': 6, 'P': 1, 'J': 0.5, 'F': 1., 'm': 1., 'Energy': -5.73877655518769e14, 'Momentum': '2 + dpd2', 'NBackdecays': 1}
states[23] = {'n': 1, 'P': 0, 'J': 0.5, 'F': 0., 'm': 0., 'Energy': -2.0659665722838864e16, 'Momentum': '2 + dpd2', 'NBackdecays': 1}

# Array rho_to_dydt contains the mapping of the time derivate of the density matrix d/dt rho[i, j] for states i, j
# to the real and imaginary part of the OBEs as d/dt rho[i, j] = rho_to_dydt[i, j, 0] + 1j*rho_to_dydt[i, j, 1]
rho_to_dydt = np.zeros((24, 24, 2))
rho_to_dydt[:] = -1
rho_to_dydt[1, 1, 0] = 0
rho_to_dydt[2, 2, 0] = 1
rho_to_dydt[3, 3, 0] = 2
rho_to_dydt[4, 4, 0] = 3
rho_to_dydt[6, 6, 0] = 4
rho_to_dydt[7, 7, 0] = 5
rho_to_dydt[8, 8, 0] = 6
rho_to_dydt[9, 9, 0] = 7
rho_to_dydt[10, 10, 0] = 8
rho_to_dydt[11, 11, 0] = 9
rho_to_dydt[12, 12, 0] = 10
rho_to_dydt[13, 13, 0] = 11
rho_to_dydt[14, 14, 0] = 12
rho_to_dydt[15, 15, 0] = 13
rho_to_dydt[16, 16, 0] = 14
rho_to_dydt[17, 17, 0] = 15
rho_to_dydt[19, 19, 0] = 16
rho_to_dydt[20, 20, 0] = 17
rho_to_dydt[21, 21, 0] = 18
rho_to_dydt[22, 22, 0] = 19
rho_to_dydt[1, 7, 0] = 20
rho_to_dydt[1, 7, 1] = 21
rho_to_dydt[1, 11, 0] = 22
rho_to_dydt[1, 11, 1] = 23
rho_to_dydt[1, 14, 0] = 24
rho_to_dydt[1, 14, 1] = 25
rho_to_dydt[1, 19, 0] = 26
rho_to_dydt[1, 19, 1] = 27
rho_to_dydt[2, 6, 0] = 28
rho_to_dydt[2, 6, 1] = 29
rho_to_dydt[2, 10, 0] = 30
rho_to_dydt[2, 10, 1] = 31
rho_to_dydt[2, 15, 0] = 32
rho_to_dydt[2, 15, 1] = 33
rho_to_dydt[2, 20, 0] = 34
rho_to_dydt[2, 20, 1] = 35
rho_to_dydt[3, 16, 0] = 36
rho_to_dydt[3, 16, 1] = 37
rho_to_dydt[3, 21, 0] = 38
rho_to_dydt[3, 21, 1] = 39
rho_to_dydt[4, 9, 0] = 40
rho_to_dydt[4, 9, 1] = 41
rho_to_dydt[4, 13, 0] = 42
rho_to_dydt[4, 13, 1] = 43
rho_to_dydt[4, 17, 0] = 44
rho_to_dydt[4, 17, 1] = 45
rho_to_dydt[4, 22, 0] = 46
rho_to_dydt[4, 22, 1] = 47
rho_to_dydt[6, 10, 0] = 48
rho_to_dydt[6, 10, 1] = 49
rho_to_dydt[6, 15, 0] = 50
rho_to_dydt[6, 15, 1] = 51
rho_to_dydt[6, 20, 0] = 52
rho_to_dydt[6, 20, 1] = 53
rho_to_dydt[7, 11, 0] = 54
rho_to_dydt[7, 11, 1] = 55
rho_to_dydt[7, 14, 0] = 56
rho_to_dydt[7, 14, 1] = 57
rho_to_dydt[7, 19, 0] = 58
rho_to_dydt[7, 19, 1] = 59
rho_to_dydt[8, 12, 0] = 60
rho_to_dydt[8, 12, 1] = 61
rho_to_dydt[9, 13, 0] = 62
rho_to_dydt[9, 13, 1] = 63
rho_to_dydt[9, 17, 0] = 64
rho_to_dydt[9, 17, 1] = 65
rho_to_dydt[9, 22, 0] = 66
rho_to_dydt[9, 22, 1] = 67
rho_to_dydt[10, 15, 0] = 68
rho_to_dydt[10, 15, 1] = 69
rho_to_dydt[10, 20, 0] = 70
rho_to_dydt[10, 20, 1] = 71
rho_to_dydt[11, 14, 0] = 72
rho_to_dydt[11, 14, 1] = 73
rho_to_dydt[11, 19, 0] = 74
rho_to_dydt[11, 19, 1] = 75
rho_to_dydt[13, 17, 0] = 76
rho_to_dydt[13, 17, 1] = 77
rho_to_dydt[13, 22, 0] = 78
rho_to_dydt[13, 22, 1] = 79
rho_to_dydt[14, 19, 0] = 80
rho_to_dydt[14, 19, 1] = 81
rho_to_dydt[15, 20, 0] = 82
rho_to_dydt[15, 20, 1] = 83
rho_to_dydt[16, 21, 0] = 84
rho_to_dydt[16, 21, 1] = 85
rho_to_dydt[17, 22, 0] = 86
rho_to_dydt[17, 22, 1] = 87
rho_to_dydt = rho_to_dydt.astype(int)

# Description of signal equations
signal_set_id = 'OBEDMS'
signal_equations = {}
signal_equations['Lyman'] = 88
signal_equations['Balmer'] = 89
signal_equations['LymanNoBD'] = 90
signal_equations['LymanBDCorrOnly'] = 91
signal_equations['LymanBD1pi'] = 92
signal_equations['LymanBD1sigma'] = 93
signal_equations['Lyman-epsilon'] = 88
signal_equations['Balmer-delta'] = 89
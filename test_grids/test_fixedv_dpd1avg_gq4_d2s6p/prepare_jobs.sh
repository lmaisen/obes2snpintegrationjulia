#!/bin/bash
mkdir -p /tqo/u/lmaisen/GridResults/test_fixedv_dpd1avg_gq4_d2s6p/Jobs
mkdir -p /tqo/u/lmaisen/GridResults/test_fixedv_dpd1avg_gq4_d2s6p/Lines
mkdir -p /tqo/u/lmaisen/GridResults/test_fixedv_dpd1avg_gq4_d2s6p/Logs
mkdir -p /tqo/u/lmaisen/GridResults/test_fixedv_dpd1avg_gq4_d2s6p/Source
mkdir -p /tqo/u/lmaisen/GridResults/test_fixedv_dpd1avg_gq4_d2s6p/Metadata
mkdir -p /tqo/u/lmaisen/GridResults/test_fixedv_dpd1avg_gq4_d2s6p/Scripts
mkdir -p /tqo/u/lmaisen/GridResults/test_fixedv_dpd1avg_gq4_d2s6p/Stats
mkdir -p /tqo/u/lmaisen/GridResults/test_fixedv_dpd1avg_gq4_d2s6p/OBE
zip -r -q /tqo/u/lmaisen/GridResults/test_fixedv_dpd1avg_gq4_d2s6p/Source/obes2snpintegrationjulia.zip /tqo/u/lmaisen/Julia/obes2snpintegrationjulia -x '*.git*'
git -C /tqo/u/lmaisen/Julia/obes2snpintegrationjulia/.git rev-parse HEAD > /tqo/u/lmaisen/GridResults/test_fixedv_dpd1avg_gq4_d2s6p/Source/obes2snpintegrationjulia.sha
cp -R /tqo/u/lmaisen/Julia/obes2snpintegrationjulia/H2SnPOBEs/src/OBEs/dmsD2s6p12CDonNp2BDonNDpD1/* /tqo/u/lmaisen/GridResults/test_fixedv_dpd1avg_gq4_d2s6p/OBE
chmod 777 2022-10-03-18-01-36_post.sh
chmod 777 submit_jobs.cmd
cp Jobs/* /tqo/u/lmaisen/GridResults/test_fixedv_dpd1avg_gq4_d2s6p/Jobs
cp Metadata/* /tqo/u/lmaisen/GridResults/test_fixedv_dpd1avg_gq4_d2s6p/Metadata
cp Scripts/* /tqo/u/lmaisen/GridResults/test_fixedv_dpd1avg_gq4_d2s6p/Scripts
./submit_jobs.cmd

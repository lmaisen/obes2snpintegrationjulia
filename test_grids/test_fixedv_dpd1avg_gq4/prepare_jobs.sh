#!/bin/bash
mkdir -p /cobra/u/lmaisen/Julia/GridResults/test_fixedv_dpd1avg_gq4/Jobs
mkdir -p /cobra/u/lmaisen/Julia/GridResults/test_fixedv_dpd1avg_gq4/Lines
mkdir -p /cobra/u/lmaisen/Julia/GridResults/test_fixedv_dpd1avg_gq4/Logs
mkdir -p /cobra/u/lmaisen/Julia/GridResults/test_fixedv_dpd1avg_gq4/Source
mkdir -p /cobra/u/lmaisen/Julia/GridResults/test_fixedv_dpd1avg_gq4/Metadata
mkdir -p /cobra/u/lmaisen/Julia/GridResults/test_fixedv_dpd1avg_gq4/Scripts
mkdir -p /cobra/u/lmaisen/Julia/GridResults/test_fixedv_dpd1avg_gq4/Stats
mkdir -p /cobra/u/lmaisen/Julia/GridResults/test_fixedv_dpd1avg_gq4/OBE
zip -r -q /cobra/u/lmaisen/Julia/GridResults/test_fixedv_dpd1avg_gq4/Source/obes2snpintegrationjulia.zip /cobra/u/lmaisen/Julia/obes2snpintegrationjulia -x '*.git*'
git -C /cobra/u/lmaisen/Julia/obes2snpintegrationjulia/.git rev-parse HEAD > /cobra/u/lmaisen/Julia/GridResults/test_fixedv_dpd1avg_gq4/Source/obes2snpintegrationjulia.sha
cp -R /cobra/u/lmaisen/Julia/obes2snpintegrationjulia/H2SnPOBEs/src/OBEs/dms2s6p12CDonNp4BDonNDpD1/* /cobra/u/lmaisen/Julia/GridResults/test_fixedv_dpd1avg_gq4/OBE
chmod 777 2022-07-04-21-14-17_post.sh
chmod 777 submit_jobs.cmd
cp Jobs/job_1.job /cobra/u/lmaisen/Julia/GridResults/test_fixedv_dpd1avg_gq4/Jobs/job_1.job
cp Metadata/test_fixedv_dpd1avg_gq4_grids.dat /cobra/u/lmaisen/Julia/GridResults/test_fixedv_dpd1avg_gq4/Metadata/test_fixedv_dpd1avg_gq4_grids.dat
cp Metadata/test_fixedv_dpd1avg_gq4_scans.dat /cobra/u/lmaisen/Julia/GridResults/test_fixedv_dpd1avg_gq4/Metadata/test_fixedv_dpd1avg_gq4_scans.dat
cp Scripts/build_jobs.py /cobra/u/lmaisen/Julia/GridResults/test_fixedv_dpd1avg_gq4/Scripts/build_jobs.py
./submit_jobs.cmd

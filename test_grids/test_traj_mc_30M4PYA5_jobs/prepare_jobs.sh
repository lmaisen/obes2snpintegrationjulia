#!/bin/bash
mkdir -p /cobra/u/lmaisen/Julia/GridResults/test_traj_mc_30M4PYA5_jobs/Jobs
mkdir -p /cobra/u/lmaisen/Julia/GridResults/test_traj_mc_30M4PYA5_jobs/Lines
mkdir -p /cobra/u/lmaisen/Julia/GridResults/test_traj_mc_30M4PYA5_jobs/Logs
mkdir -p /cobra/u/lmaisen/Julia/GridResults/test_traj_mc_30M4PYA5_jobs/Source
mkdir -p /cobra/u/lmaisen/Julia/GridResults/test_traj_mc_30M4PYA5_jobs/Metadata
mkdir -p /cobra/u/lmaisen/Julia/GridResults/test_traj_mc_30M4PYA5_jobs/Scripts
mkdir -p /cobra/u/lmaisen/Julia/GridResults/test_traj_mc_30M4PYA5_jobs/Stats
mkdir -p /cobra/u/lmaisen/Julia/GridResults/test_traj_mc_30M4PYA5_jobs/OBE
zip -r -q /cobra/u/lmaisen/Julia/GridResults/test_traj_mc_30M4PYA5_jobs/Source/obes2snpintegrationjulia.zip /cobra/u/lmaisen/Julia/obes2snpintegrationjulia -x '*.git*'
git -C /cobra/u/lmaisen/Julia/obes2snpintegrationjulia/.git rev-parse HEAD > /cobra/u/lmaisen/Julia/GridResults/test_traj_mc_30M4PYA5_jobs/Source/obes2snpintegrationjulia.sha
cp -R /cobra/u/lmaisen/Julia/obes2snpintegrationjulia/H2SnPOBEs/src/OBEs/dms2s6p12CDonNp4BDonNDpD1/* /cobra/u/lmaisen/Julia/GridResults/test_traj_mc_30M4PYA5_jobs/OBE
chmod 777 2022-07-04-21-14-17_post.sh
chmod 777 submit_jobs.cmd
cp Jobs/job_1.job /cobra/u/lmaisen/Julia/GridResults/test_traj_mc_30M4PYA5_jobs/Jobs/job_1.job
cp Jobs/job_2.job /cobra/u/lmaisen/Julia/GridResults/test_traj_mc_30M4PYA5_jobs/Jobs/job_2.job
cp Jobs/job_3.job /cobra/u/lmaisen/Julia/GridResults/test_traj_mc_30M4PYA5_jobs/Jobs/job_3.job
cp Jobs/job_4.job /cobra/u/lmaisen/Julia/GridResults/test_traj_mc_30M4PYA5_jobs/Jobs/job_4.job
cp Metadata/test_traj_mc_30M4PYA5_jobs_grids.dat /cobra/u/lmaisen/Julia/GridResults/test_traj_mc_30M4PYA5_jobs/Metadata/test_traj_mc_30M4PYA5_jobs_grids.dat
cp Metadata/test_traj_mc_30M4PYA5_jobs_scans.dat /cobra/u/lmaisen/Julia/GridResults/test_traj_mc_30M4PYA5_jobs/Metadata/test_traj_mc_30M4PYA5_jobs_scans.dat
cp Scripts/build_jobs.py /cobra/u/lmaisen/Julia/GridResults/test_traj_mc_30M4PYA5_jobs/Scripts/build_jobs.py
./submit_jobs.cmd

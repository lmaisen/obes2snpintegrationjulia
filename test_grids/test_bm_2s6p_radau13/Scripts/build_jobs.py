# -*- coding: utf-8 -*-
"""
@author: Lothar Maisenbacher/MPQ

Create jobs and metadata for numerical integration of OBEs on MPCDF clusters,
using the Julia OBE integrator `obes2snpintegrationjulia` by L.M.
(https://gitlab.mpcdf.mpg.de/lmaisen/obes2snpintegrationjulia)
and the SLURM job manager.
"""

import numpy as np
import pandas as pd
import itertools
import datetime
import os
import glob
import uuid
import shutil
import random

# pyhs
import pyhs.gen
import pyhs.files

# pyha
import pyha.def_atomic
import pyha.sim_grids
import pyha.def_line_sampling

### Seed random number generator
seed = int.from_bytes(os.urandom(2500), 'big')
random.seed(seed)

### Init simulation grid class
SimGrids = pyha.sim_grids.SimGrids()

# Get OBE ID
def get_obe_id(grid):
    """Get ID of OBE from grid parameters."""
    obe_id = None
    if grid['OBEID'] != '':
        obe_id = grid['OBEID']
    else:
        if grid['Model'] == 'DMS':
            obe_id = (
                f'dms2s{grid["FS"].lower()}'
                +f'CD{"on" if grid["QI"] else "off"}'
                +f'Np{grid["MaxTransverseMomentum"]:d}'
                +f'BD{"on" if grid["Backdecay"] else "off"}'
                +(
                    'NDpD{:d}'.format(grid['NDeltapDecay'])
                    if grid['NDeltapDecay'] != 0 and grid['Backdecay']
                    else '')
                )
        elif grid['Model'] == 'BigModel':
            obe_id = (
                f'bm2s{grid["FS"].lower()}CD{"on" if grid["QI"] else "off"}')
    return obe_id

### Atomic properties
atomic_properties = pyha.def_atomic.atomic_properties

### Parameters
## OBE parameters and numerical integration settings
# Constant parameters.
# These parameters are currently hard-coded and changing the values here has no effect.
obe_params_constants = {
    # Second-order Doppler effect on 2S-nP transition included (bool)
    '2SnPSOD': False,
    # 2S-nP laser beam backreflection switched on (bool)
    '2SnPBackreflection': True,
    # 2S-nP laser beam divergence included (bool)
    '2SnPLaserBeamDivergence': False,
    }
# Future parameters.
# These parameters are currently not implemented, but are reserved for future use and
# might have already been used in previous implementations.
obe_params_future = {
    # Distance to detection region from nozzle (m)
    'DetectionDistance': 0.204,
    }
# Default parameters.
obe_params_defaults = {
    # 2S-nP laser 1/e^2 beam radius (m)
    '2SnPLaserWaistRadius': 2.2e-3,
    # 2S-nP laser beam profile (either 'Gaussian' or 'Square')
    '2SnPLaserBeamProfile': 'Gaussian',
    # Start of detection region along longitudinal (z) direction,
    # relative to interaction point (m)
    'DetectionBoundaryStart': -7.5e-3,
    # Endof detection region along longitudinal (z) direction,
    # relative to interaction point (m)
    'DetectionBoundaryEnd': 7.5e-3,
    # Isotope
    'Isotope': 'H',
    # Numerical integration: Solver
    'Solver': 'radau',
    # Numerical integration: Absolute tolerance
    'SolveAbsTol': 1e-12,
    # Numerical integration: Relative tolerance
    'SolveRelTol': 5e-13,
    # Numerical integration: Max. number of iterations
    'SolveMaxIters': int(2e7),
    # Numerical integration: Initial step size (s)
    'SolveInitialStepSize': 1e-12,
    }
# Combine constant, future, and default parameters
obe_params_defaults = {
    **obe_params_constants,
    **obe_params_future,
    **obe_params_defaults,
    }
## Grid generation parameters
# Default grid generation parameters
grid_params_default = {
    # Randomize transverse velocities (e.g. used to create grids to test interpolation) (bool)
    'RandomizeVxs': False,
    # Transverse velocities are included up to included this maximum alpha angle (mrad, float)
    # (applies only to 'VxVP' grids)
    'MaxAlpha': 16.,
    # Maximum change of atom transverse momentum (int), in units of ħk, where k is the photon
    # momentum, included in OBEs. For the Big model, this is always 0.
    'MaxTransverseMomentum': 0,
    }

### Settings

## Cluster
# Cluster to use ('Raven' or 'Cobra')
cluster_id = 'Cobra'
# Max. number of jobs that can/should be submitted at once
max_jobs = 1000
# Number of workers/threads per job
num_cpus = 40
num_threads = 29
# num_cpus = 32
# num_threads = 32
# Username on cluster
username = 'lmaisen'
# Print verbose output
verbose_output = True
# Garbage collect on workers and in main thread after every n-th scan, where n (int) is set here.
# Set to None for default of script, and to -1 to deactivate garbage collection altogether.
gc_workers = 100
# Cluster-specific settings
clusters = {}
clusters['Raven'] = {
    # Path to directory in which results will be saved
    'OutputDir': '/raven/u/'+username+'/Julia/GridResults',
    # Path to repository of Julia OBE integrator
    'RepDir': (
        '/raven/u/'+username+'/Julia/obes2snpintegrationjulia'),
    # Path to Julia project/environment
    'ProjectDir': (
        '/raven/u/'+username+'/Julia/obes2snpintegrationjulia/scripts'),
    'Partition': None,
    # Module to load that makes Julia available
    'JuliaModule': 'julia/1.7',
    # Memory per worker (in MB)
    'MemPerWorker': 3000,
    }
clusters['Cobra'] = {
    'OutputDir': '/cobra/u/'+username+'/Julia/GridResults',
    'RepDir': (
        '/cobra/u/'+username+'/Julia/obes2snpintegrationjulia'),
    'ProjectDir': (
        '/cobra/u/'+username+'/Julia/obes2snpintegrationjulia/scripts'),
    'Partition': None,
    'JuliaModule': 'julia/1.7',
    'MemPerWorker': 2000,
    }

# Common grid parameters
grid_params_default = {
    **grid_params_default,
    'InputState': 'PlaneWave',
    'NThreads': num_threads,
    'NCPUs': num_cpus,
    }

## Jobs
# Prefix time on job file and metadata files
prefix_time = False
# Submit jobs by calling submit_jobs.cmd at the end of prepare_jobs.sh
submit_jobs_after_prep = True
# Number of decimal places alpha angle (in mrad) is rounded to
alpha_round_dec_places = 9
# Number of decimal places transverse velocity (vx) (in m/s) is rounded to
vx_round_dec_places = 9

## Grids to generate
grids_params = []
# Grid model and type: Discrete momentum space (DMS)
# Discrete momentum space model, takes into account 2S F=0 mF=0, 1S, and
# nP F=1 mF=0 levels and the momentum change upon absorption or emission
# of photons.
# Used for simulation of light force shift.
# Grid type 'FixedV': Fixed 2S trajectory and back decay momentum change
grids_params += [
    # {'Model': 'DMS', 'Type': 'FixedV', 'GridUID': 'test_fixedv_dpdscan',
    # 'FS': '6P12', 'QI': True, 'Backdecay': True, 'BackdecayModel': 'NDpD1',
    # 'NDeltapDecay': 1, 'SignalSetID': 'OBEDMS',
    # 'DeltapDecayAvg': '', 'NDeltapDecayAvg': -1,
    # 'Solver': 'Vern7','SolveAbsTol': 1e-10, 'SolveRelTol': 5e-11, 'SolveMaxIters': int(2e7),
    # 'SolveInitialStepSize': 1e-12,
    # 'Speeds': [200.], '2SnPLaserPowers': [30.], 'VxOffsetsVr': [0.],
    # },
    # No back decay
    # {'Model': 'DMS', 'Type': 'FixedV', 'GridUID': 'test_10lines_fixedv_bdoff',
    # 'FS': '6P12', 'QI': True, 'Backdecay': False, 'BackdecayModel': 'NDpD0',
    # 'NDeltapDecay': 0, 'SignalSetID': 'OBEDMS',
    # 'Solver': 'Vern7','SolveAbsTol': 1e-10, 'SolveRelTol': 5e-11, 'SolveMaxIters': int(2e7),
    # 'SolveInitialStepSize': 1e-12,
    # 'Speeds': [200.], '2SnPLaserPowers': [30.], 'VxOffsetsVr': [0.],
    # },
    # Deuterium approximated with hydrogen levels
    # {'Model': 'DMS', 'Type': 'FixedV', 'GridUID': 'test_hd_fixedv_dpdscan_dpd2s',
    # 'OBEID': 'dmsHD2s6p12CDonNp2BDonNDpD1',
    # 'FS': '6P12', 'QI': True, 'Backdecay': True, 'BackdecayModel': 'NDpD1',
    # 'NDeltapDecay': 1, 'SignalSetID': 'OBEDMS',
    # 'DeltapDecayAvg': '', 'NDeltapDecayAvg': -1,
    # 'CalcFreqSampling': 'Fixed', 'CalcFixedFreqListID': 'H2S6P2019_alphaOffsetleq6mrad',
    # 'Solver': 'Vern7','SolveAbsTol': 1e-10, 'SolveRelTol': 5e-11, 'SolveMaxIters': int(2e7),
    # 'SolveInitialStepSize': 1e-12,
    # 'Speeds': [200.], '2SnPLaserPowers': [30.], 'VxOffsetsVr': [0.], 'Dpd2s': [-1, 0, 1],
    # },
    ]
# Grid type 'FixedVDpDavg': Fixed 2S trajectory, averaging over back decay momentum change
grids_params += [
    # Averaging over back decay momentum change DpD1 using 4-point Gaussian quadrature rule
    # {'Model': 'DMS', 'Type': 'FixedVDpDavg', 'GridUID': 'test_fixedv_dpd1avg_gq4',
    # 'FS': '6P12', 'QI': True, 'Backdecay': True, 'BackdecayModel': 'NDpD1',
    # 'NDeltapDecay': 1, 'SignalSetID': 'OBEDMS',
    # 'DeltapDecayAvg': 'GQ', 'NDeltapDecayAvg': 4,
    # 'CalcFreqSampling': 'Fixed', 'CalcFixedFreqListID': 'H2S6P2019_alphaOffsetleq6mrad',
    # 'Solver': 'Vern7','SolveAbsTol': 1e-10, 'SolveRelTol': 5e-11, 'SolveMaxIters': int(2e7),
    # 'SolveInitialStepSize': 1e-12,
    # 'Speeds': [200.], '2SnPLaserPowers': [30.],
    # },
    # Uniform averaging over back decay momentum change DpD1
    # {'Model': 'DMS', 'Type': 'FixedVDpDavg', 'GridUID': 'test_fixedv_dpd1avg_uniform101',
    # 'FS': '6P12', 'QI': True, 'Backdecay': True, 'BackdecayModel': 'NDpD1',
    # 'NDeltapDecay': 1, 'SignalSetID': 'OBEDMS',
    # 'DeltapDecayAvg': 'Uniform', 'NDeltapDecayAvg': 101,
    # 'CalcFreqSampling': 'Fixed', 'CalcFixedFreqListID': 'H2S6P2019_alphaOffsetleq6mrad',
    # 'Solver': 'Vern7','SolveAbsTol': 1e-10, 'SolveRelTol': 5e-11, 'SolveMaxIters': int(2e7),
    # 'SolveInitialStepSize': 1e-12,
    # 'Speeds': [200.], '2SnPLaserPowers': [30.],
    # },
    ]
# Grid type 'VxVP': DMS model VxVP grid (transverse velocity - speed - 2S-nP laser power)
# This differs from grid type 'FixedVDpDavg' in that the transverse velocity (vx) is sampled at
# fixed points, as opposed to sampling the angle (alpha angle).
grids_params += [
    # {'Model': 'DMS', 'Type': 'VxVP', 'GridUID': 'DMSVxVPGrid19',
    # 'FS': '6P12', 'QI': True, 'Backdecay': True, 'BackdecayModel': 'NDpD1',
    # 'NDeltapDecay': 1, 'SignalSetID': 'OBEDMS',
    # 'DeltapDecayAvg': 'GQ', 'NDeltapDecayAvg': 4,
    # 'CalcFreqSampling': 'Fixed', 'CalcFixedFreqListID': 'H2S6P2019_alphaOffsetleq6mrad',
    # 'Solver': 'Vern7','SolveAbsTol': 1e-10, 'SolveRelTol': 5e-11, 'SolveMaxIters': int(2e7),
    # 'SolveInitialStepSize': 1e-12,
    # 'Speeds': (
    #     np.hstack((
    #         np.linspace(20, 300, 29),
    #         np.linspace(300, 1200, 37)
    #     ))),
    # '2SnPLaserPowers': np.hstack(([1.], np.linspace(2.5, 35, 14))),
    # },
    # {'Model': 'DMS', 'Type': 'VxVP', 'GridUID': 'DMSVxVPTestGrid5',
    # 'FS': '6P12', 'QI': True, 'Backdecay': True, 'BackdecayModel': 'NDpD1',
    # 'NDeltapDecay': 1, 'SignalSetID': 'OBEDMS',
    # 'DeltapDecayAvg': 'GQ', 'NDeltapDecayAvg': 4,
    # 'CalcFreqSampling': 'Fixed', 'CalcFixedFreqListID': 'H2S6P2019_alphaOffsetleq6mrad',
    # 'Solver': 'Vern7','SolveAbsTol': 1e-10, 'SolveRelTol': 5e-11, 'SolveMaxIters': int(2e7),
    # 'SolveInitialStepSize': 1e-12,
    # 'RandomizeVxs': True,
    # 'Speeds': (
    #     np.hstack((
    #         np.linspace(20, 300, 29),
    #         np.linspace(300, 1200, 37)
    #     ))),
    # '2SnPLaserPowers': [30.],
    # },
    ]
# MC of 2S trajectory set, averaging over momentum change
grids_params += [
    # {'Model': 'DMS', 'Type': 'MCDpDavg', 'GridUID': 'test_traj_mc_30M4PYA5',
    # 'FS': '6P12', 'QI': True, 'Backdecay': True, 'BackdecayModel': 'NDpD1',
    # 'NDeltapDecay': 1, 'SignalSetID': 'OBEDMS',
    # 'DeltapDecayAvg': 'GQ', 'NDeltapDecayAvg': 4,
    # 'CalcFreqSampling': 'Fixed', 'CalcFixedFreqListID': 'H2S6P2019_alphaOffsetleq6mrad',
    # 'Solver': 'Vern7','SolveAbsTol': 1e-10, 'SolveRelTol': 5e-11, 'SolveMaxIters': int(2e7),
    # 'SolveInitialStepSize': 1e-12,
    # '2SnPLaserPowers': [10., 30.],
    # 'AlphaOffsets': [0., 10.],
    # 'TrajUIDs': ['30M4PYA5'], 'NsTrajs': [int(1e1)], 'DelaySetID': '2S6P',
    # },
    ]
# Grid model and type: Big model (BM)
# This model includes all coupled levels up to n, but does not take into account
# the momentum change in absorption or emission of photons
grids_params += [
    # {'Model': 'BigModel', 'Type': 'AVP', 'GridUID': 'test_bm_2s4p_radau13',
    # 'OBEID': 'bm2s4p12CDon',
    # 'FS': '4P12', 'QI': True, 'Backdecay': True,
    # 'BackdecayModel': 'NDpD0', 'NDeltapDecay': 0, 'SignalSetID': 'All2S4P',
    # 'CalcFreqSampling': 'Fixed', 'CalcFixedFreqListID': 'Span80MHzPoints79',
    # 'Solver': 'radau', 'SolveAbsTol': 1e-13, 'SolveRelTol': 5e-14, 'SolveMaxIters': int(2e7),
    # 'SolveInitialStepSize': 1e-12, 'NThreads': 32,
    # 'Alphas': [2.0], 'Speeds': [200.], '2SnPLaserPowers': [15.],
    # '2SnPLaserWaistRadius': 2.11e-3,
    # },
    {'Model': 'BigModel', 'Type': 'AVP', 'GridUID': 'test_bm_2s6p_radau13',
    'OBEID': 'bm2s6p12CDon',
    'FS': '6P12', 'QI': True, 'Backdecay': True,
    'BackdecayModel': 'NDpD0', 'NDeltapDecay': 0, 'SignalSetID': 'All2S6P',
    'CalcFreqSampling': 'Fixed', 'CalcFixedFreqListID': 'Span80MHzPoints79',
    'Solver': 'radau', 'SolveAbsTol': 1e-13, 'SolveRelTol': 5e-14, 'SolveMaxIters': int(2e7),
    'SolveInitialStepSize': 1e-12, 'NThreads': 32,
    'Alphas': [0.5], 'Speeds': [200.], '2SnPLaserPowers': [30.],
    '2SnPLaserWaistRadius': 2.2e-3,
    },
    # {'Model': 'BigModel', 'Type': 'AVP', 'GridUID': 'InterpTest29bVs_2',
    # 'FS': '6P12', 'QI': True, 'Backdecay': True, 'OBEID': 'bm2s6p12CDon',
    # 'BackdecayModel': 'NDpD0', 'NDeltapDecay': 0, 'SignalSetID': 'All2S6P',
    # 'CalcFreqSampling': 'Fixed', 'CalcFixedFreqListID': 'Span128MHzPoints256',
    # 'Solver': 'radau', 'SolveAbsTol': 1e-13, 'SolveRelTol': 5e-14, 'SolveMaxIters': int(2e7),
    # 'SolveInitialStepSize': 1e-12, 'NThreads': 32,
    # 'Alphas': [0.5],
    # 'Speeds': [
    #     25., 50., 75., 100., 125., 150., 175., 200., 225.,
    #     250.,  275.,  300.,  325.,  350.,  375.,  400.,  425.,  450.,
    #     475.,  500.,  525.,  550.,  575.,  600.,  625.,  650.,  675.,
    #     700.,  725.,  750.,  775.,  800.,  825.,  850.,  875.,  900.,
    #     925.,  950.,  975., 1000.
    #     ],
    # '2SnPLaserPowers': [30.],
    # },
    # {'Model': 'BigModel', 'Type': 'AVP', 'GridUID': 'test_bm_2s6p',
    # 'OBEID': 'bm2s6p12CDon',
    # 'FS': '6P12', 'QI': True, 'Backdecay': True,
    # 'BackdecayModel': 'NDpD0', 'NDeltapDecay': 0, 'SignalSetID': 'All2S6P',
    # 'CalcFreqSampling': 'Fixed', 'CalcFixedFreqListID': 'Span128MHzPoints256',
    # 'Solver': 'radau', 'SolveAbsTol': 1e-13, 'SolveRelTol': 5e-14, 'SolveMaxIters': int(2e7),
    # 'SolveInitialStepSize': 1e-12, 'NThreads': 32,
    # 'Alphas': [
    #     0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5,
    #     11.5, 12.5, 13.5, 14.5, 15.5],
    # 'Speeds': [200.], '2SnPLaserPowers': [30.],
    # },
    # {'Model': 'BigModel', 'Type': 'AVP', 'GridUID': '30', 'FS': '6P32', 'QI': True,
    # 'OBEID': 'bm2s6p32CDon2002WS', 'Backdecay': True,
    # 'BackdecayModel': 'NDpD0', 'NDeltapDecay': 0, 'SignalSetID': 'All2S6P',
    # 'CalcFreqSampling': 'Fixed', 'CalcFixedFreqListID': 'Span128MHzPoints256',
    # 'Solver': 'Dopr853', 'SolveAbsTol': 1e-10, 'SolveRelTol': 5e-11, 'SolveMaxIters': int(2e7),
    # 'SolveInitialStepSize': 1e-12, 'NThreads': 32,
    # 'Alphas': [
    #     0.5,  1.5,  2.5,  3.5,  4.5,  5.5,  6.5,  7.5,  8.5,  9.5, 10.5,
    #     11.5, 12.5, 13.5, 14.5, 15.5],
    # 'Speeds': [
    #     25., 50., 75., 100., 125., 150., 175., 200., 225.,
    #     250.,  275.,  300.,  325.,  350.,  375.,  400.,  425.,  450.,
    #     475.,  500.,  525.,  550.,  575.,  600.,  625.,  650.,  675.,
    #     700.,  725.,  750.,  775.,  800.,  825.,  850.,  875.,  900.,
    #     925.,  950.,  975., 1000.
    #     ],
    # '2SnPLaserPowers': [
    #     2.5, 5., 7.5, 10., 12.5, 15., 17.5, 20., 22.5, 25., 27.5, 30., 32.5, 35., 37.5,
    #     40., 42.5, 45., 47.5, 50.],
    # },
    # {'Model': 'BigModel', 'Type': 'AVP', 'GridUID': 'D2', 'Isotope': 'D',
    # 'FS': '6P12', 'QI': True, 'Backdecay': True, 'OBEID': 'd2s6pPiExc122021',
    # 'BackdecayModel': 'NDpD0', 'NDeltapDecay': 0, 'SignalSetID': 'Lyman2S6P',
    # 'CalcFreqSampling': 'Fixed', 'CalcFixedFreqListID': 'Span64MHzExpSamplingPoints72',
    # 'Solver': 'Dopr853', 'SolveAbsTol': 1e-10, 'SolveRelTol': 5e-11, 'SolveMaxIters': int(2e7),
    # 'SolveInitialStepSize': 1e-12, 'NThreads': 32,
    # 'Alphas': [
    #     0.5,  1.5,  2.5,  3.5,  4.5,  5.5,  6.5,  7.5,  8.5,  9.5, 10.5,
    #     11.5, 12.5, 13.5, 14.5, 15.5],
    # 'Speeds': [
    #     50., 100., 150., 200., 250., 300., 350., 400., 450., 500.,
    #     550., 600., 650., 700., 750.,  800.,  850.,  900., 950., 1000.
    #     ],
    # '2SnPLaserPowers': [5., 15., 25., 35., 45.],
    # },
    ]

# Job numbering offset
i_job_offset = 0

### Prepare grids
# Init empty DataFrame for grids
dfSimGrids = SimGrids.df_templates[SimGrids.df_id_grids].copy()

# Requested grid params, indexed with requested or assigned grid UID
grids_params_uid_indexed = {}
for grid_params in grids_params:
    grid_model = grid_params['Model']
    grid_type = grid_params['Type']

    # Assign grid UID
    grid_uid = (
        pyhs.gen.get_uid()
        if (grid_params.get('GridUID') is None or grid_params['GridUID'] == '')
        else grid_params['GridUID'])

    # Default values
    alpha_offsets = None

    # Discrete momentum space (DMS) model
    if grid_model == 'DMS':

        obe_params = {
            **obe_params_defaults,
            'Solver': 'Vern7',
            'SolveAbsTol': 1e-10,
            'SolveRelTol': 5e-11,
            'SolveMaxIters': int(2e7),
            'SolveInitialStepSize': 1e-12,
            }

        # Fixed 2S trajectory
        if grid_type in ['FixedV', 'FixedVDpDavg', 'VxVP']:
            type_params = {}
            vr = atomic_properties[grid_params['FS']]['RecoilVelocity']

            grid_params = {
                **grid_params_default,
                'MaxTransverseMomentum': 4,
                'MaxCalcTime': 24*3600,
                **grid_params,
                }

            # Random laser powers
            # laser_powers_2snp = np.array([random.uniform(25, 30) for i in range(10)])

            if grid_type == 'FixedV':
                type_params = {
                    **type_params,
                    'DeltapDecayAvg': '',
                    'NDeltapDecayAvg': -1,
                    }
                transverse_velocities_vr = grid_params['VxOffsetsVr']
                alpha_offsets = np.round(
                    1e3*np.array(
                        [np.arcsin(x*vr/grid_params['Speeds'][0])
                         for x in transverse_velocities_vr]),
                    alpha_round_dec_places)
            if grid_type in ['FixedVDpDavg', 'VxVP']:
                type_params = {
                    **type_params,
                    }
                alpha_offsets = None
            dfSimGrids = dfSimGrids.append(pd.Series({
                'Timestamp': datetime.datetime.utcnow(),
                **obe_params,
                **{key: value for key, value in grid_params.items() if key in dfSimGrids.columns},
                **type_params,
                'NSubsets': -1,
                'DelaySetID': 'FixedV',
                }, name=grid_uid))
            grid_params['AlphaOffsets'] = (
                alpha_offsets if alpha_offsets is not None else grid_params.get('AlphaOffsets'))
            grids_params_uid_indexed[grid_uid] = grid_params

        # MC of 2S trajectory set
        if grid_type in ['MCDpDavg']:

            grid_params = {
                **grid_params_default,
                'MaxTransverseMomentum': 4,
                'MaxCalcTime': 86400/2,
                **grid_params,
                }

            traj_uids = grid_params['TrajUIDs']
            num_traj = int(1e6)
            grid_params['NTrajs'] = num_traj
            # num_trajs_per_job = int(25e3)
            # subset_scans = np.arange(1, int(np.ceil(num_traj/num_trajs_per_job))+1)
            # num_param_scans = len(subset_scans)
            num_param_scans = 1

            for traj_uid, num_trajs in zip(grid_params['TrajUIDs'], grid_params['NsTrajs']):
                dfSimGrids = dfSimGrids.append(pd.Series({
                    'Timestamp': datetime.datetime.utcnow(),
                    **obe_params,
                    **{
                        key: value for key, value in grid_params.items()
                        if key in dfSimGrids.columns},
                    'TrajUID': traj_uid,
                    'NSubsets': num_param_scans,
                    }, name=grid_uid))
                grid_params['NTrajs'] = num_trajs
                grids_params_uid_indexed[grid_uid] = grid_params

    # Big model (BM)
    elif grid_model == 'BigModel':

        obe_params = {
            **obe_params_defaults,
            'Solver': 'radau',
            'SolveAbsTol': 1e-13,
            'SolveRelTol': 5e-14,
            'SolveMaxIters': int(2e7),
            }

        # AVP (alpha angle - speed - 2S-nP laser power) scan
        if grid_type in ['AVP']:

            grid_params = {
                **grid_params_default,
                'MaxTransverseMomentum': 0,
                'MaxCalcTime': 86400,
                **grid_params,
                }

            dfSimGrids = dfSimGrids.append(pd.Series({
                'Timestamp': datetime.datetime.utcnow(),
                **obe_params,
                **{
                    key: value for key, value in grid_params.items()
                    if key in dfSimGrids.columns},
                'MaxTransverseMomentum': 0,
                'DeltapDecayAvg': '',
                'NDeltapDecayAvg': -1,
                'NSubsets': -1,
                'DelaySetID': 'FixedV',
                }, name=grid_uid))
            grids_params_uid_indexed[grid_uid] = grid_params


# Cast datatypes of DataFrame columns
dfSimGrids = SimGrids.cast_containers_to_data_format(
    dfSimGrids, SimGrids.df_id_grids)

# Assign OBEs
for i_grid, (grid_uid, grid) in enumerate(dfSimGrids.iterrows()):
    dfSimGrids.loc[grid_uid, 'OBEID'] = get_obe_id(grid)

job_id = 1
for grid_uid, grid in dfSimGrids.iterrows():
    print(f'Building metadata and jobs for grid \'{grid_uid}\'')

    grid_params = grids_params_uid_indexed[grid_uid]
    randomize_vxs = grid_params['RandomizeVxs']

    # Output hints
    if randomize_vxs:
        print(
            'Hint: Randomizing transverse velocities (vx)'
            +' (make sure this is what you want)')

    ## Build list of simulation scans
    # Init empty DataFrame for grid lines
    dfSimGridScans_template = SimGrids.df_templates['dfSimGridScans'].copy()
    dfSimGridScans = dfSimGridScans_template.copy()

    # Ensure that all list-like input parameters are arrays
    alphas = np.atleast_1d(grid_params.get('Alphas'))
    laser_powers_2snp = np.atleast_1d(grid_params['2SnPLaserPowers'])
    speeds = np.atleast_1d(grid_params.get('Speeds'))
    alpha_offsets = np.atleast_1d(grid_params.get('AlphaOffsets'))
    dpd2s = np.atleast_1d(grid_params.get('Dpd2s'))
    if np.any(~np.isnan(dpd2s.astype(float))):
        print(
            'Hint: `dpd2` (momentum change of back decay 2) is not set to NaN'
            +' (make sure this is what you want)')

    # Discrete momentum space (DMS) model
    if grid['Model'] == 'DMS':

        set_list = list(itertools.product(
            laser_powers_2snp,
            speeds,
            alpha_offsets,
            dpd2s,
            ))

        for i_set, (laser_power, speed, alpha_offset, dpd2) \
                in enumerate(set_list):

            # Find alpha offset angle for which transverse velocity corresponds to recoil velocity,
            # at which point two-photon resonance with level with opposite momentum occurs
            vr = atomic_properties[grid['FS']]['RecoilVelocity']
            if speed is not None:
                alpha_vr_rad = np.arcsin(vr/speed)
                # Array of alpha offsets around two-photon resonance
                alpha_lim_fine = np.round(
                    1e3*np.array([alpha_vr_rad-5e-4, alpha_vr_rad+5e-4]), alpha_round_dec_places)
            else:
                alpha_vr_rad = None
                alpha_lim_fine = None

            vxs_scans = None
            alphas_scans = None
            max_vx = None

            # Fixed 2S trajectory,
            # backdecay to 2S initial level either not included or included with fixed momentum
            # change (Dpd)
            if grid['Type'] in ['FixedV', 'FixedVDpDavg']:

                # Backdecay to 2S initial level included,
                # and averaged over momentum change upon backdecay (Dpd)
                # (grid type is 'FixedVDpDavg')
                # or
                # model does not include backdecay to 2S initial level
                if grid['Type'] in ['FixedVDpDavg'] or not grid['Backdecay']:

                    alphas_scans = [
                        [0, 25, 10],
                        # [0, 25, 251],
                        # [*alpha_lim_fine, np.ptp(alpha_lim_fine)/5+1],
                        ]

                    dpd1s_hbark = [np.nan]

                # Model includes backdecay to 2S initial level,
                # momentum change upon backdecay (Dpd) assumed to be fixed and must be defined
                else:

                    # alphas_scan = [
                    #     [0, 25, 10],
                    # ]

                    # dpd1s_hbark = [0.]

                    alphas_scans = [
                        [alpha_offset, alpha_offset, 1],
            #            [0, 25, 251],
            #            [*alpha_lim_fine, np.ptp(alpha_lim_fine)/10+1],
                        ]

                    dpd1_scans = [
                        [-1000, 1000, 11],
                        # [-1000, 1000, 201],
                        ]

                    # Fine scan of Dpd1 around resonance
                    dpd_in = np.sin(alpha_offset*1e-3)*speed/vr
                    dpd1_offset = -dpd_in
                    dpd1_step_size = 1e-3
                    dpd1_range = 4e-2
                    dpd1_offset = np.round(dpd1_offset/dpd1_step_size)*dpd1_step_size
                    dpd1s_offset = np.hstack((
                        np.linspace(
                            -dpd1_range/2+dpd1_offset,
                            dpd1_range/2+dpd1_offset,
                            int(np.ceil(dpd1_range/dpd1_step_size))+1)
                        ))
                    dpd1s = np.mod(dpd1s_offset+1, 2)-1
                    if -1 in dpd1s_offset:
                        dpd1s = np.append(dpd1s, [1])
                    dpd1s = np.unique(np.sort(dpd1s))
                    dpd1s_split = np.split(
                        dpd1s, np.where(np.diff(dpd1s) > 1.1*dpd1_step_size)[0]+1)

                    dpd1_scans = np.vstack((dpd1_scans, [
                        [np.min(dpd1s_)*1e3, np.max(dpd1s_)*1e3, len(dpd1s_)]
                        for dpd1s_ in dpd1s_split
                        ]))

                    dpd1s_hbark = np.zeros(0)
                    for i_param_scan, param_scan in enumerate(dpd1_scans):
                        dpd1s_ = np.linspace(*[int(elem) for elem in param_scan])
                        dpd1s_hbark_ = dpd1s_.astype(int)*1e-3
                        dpd1s_hbark = np.append(dpd1s_hbark, dpd1s_hbark_)

            # DMS model VxVP grid (transverse velocity (vx) - speed - 2S-nP laser power)
            elif grid['Type'] in ['VxVP']:

                max_vx_scan = np.round(
                    2*speeds.max()*np.sin(grid_params['MaxAlpha']*1e-3)+0.5)/2
                vxs_scans = [
                        [0.05, 0.8, 16],
                        [0.8, 4.0, 33],
                        [4.0, 10.0, 31],
                        [10.0, max_vx_scan, int((max_vx_scan-10.0)*2+1)],
                        [vr-0.15, vr+0.15, 2],
                        [vr-0.1, vr+0.1, 21],
                        [vr-0.04, vr+0.04, 41],
                        [vr-0.005, vr+0.005, 11],
                        [vr+0.3, vr+0.3, 1],
                    ]

                # Max. transverse velocity, as given by max. alpha angle, for next-higher speed
                higher_vs = speeds[speeds > speed]
                next_higher_v = (higher_vs.min() if len(higher_vs) > 0 else speed)
                max_vx = (next_higher_v)*np.sin(grid_params['MaxAlpha']*1e-3)

                dpd1s_hbark = [np.nan]

            # DMS model MCDpDavg grid: Monte Carlo of 2S trajectory set, which defines alpha angle.
            # However, an alpha offset can be included, which defines the misalignment between
            # the atomic beam (i.e., the 2S trajectory set) and the 2S-nP laser beam.
            # Backdecay to 2S initial level included,
            # and averaged over momentum change upon backdecay (Dpd).
            elif grid['Type'] in ['MCDpDavg']:

                dpd1s_hbark = [np.nan]

                alphas_scans = [[alpha_offset, alpha_offset, 1]]

            else:
                raise Exception(
                    f'Unknown grid type \'{grid["Type"]}\'')

            if vxs_scans is None and alphas_scans is not None:
                # Alpha angle is given, calculate transverse velocity

                alphas = np.zeros(0)
                for i_param_scan, param_scan in enumerate(alphas_scans):
                    alphas_ = np.linspace(
                        *[int(elem) for elem in param_scan])
                    # alpha_offsets_urad_ = np.array([
                    #     random.uniform(param_scan[0], param_scan[1])
                    #     for i in range(param_scan[2])])
                    alphas = np.append(alphas, alphas_)
                # Round alpha angle (here in units of mrad) to nrad
                alphas = np.round(alphas, alpha_round_dec_places)
                alphas = np.unique(alphas)

                dfSimGridScans_ = dfSimGridScans_template.copy()
                dfSimGridScans_[['AlphaOffset', 'DeltapDecay1']] = (
                    np.array(list(itertools.product(alphas, dpd1s_hbark))))
                if speed is not None:
                    dfSimGridScans_['Vx'] = (speed*np.sin(dfSimGridScans_['AlphaOffset']*1e-3))
                else:
                    dfSimGridScans_['Vx'] = np.nan

            elif alphas_scans is None and vxs_scans is not None:
                # Transverse velocity is given, calculate alpha angle

                vxs = np.zeros(0)
                for i_param_scan, param_scan in enumerate(vxs_scans):
                    if param_scan[2] < 1:
                        continue
                    if randomize_vxs:
                        vxs_ = np.array([
                            random.uniform(param_scan[0], param_scan[1])
                            for i in range(param_scan[2])])
                    else:
                        vxs_ = np.linspace(
                            *param_scan)
                    vxs = np.append(vxs, vxs_)
                vxs2 = vxs.copy()
                # Round transverse velocity to μm/s
                vxs = np.round(vxs, vx_round_dec_places)
                vxs = np.unique(vxs)

                # Only keep transverse velocities less or equal to lowest transverse velocity
                # above or equal to maximum transverse velocity, calculated above
                higher_vxs = vxs[vxs >= max_vx]
                next_higher_vx = (higher_vxs.min() if len(higher_vxs) > 0 else vxs.max())
                if max_vx is not None:
                    vxs = vxs[vxs <= next_higher_vx]

                dfSimGridScans_ = dfSimGridScans_template.copy()
                dfSimGridScans_[['Vx', 'DeltapDecay1']] = (
                    np.array(list(itertools.product(vxs, dpd1s_hbark))))
                dfSimGridScans_['AlphaOffset'] = np.round(
                    np.arcsin(dfSimGridScans_['Vx']/speed)*1e3, alpha_round_dec_places)

            else:
                raise Exception(
                    'Transverse velocity (vx) or transverse angle (alpha) not defined')

            dfSimGridScans_['DeltapDecay2'] = dpd2 if not None else np.nan
            dfSimGridScans_['GridUID'] = grid_uid
            dfSimGridScans_['JobID'] = job_id
            dfSimGridScans_['2SnPLaserPower'] = laser_power
            dfSimGridScans_['V'] = speed if not None else np.nan
            dfSimGridScans_['Subset'] = -1
            dfSimGridScans_['NTrajs'] = grid_params.get('NTrajs', 1)
            dfSimGridScans_.index = [str(uuid.uuid4()) for i in range(len(dfSimGridScans_))]
            dfSimGridScans_.index.name = 'SimScanUID'
            dfSimGridScans = dfSimGridScans.append(dfSimGridScans_, sort=False)

    # Big model (BM) AVP grid (alpha angle - speed - 2S-nP laser power)
    elif grid['Model'] == 'BigModel' and grid['Type'] == 'AVP':

        set_list = list(itertools.product(
            laser_powers_2snp,
            speeds,
            alphas,
            ))

        dfSimGridScans_ = dfSimGridScans_template.copy()
        dfSimGridScans_[['2SnPLaserPower', 'V', 'AlphaOffset']] = (
            np.array(set_list))
        dfSimGridScans_['AlphaOffset'] = np.round(
            dfSimGridScans_['AlphaOffset'], alpha_round_dec_places)
        dfSimGridScans_['Vx'] = np.round(
            dfSimGridScans_['V']*np.sin(dfSimGridScans_['AlphaOffset']*1e-3), vx_round_dec_places)
        dfSimGridScans_['DeltapDecay1'] = np.nan
        dfSimGridScans_['DeltapDecay2'] = np.nan
        dfSimGridScans_['GridUID'] = grid_uid
        dfSimGridScans_['JobID'] = job_id
        dfSimGridScans_['Subset'] = -1
        dfSimGridScans_['NTrajs'] = 1
        dfSimGridScans_.index = [str(uuid.uuid4()) for i in range(len(dfSimGridScans_))]
        dfSimGridScans_.index.name = 'SimScanUID'
        filenames_ = [
            f'V{scan["V"]:.0f}A{scan["AlphaOffset"]*1e1:.0f}P{scan["2SnPLaserPower"]*10:.0f}'
            for _, scan in dfSimGridScans_.iterrows()]
        dfSimGridScans_['FilenameLegacy'] = filenames_
        dfSimGridScans = dfSimGridScans.append(dfSimGridScans_, sort=False)

    # Drop duplicates from list of lines
    mask_duplicated = dfSimGridScans.duplicated(subset=[
        'GridUID', 'AlphaOffset', 'DeltapDecay1', 'DeltapDecay2', '2SnPLaserPower', 'V'])
    print(f'Removing {np.sum(mask_duplicated):d} duplicates')
    dfSimGridScans = dfSimGridScans[~mask_duplicated]

    # Add SimScanIID (numeric index)
    dfSimGridScans['SimScanIID'] = range(len(dfSimGridScans))

    # Add frequency list ID and filename to each scan
    mask_scans = dfSimGridScans['GridUID'] == grid_uid
    dfSimGridScans.loc[mask_scans, 'Filename'] = f'{grid_uid}.npz'
    if grid['CalcFreqSampling'] == 'Fixed':
        dfSimGridScans.loc[mask_scans, 'CalcFreqListID'] = grid['CalcFixedFreqListID']
    else:
        dfSimGrids.loc[grid_uid, 'CalcFixedFreqListID'] = ''
        dfSimGridScans.loc[mask_scans, 'CalcFreqListID'] = (
            dfSimGridScans.apply(
                lambda scan: pyha.def_line_sampling.get_freq_list(
                    grid['CalcFreqSampling'],
                    fixed_freq_list_id=grid['CalcFixedFreqListID'], scan=scan)[0],
                axis=1
                ))

    # Cast datatypes of DataFrame columns
    dfSimGridScans = SimGrids.cast_containers_to_data_format(
        dfSimGridScans, 'dfSimGridScans')

    ### Create jobs
    time_prefix = datetime.datetime.utcnow().strftime('%Y-%m-%d-%H-%M-%S')

    # Select cluster
    cluster = clusters[cluster_id]
    output_dir = cluster['OutputDir']
    rep_dir = cluster['RepDir']
    scripts_dir = pyhs.files.join_paths_posix(rep_dir, 'scripts')

    ## Local output folder
    dir_out = pyhs.files.join_paths_posix('build_jobs_out', grid_uid)

    #num_submit_files = int(np.ceil(num_param_scans*len(set_list)/max_jobs))
    num_submit_files = 1

    try:

        # Create or clear out directories
        dirs = ['', 'Jobs', 'Metadata', 'Scripts', 'Lines', 'Stats']
        dirs = [pyhs.files.join_paths_posix(dir_out, dir_) for dir_ in dirs]
        for dir_ in dirs:
            if not os.path.exists(dir_):
                os.makedirs(dir_)
            files = glob.glob(pyhs.files.join_paths_posix(dir_, '*'))
            for f in files:
                if os.path.isfile(f):
                    os.remove(f)

        # Create preparation file
        f_prepare = open(pyhs.files.join_paths_posix(dir_out, 'prepare_jobs.sh'), 'w', newline='\n')
        print('#!/bin/bash', file=f_prepare)
        for folder in ['Jobs', 'Lines', 'Logs', 'Source', 'Metadata', 'Scripts', 'Stats', 'OBE']:
            print(
                'mkdir -p '+pyhs.files.join_paths_posix(output_dir, grid_uid, folder),
                file=f_prepare)
        # Zip and copy contents of local copy of Julia OBE integrator repository
        print(
            'zip -r -q '
            +pyhs.files.join_paths_posix(
                output_dir, grid_uid, 'Source', 'obes2snpintegrationjulia.zip')
            +f' {rep_dir} -x \'*.git*\''
            , file=f_prepare)
        # Save SHA of head of local Julia OBE integrator git repository to file
        print(
            'git -C '
            +pyhs.files.join_paths_posix(rep_dir, '.git')
            +' rev-parse HEAD > '
            +pyhs.files.join_paths_posix(
                output_dir, grid_uid, 'Source', 'obes2snpintegrationjulia.sha')
            , file=f_prepare)
        # Copy OBE from Julia OBE integrator repository
        print(
            'cp -R '
            +pyhs.files.join_paths_posix(rep_dir, 'H2SnPOBEs/src/OBEs', grid['OBEID'], '*')
            +' '+pyhs.files.join_paths_posix(output_dir, grid_uid, 'OBE')
            , file=f_prepare)

        # Create post processing file
        post_filename = time_prefix+'_post.sh'
        f_post = open(pyhs.files.join_paths_posix(dir_out, post_filename), 'w', newline='\n')
        print('#!/bin/bash', file=f_post)
        print('chmod 777 '+post_filename, file=f_prepare)

        # Create submit file(s)
        f_submits = []
        for i_submit_file in range(num_submit_files):
            if i_submit_file == 0:
                submit_filename = 'submit_jobs.cmd'
            else:
                submit_filename = 'submit_jobs_{:d}.cmd'.format(i_submit_file+1)
            f_submits.append(
                open(pyhs.files.join_paths_posix(dir_out, submit_filename), 'w', newline='\n'))
            print('chmod 777 '+submit_filename, file=f_prepare)

        for i_job in range(1):

            job_id = i_job+i_job_offset+1
            print(
                f'Creating job {job_id} for grid \'{grid_uid}\''
                )

            batch_folder = 'Jobs'
            batch_filebase = time_prefix+'_' if prefix_time else ''
            batch_filebase += 'job_{:d}'.format(job_id)
            batch_filename = batch_filebase+'.job'
            log_filename = batch_filebase+'_${SLURM_JOB_ID}.log'
            f = open(
                pyhs.files.join_paths_posix(dir_out, batch_folder, batch_filename),
                'w', newline='\n')

            hours, remainder = np.divmod(grid_params['MaxCalcTime'], 3600)
            minutes, seconds = np.divmod(remainder, 60)

            ## Using bash
            print('#!/bin/bash -l', file=f)
            ## SLURM commands
            print(
                '#SBATCH -o '
                +pyhs.files.join_paths_posix(
                    output_dir, grid_uid, 'Logs', f'job_{job_id:d}_%j.out'),
                file=f)
            print(
                '#SBATCH -e '
                +pyhs.files.join_paths_posix(
                    output_dir, grid_uid, 'Logs', f'job_{job_id:d}_%j.err'),
                file=f)
            print('#SBATCH -D ./', file=f)
            print(f'#SBATCH -J {grid_uid}_{i_job+1:d}', file=f)
            if cluster.get('Partition') is not None:
                print(f'#SBATCH --partition={cluster["Partition"]}', file=f)
            # Number of full nodes; if a shared node is used, this should not be set
            # print('#SBATCH --nodes=1', file=f)
            # Number of tasks per node. Always 1, since Julia internally starts more tasks.
            # print('#SBATCH --ntasks-per-node=1', file=f)
            # Number of tasks if using shared node.
            # Always 1, since Julia internally starts more tasks.
            print('#SBATCH --ntasks=1', file=f)
            # Set the number of logical CPUs per task. If hyperthreading is not enabled, this
            # corresponds to the number of physical CPUs or cores, if it is enabled, this
            # corresponds to the number of logical CPUs.
            print(f'#SBATCH --cpus-per-task={grid["NCPUs"]:d}', file=f)
            # Enable hyperthreading, that is two threads per physical CPU/core by setting
            # ``ntasks-per-core`` to ``2``.
            # For MPCDF system, this defaults to 1 and not exporting this line results in
            # hyperthreading being disabled.
#            print(f'#SBATCH --ntasks-per-core=2', file=f)
            print(f'#SBATCH --mem={grid["NCPUs"]*cluster["MemPerWorker"]:d}', file=f)
            # Not necessary and discouraged on MPCDF Raven
            # print('#SBATCH --hint=multithread', file=f)
            print('#SBATCH --mail-type=none', file=f)
            print(f'#SBATCH --mail-user={username}@mpq.mpg.de', file=f)
            print(
                f'#SBATCH --time={int(hours):02d}:{int(minutes):02d}:{int(seconds):02d}'
                , file=f)
            print(f'#SBATCH --chdir={scripts_dir}', file=f)
            print(
                '\n'.join([
                    '',
                    '# Output Slurm parameters',
                    'echo Slurm: Job $SLURM_JOB_NAME with ID $SLURM_JOB_ID on node'
                    +' $SLURM_JOB_NODELIST',
                    '',
                    '# Load Julia',
                    'module purge',
                    f'module load {cluster["JuliaModule"]}',
                    '',
                    '# Set number of OMP threads to fit the number of available cpus,'
                    +' if applicable.',
                    'export OMP_NUM_THREADS=${SLURM_CPUS_PER_TASK}',
                    '',
                    ]), file=f)
            print('# Run the program:', file=f)

            print(
                'srun julia'
                +f' --project={cluster["ProjectDir"]}'
                +f' -p {grid["NThreads"]:d}'
                +' '+pyhs.files.join_paths_posix(
                    scripts_dir,
                    'process_job_cluster_mc.jl'
                    if grid['Type'] == 'MCDpDavg' else 'process_job_cluster.jl'
                    )
                +f' {grid_uid}'
                +(' --verbose' if verbose_output else '')
                +(f' --gc_workers {gc_workers}' if gc_workers is not None else '')
                +' >> '+pyhs.files.join_paths_posix(output_dir, grid_uid, 'Logs', log_filename)
                , file=f)

            f.close()

            print(
                'sbatch '+pyhs.files.join_paths_posix(batch_folder, batch_filename),
                file=f_submits[int(np.floor(i_job/max_jobs))])

            print(
                'cp '+pyhs.files.join_paths_posix(batch_folder, batch_filename)
                +' '+pyhs.files.join_paths_posix(output_dir, grid_uid, 'Jobs', batch_filename),
                file=f_prepare)

        # Write grids DataFrames to disk
        file_prefix = (time_prefix+'_' if prefix_time else '') + grid_uid
        filename_grids = file_prefix+'_grids.dat'
        filename_scans = file_prefix+'_scans.dat'
        mask_grids = dfSimGrids.index == grid_uid
        SimGrids.write_df_to_csv(
            dfSimGrids[mask_grids], SimGrids.df_id_grids,
            pyhs.files.join_paths_posix(dir_out, 'Metadata', filename_grids))
        print(f'Creating {len(dfSimGridScans):d} scans for grid \'{grid_uid}\'')
        SimGrids.write_df_to_csv(
            dfSimGridScans, 'dfSimGridScans',
            pyhs.files.join_paths_posix(dir_out, 'Metadata', filename_scans))
        # Copy metadata
        for filename in [filename_grids, filename_scans]:
            print(
                'cp '+pyhs.files.join_paths_posix('Metadata', filename)
                +' '+pyhs.files.join_paths_posix(output_dir, grid_uid, 'Metadata', filename),
                file=f_prepare)

        # Copy this Python script
        filename_script = 'build_jobs.py'
        filename_script_copy = time_prefix+'_'+filename_script
        shutil.copy2(
            filename_script, pyhs.files.join_paths_posix(dir_out, 'Scripts', filename_script))
        print(
            'cp '+pyhs.files.join_paths_posix('Scripts', filename_script)
            +' '+pyhs.files.join_paths_posix(output_dir, grid_uid, 'Scripts', filename_script),
            file=f_prepare)
        if submit_jobs_after_prep:
            print(
                './submit_jobs.cmd',
                file=f_prepare)

    finally:

        for f_submit in f_submits:
            f_submit.close()
        f_prepare.close()
        f_post.close()

# Activate project/environment used by main process, as e.g. set by `--project`
# option upon launch, on all workers
# Set `project_main` to currently active project of main process
project_main = Base.active_project()
# Broadcast variable `project_main` to workers
@eval @everywhere project_main=$project_main
# Import package manager on workers
@everywhere import Pkg
# Activate project `project_main` on workers
@everywhere Pkg.activate(project_main)

include("get_commandline_args.jl")
parsed_args = parse_commandline()

println("Using $(nprocs()-1) workers")

include("process_job.jl")

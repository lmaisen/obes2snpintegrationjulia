import DataFrames
import CSV
import NPZ
import Glob

traj_uid = "dphHl69w"
dir_trajs = "C:/Users/Lothar/Dropbox/MPQ/Calculations/Atom trajectories/Trajectories"
dir_traj_metadata = Base.Filesystem.joinpath(dir_trajs, "metadata")
dir_traj_sets = Base.Filesystem.joinpath(dir_trajs, "sets")

metadata_filepaths = Glob.glob("* Params.dat", dir_traj_metadata)
metadata_filenames = [
    Base.Filesystem.basename(filepath) for filepath in metadata_filepaths]
println("Found $(length(metadata_filepaths)) metadata file(s)")

# Load trajectory sets metadata
dfTrajParams = DataFrames.DataFrame()
for filepath in metadata_filepaths
    DataFrames.append!(
        dfTrajParams, DataFrames.DataFrame(CSV.File(filepath)))
end

# Find numerical index of requested trajectory set
grid_iid = findfirst(==(traj_uid), dfTrajParams.TrajUID)
if isnothing(grid_iid)
    error("Could not find metadata for trajectory set with UID \"$(traj_uid)\"")
end
traj_param = dfTrajParams[grid_iid, :]

# Load trajectory set from disk
filename_npz = Base.Filesystem.splitext(traj_param["Filename"])[1] * ".npz"
traj_set = NPZ.npzread(
    Base.Filesystem.joinpath(
        dir_traj_sets, filename_npz),
    ["Positions", "Vels", "2SExcProbs_IntDelaySum", "IoniProbs_IntDelaySum"]
    )
println(
    "Read $(size(traj_set["Positions"])[1]) trajectories"
    * " from \"$(filename_npz)\"")

exc_probs_2s_int_delay_sum = traj_set["2SExcProbs_IntDelaySum"]

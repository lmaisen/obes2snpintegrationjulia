using Distributed

parsed_args = Dict(
    "GridUID"=>"test_fixedv_dpd1avg_gq4",
	# "GridUID"=>"test_fixedv_dpd1avg_gq4_d2s6p",
	# "GridUID"=>"test_traj_mc_30M4PYA5_jobs",
    # "GridUID"=>"test_bm_2s4p_radau13",
	"JobID"=>1,
    # "GridResultsDir"=>"../../../Results/GridResults",
	# "TrajsDir"=>"C:/Users/Lothar/Dropbox/MPQ/Calculations/Atom trajectories/Trajectories",
	"GridResultsDir"=>"../test_grids",
	"TrajsDir"=>"../test_trajectories",
    "verbose"=>true,
    "gc_workers"=>10
    )

# Start workers
num_procs = 4
if num_procs-nprocs()+1 > 0
    println("Starting $(num_procs-nprocs()+1) additional workers")
    addprocs(num_procs-nprocs()+1)
end

# Activate project/environment used by main process, as e.g. set by `--project`
# option upon launch, on all workers
# Set `project_main` to currently active project of main process
project_main = Base.active_project()
# Broadcast variable `project_main` to workers
@eval @everywhere project_main=$project_main
# Import package manager on workers
@everywhere import Pkg
# Activate project `project_main` on workers
@everywhere Pkg.activate(project_main)

include("process_job.jl")

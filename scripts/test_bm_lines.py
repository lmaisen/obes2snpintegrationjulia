# -*- coding: utf-8 -*-
"""
Created on Mon Feb 15 15:10:05 2021

@author: Lothar Maisenbacher/MPQ

Test numerical integration of single lines of big model OBEs,
generated in Julia with test_bm.jl
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

import pyhs.gen

import pyh.fit

import pyha.def_line_sampling
import pyha.settings_default as sim_settings

pyhs.gen.set_up_command_line_logger()
logger = pyhs.gen.get_command_line_logger()

SimFitClass = pyh.fit.FitClass(sim_settings)

#%% Load Julia results

#filepath = 'Lines/bm2s4p12CDon/test_bm_radau_1.0e-10.npz'
filepath = 'Lines/bm2s4p12CDon/test_bm_Vern7_1.0e-10.npz'
#filepath = 'Lines/bm2s4p12CDon/31032021_test_bm_radau_1.0e-13_A5.npz'
#filepath = 'Lines/bm2s4p12CDon/31032021_test_bm_radau_1.0e-13_A5_w022.npz'
#filepath = 'Lines/bm2s4p12CDon/06042021_test_bm_2002_radau_1.0e-13.npz'

with np.load(filepath) as scans_lines:
    scans_detunings = scans_lines['Detunings']
    scans_signals = scans_lines['Signals']
    scans_populations = scans_lines['Populations']
logger.info(f'Loaded signals have a size of {scans_signals.nbytes/2**20:.2f} MB')
logger.info(f'Loaded populations have a size of {scans_populations.nbytes/2**20:.2f} MB')

# 2S-4P
fit_func_id = 'Lorentzian'
freq_list_id_fit_req = 'Span80MHzPoints79'
# Lyman-alpha-sigma- (low QI effect)
signal_column = 0
cfr_comparison = -553.481329147549445
# Lyman-gamma-sigma- (large QI effect)
#signal_column = 3
#cfr_comparison = 81324.49144168933
zero_detuning_column = 39
signal_comparison = 0.006493440083153124

# 2S-6P
#fit_func_id = 'Voigt'
#freq_list_id_fit_req = 'Span128MHzPoints255'
#freq_list_id_fit_req = 'Span80MHzPoints79'
# Lyman-epsilon-sigma- (large QI effect)
#signal_column = 9
#cfr_comparison = 26135.964049943886
#zero_detuning_column = 39
#signal_comparison = 0.1358113645550048

##%% Load result from C++/NR3

# 2S-4P
#grid = np.loadtxt('Lines/bm2s4p12CDon/V200A20P150_nr3_cluster')
grid = np.loadtxt('Lines/bm2s4p12CDon/V200A2P15_grid25')
# 2S-6P
#grid = np.loadtxt('Lines/bm2s6p12CDon/V200A5P300_grid29b')
#grid = np.loadtxt('Lines/bm2s6p12CDon/V200A5P30_grid6')

grid = grid
print(grid.shape)

##%%

fig_lines = plt.figure('Lines', constrained_layout=True)
fig_lines.clf()
gsl = gridspec.GridSpec(2, 1,
    width_ratios=[1],
    height_ratios=np.ones(2),
    figure=fig_lines,
    )
axl0 = fig_lines.add_subplot(gsl[0])
axl1 = fig_lines.add_subplot(gsl[1], sharex=axl0)

fig = plt.figure('Signal interpolation', constrained_layout=True)
fig.clf()
gs = gridspec.GridSpec(2, 1,
    width_ratios=[1],
    height_ratios=np.ones(2),
    figure=fig,
    )
ax0 = fig.add_subplot(gs[0])
ax1 = fig.add_subplot(gs[1], sharex=ax0)

active_fit_func_ids = (
    []
    + ['Lorentzian']
#    + ['Lorentzian', 'FanoLorentzian', 'Voigt', 'FanoVoigt']
    + ['Voigt']
#    + ['FanoVoigt']
    + ['VoigtDoublet']
#    + ['VoigtDoubletEqualAmps']
    )

##%% Plot and fit single line from grid

# Get list of detunings to fit
freq_list_id, freq_list = pyha.def_line_sampling.get_freq_list(
    'Fixed', freq_list_id_fit_req)
detunings_fit_req = freq_list['Freqs']

mask_detunings_grid = np.in1d(grid[:, 0], detunings_fit_req)
detunings_grid = grid[:, 0][mask_detunings_grid]
yl_grid = grid[mask_detunings_grid, signal_column+1]

detunings = scans_detunings

mask_detunings = np.in1d(detunings, detunings_fit_req)
detunings_fit = detunings[mask_detunings]
ylc = scans_signals[mask_detunings, signal_column]

axl0.cla()
axl1.cla()
axl1.set_xlabel('Detuning (Hz)')
axl0.set_ylabel('Signal')
axl1.set_ylabel('Interpolation error (%)')

axl0.plot(
    detunings_fit,
    ylc,
    linestyle='-', marker='d')

axl0.plot(
    detunings_grid,
    yl_grid,
    linestyle='-', marker='d')

line_fits_c = SimFitClass.Fit.fitLine(
    detunings_fit, ylc, np.ones(len(detunings_fit)),
    out_func=logger.info,
    fit_func_ids=active_fit_func_ids,
    )

line_fits_grid = SimFitClass.Fit.fitLine(
    detunings_grid, yl_grid, np.ones(len(detunings_grid)),
    out_func=logger.info,
    fit_func_ids=active_fit_func_ids,
    )

cfr_c = line_fits_c[fit_func_id]["Popt"][0]
cfr_g = line_fits_grid[fit_func_id]["Popt"][0]
print(f'{fit_func_id} fit, using freq. list {freq_list_id_fit_req}')
print(f'Calculated: {cfr_c:.10f} (abs. {(cfr_comparison-cfr_c):.2e}, rel. {(cfr_comparison-cfr_c)/cfr_comparison:.2e})')
print(f'C++/NR3: {cfr_g:.10f} (abs. {(cfr_comparison-cfr_g):.2e}, rel. {(cfr_comparison-cfr_g)/cfr_comparison:.2e})')

print(
  f'Calculated: Zero detuning signal comparison: {(signal_comparison-ylc[zero_detuning_column])/signal_comparison:.2e}'
  )
print(
  f'C++/NR3: Zero detuning signal comparison: {(signal_comparison-yl_grid[zero_detuning_column])/signal_comparison:.2e}'
  )

# -*- coding: utf-8 -*-
"""
Created on Mon Feb 15 15:10:05 2021

@author: Lothar Maisenbacher/MPQ

Load and fit calculated grids.
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import pandas as pd
import os

import pyh.fit
import pyhs.gen
import pyha.sim_grids
import pyha.settings_default as sim_settings

pyhs.gen.set_up_command_line_logger()
logger = pyhs.gen.get_command_line_logger()

SimFitClass = pyh.fit.FitClass(sim_settings)

dir_grids = '../test_grids'

#%% Load interpolation grid

grid_uid = 'test_hd_fixedv_dpdscan_dpd2s'

with np.load(os.path.join(dir_grids, f'{grid_uid}/Lines/{grid_uid}.npz')) as scans_lines:
    scans_detunings = scans_lines['Detunings']
    scans_signals = scans_lines['Signals']
    scans_populations = scans_lines['Populations']
logger.info(f'Loaded signals have a size of {scans_signals.nbytes/2**20:.2f} MB')
logger.info(f'Loaded populations have a size of {scans_populations.nbytes/2**20:.2f} MB')

SimGrids = pyha.sim_grids.SimGrids()
SimGrids.load_grids([os.path.join(dir_grids, grid_uid)])
dfGrids = SimGrids.dfSimGrids
grid = dfGrids.loc[grid_uid]
dfScans = SimGrids.dfSimGridScans
# dfScans.sort_values(by='AlphaOffset', inplace=True)
#dfScans = dfScans[dfScans['2SnPLaserPower'] == 30.]

# Big model: Lyman-epsilon-sigma- (large QI effect)
# signal_column = 9
# fit_func_id = 'Voigt'

# DMS: Lyman decays
signal_column = 0
fit_func_id = 'Voigt'

#%% Prepare figures

fig_lines = plt.figure('Lines', constrained_layout=True)
fig_lines.clf()
gsl = gridspec.GridSpec(2, 1,
    width_ratios=[1],
    height_ratios=np.ones(2),
    figure=fig_lines,
    )
axl0 = fig_lines.add_subplot(gsl[0])
axl1 = fig_lines.add_subplot(gsl[1], sharex=axl0)

active_fit_func_ids = (
    []
    + ['Lorentzian']
#    + ['Lorentzian', 'FanoLorentzian', 'Voigt', 'FanoVoigt']
    + ['Voigt']
#    + ['FanoVoigt']
    + ['VoigtDoublet']
#    + ['VoigtDoubletEqualAmps']
    )

#%% Plot and fit single line from grid

scan = dfScans.iloc[0]
detunings = scans_detunings[scan['SimScanIID'], :]
ylc = scans_signals[scan['SimScanIID'], :, signal_column]

axl0.cla()
axl1.cla()
axl1.set_xlabel('Detuning (Hz)')
axl0.set_ylabel('Signal')
axl1.set_ylabel('Interpolation error (%)')

axl0.plot(
    detunings,
    ylc,
    linestyle='-', marker='d')

line_fits_c = SimFitClass.Fit.fit_line(
    detunings, ylc, np.ones(len(detunings)),
    out_func=logger.info,
    fit_func_ids=active_fit_func_ids,
    )

fit_func = SimFitClass.Fit.fit_funcs[fit_func_id]
sr_fit = SimFitClass.Fit.fit_result_to_dict(line_fits_c[fit_func_id], fit_func)

print(scan[['Vx', 'V', '2SnPLaserPower', 'DeltapDecay1', 'DeltapDecay2']])
print(
    '{}:\nCFR = {:.12f} Hz\nGamma = {:.3e} Hz\nAmp = {:.5} photons\nAmp1 = {:.5} photons'
    .format(
        fit_func_id,
        sr_fit['CFR_Value'],
        sr_fit.get('Gamma_Value', np.nan),
        sr_fit.get('Amp_Value', np.nan),
        sr_fit.get('Amp1_Value', np.nan),
        ))

using ArgParse

"""
Parse command line arguments and return them as dict.
"""
function parse_commandline()
    s = ArgParseSettings()

    @add_arg_table! s begin
        "GridUID"
            help = "UID of grid to calculate"
            arg_type = String
            required = true
        "--JobID"
            help = "Job ID of grid to process"
            arg_type = Int
            default = 1
        "--GridResultsDir"
            help = "Absolute or relative path to directory where grid results are stored"
            arg_type = String
            default = "/u/lmaisen/GridResults"
        "--TrajsDir"
            help = "Absolute or relative path to directory where trajectory sets are stored"
            arg_type = String
            default = "/u/lmaisen/Trajectories"
        "--verbose"
            help = "Print verbose output"
            action = :store_true
        "--gc_workers"
            help = "n: garbage collect on workers after every n-th scan"
            arg_type = Int
            default = 10
    end

    return parse_args(s)
end

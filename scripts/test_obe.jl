using Printf
using Plots
using DifferentialEquations
import NPZ

dir_user_pkg = Base.Filesystem.joinpath(@__DIR__, "../")
if !(dir_user_pkg in LOAD_PATH)
    push!(LOAD_PATH, dir_user_pkg)
end
import H2SnPOBEs

obe_id = "dms2s6p12CDonNp4BDonNDpD1"
# obe_id = "bm2s4p12CDon"
# obe_id = "H2SnPStandingWave"
obe = H2SnPOBEs.OBEs.set_obe(obe_id)

alpha_offset = 2.
v = 200.
w0 = 2.2e-3
z_min = -7.5e-3
start_pos = Array([0, 0, z_min])
dpd1 = -1.
laser_power = 30e-6

p = H2SnPOBEs.OBEParameters()
H2SnPOBEs.OBEs.set_obe_parameters!(obe, p)

# H2SnPOBEs.calc_derived_params!(p)
p.Isotope = "H"
p.FS = "6P12"
p.CalcFreqSampling = "Fixed"
p.CalcFixedFreqListID = "Span128MHzPoints256"
p.CalcFreqListID = p.CalcFixedFreqListID
p.AlphaOffset = alpha_offset
p.StartPos = start_pos
p.V = v
H2SnPOBEs.set_velocity!(p)
p.LaserWaistRadius = w0
p.DeltapDecay1 = dpd1
p.LaserBeamProfile = "Gaussian"
p.Detuning = 0.
p.LaserPower = laser_power
H2SnPOBEs.set_intensity!(p)
# Set detected signal to Lyman decays only
p.gamma_s = p.gamma_l

detunings = H2SnPOBEs.get_freq_list(p)
t_max = -2*z_min/p.Vel[3]

tspan = (0.0, t_max)
initial_state = obe.get_initial_state(p)
signal_columns = [
    obe.signals[signal]["EquationIndex"]
    for signal in obe.info["SignalsToSave"]]
num_signals = length(obe.info["SignalsToSave"])
population_columns = [
    obe.states[state]["PopulationEquationIndex"]
    for state in obe.info["PopulationsToSave"]]
num_populations = length(obe.info["PopulationsToSave"])

signal_index = obe.info["NEqs"]-obe.info["NSignalEquations"]+1
prob = DifferentialEquations.ODEProblem(
    obe.derivs, initial_state, tspan,
    p)

# abs_tol = 1e-8
abs_tol = 1e-10
rel_tol = abs_tol/2
dt = 1e-12
maxiters=2e7

# Integration at single detuning
# start_time = time()
# sol = DifferentialEquations.solve(
#     prob, DifferentialEquations.Vern7(),
#     reltol=rel_tol, abstol=abs_tol, dt=dt, saveat=t_max, maxiters=maxiters)
# print("")
# println("Integration took $(@sprintf("%.2f", time()-start_time)) s")
signal_comparison = 0.006493440083153124
# # println(sol.u[end][signal_columns[1]])
# println(@sprintf(
#     "%.2e", (signal_comparison-sol.u[end][signal_columns[1]])/signal_comparison))
#
# # Save results for single detuning
# NPZ.npzwrite(
#     Base.Filesystem.joinpath(
#         "Lines", "test_obe_0.npz"),
#     Dict(
#         "Detunings" => p.Detuning, "Signals" => sol.u[end][signal_columns],
#         "Populations" => sol.u[end][population_columns]))

# using ODEInterfaceDiffEq

function test_integration(solver, rel_tol=rel_tol, abs_tol=abs_tol)
    start_time = time()
    sol2 = DifferentialEquations.solve(
        prob, solver(),
        reltol=rel_tol, abstol=abs_tol, dt=dt, saveat=t_max, maxiters=maxiters)
    print("")
    println("Integration took $(@sprintf("%.2f", time()-start_time)) s")
    # println(sol2.u[end][signal_columns[1]])
    println(@sprintf(
        "%.2e", (signal_comparison-sol2.u[end][signal_columns[1]])/signal_comparison))
        # println(@sprintf(
        #     "%.2e", (sol.u[end][signal_columns[1]]-sol2.u[end][signal_columns[1]])/sol.u[end][signal_columns[1]]))
    return sol2
end

sol2 = test_integration(DifferentialEquations.Vern7)

start_time = time()
a = zeros(length(detunings), obe.info["NEqs"])
for i = 1:length(detunings)
    p.Detuning = detunings[i]
    prob_ = DifferentialEquations.ODEProblem(
        obe.derivs, initial_state, tspan,
        p)
    sol_ = DifferentialEquations.solve(
        prob_, DifferentialEquations.Vern7(),
        reltol=5e-11, abstol=1e-10, dt=1e-12, saveat=t_max)
    a[i, :] = sol_.u[end]
end
print("")
println("Integration took $(@sprintf("%.2f", time()-start_time)) s")
println(a[1, signal_index])

# Plot line
# h = plot(
#     detunings, a[:, signal_index],
#     marker = :d, lw = 3)
# display(h)

# Write results to file
NPZ.npzwrite(
    Base.Filesystem.joinpath("../Lines", "data.npz"),
    Dict("Detunings" => detunings, "Signal" => a[:, signal_index]))

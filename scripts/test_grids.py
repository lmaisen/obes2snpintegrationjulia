# -*- coding: utf-8 -*-
"""
Created on Mon Feb 15 15:10:05 2021

@author: Lothar Maisenbacher/MPQ

Test numerical integration of grids by fitting the scans and comparing the results.
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import os

import pyh.fit
import pyhs.gen

# pyha
import pyha.sim_grids
import pyha.settings_default as sim_settings
import pyha.def_line_sampling

pyhs.gen.set_up_command_line_logger()
logger = pyhs.gen.get_command_line_logger()

SimFitClass = pyh.fit.FitClass(sim_settings)

dir_grids = '../../../Results/GridResults'
# dir_grids = '../test_grids'

# H 2S-4P BM: Grid UID 'test_bm_2s4p_radau13'
# grid_uid = 'test_bm_2s4p_radau13'
# fit_func_id = 'Lorentzian'
# freq_list_id_fit_req = 'Span80MHzPoints79'
# zero_detuning_column = 39
# # Lyman-alpha-sigma- (low QI effect)
# signal_column = 0
# cfr_comparison = -553.483130847408
# signal_comparison = 0.006493440083
# # Lyman-gamma-sigma- (large QI effect)
# # signal_column = 3
# # cfr_comparison = 81324.490184260794
# # signal_comparison = 0.129434746901

# H 2S-6P BM: Grid UID 'test_bm_2s6p_radau13'
# grid_uid = 'test_bm_2s6p_radau13'
# fit_func_id = 'Voigt'
# # freq_list_id_fit_req = 'Span128MHzPoints255'
# freq_list_id_fit_req = 'Span80MHzPoints79'
# # Lyman-epsilon-sigma- (large QI effect)
# signal_column = 9
# cfr_comparison = 26135.960489599231
# zero_detuning_column = 39
# signal_comparison = 0.135811364510

# H 2S-6P DMS: Grid UID 'test_fixedv_dpd1avg_gq4'
# grid_uid = 'test_fixedv_dpd1avg_gq4'
# fit_func_id = 'Lorentzian'
# freq_list_id_fit_req = None
# # Lyman
# signal_column = 0
# zero_detuning_column = 14
# cfr_comparison = -577.863839203103
# signal_comparison = 0.409430216241

# H 2S-6P DMS trajectory set MC: Grid UID 'test_traj_mc_30M4PYA5_jobs'
# grid_uid = 'test_traj_mc_30M4PYA5_jobs'
# fit_func_id = 'Lorentzian'
# freq_list_id_fit_req = None
# # Lyman
# signal_column = 0
# zero_detuning_column = 14
# cfr_comparison = -5695.591208158981
# signal_comparison = 0.006732311750

# D 2S-6P DMS: Grid UID 'test_fixedv_dpd1avg_gq4_d2s6p'
grid_uid = 'test_fixedv_dpd1avg_gq4_d2s6p'
fit_func_id = 'Lorentzian'
freq_list_id_fit_req = None
# Lyman
signal_column = 0
zero_detuning_column = 14
cfr_comparison = -1165.288696672504
signal_comparison = 0.411080049732

#%% Load calculated grid

SimGrids = pyha.sim_grids.SimGrids()
SimGrids.load_grids([os.path.join(dir_grids, grid_uid)])

dfGrids = SimGrids.dfSimGrids
grid = dfGrids.loc[grid_uid]
obe = SimGrids.obes[grid_uid]

dfScans = SimGrids.dfSimGridScans
dfScans['NumericIndex'] = range(0, len(dfScans))

# Load calculated scans
scans_detunings, scans_signals, scans_populations = (
    SimGrids.load_grid_scans(grid_uid))
num_detunings = scans_detunings.shape[1]

# Get list of detunings
# freq_list_id, freq_list = pyha.def_line_sampling.get_freq_list(
#     grid['CalcFreqSampling'], grid['CalcFixedFreqListID'])
# detunings = freq_list['FreqsUnique']

# Delay set ID 'FixedV' corresponds to no actual delays present,
# but instead scans that correspond to a fixed atom speed
fixed_v = grid['DelaySetID'] == 'FixedV'

#%% Output OBE metadata

if obe is not None:
    print(
        f'Grid calculated with OBE \'{obe["Info"]["OBEID"]}\''
        +f' on {SimGrids.dfSimGridScanStats.Timestamp.mean()}'
        +f', and derived with derivation script version {obe["Info"]["DerivationScriptVersion"]}'
        +f' with Mathematica version {obe["Info"]["DerivationMathematicaVersion"]}'
        +f' on OS {obe["Info"]["DerivationOS"]} on {obe["Info"]["DerivationTimestamp"]}.'
        )

#%% Prepare figures

fig_lines = plt.figure('Lines', constrained_layout=True)
fig_lines.clf()
gsl = gridspec.GridSpec(
    2, 1,
    width_ratios=[1],
    height_ratios=np.ones(2),
    figure=fig_lines,
    )
axl0 = fig_lines.add_subplot(gsl[0])
axl1 = fig_lines.add_subplot(gsl[1], sharex=axl0)

active_fit_func_ids = (
    []
    + ['Lorentzian']
    + ['Voigt']
    + ['VoigtDoublet']
    )

#%% Plot and fit single line from grid

scan_iid = 0
delay_iid = 0
scan = dfScans.iloc[scan_iid]

# Get list of detunings to fit
detunings = scans_detunings[scan['NumericIndex']]
if freq_list_id_fit_req is not None:
    freq_list_id, freq_list = pyha.def_line_sampling.get_freq_list(
        'Fixed', freq_list_id_fit_req)
    detunings_fit_req = freq_list['Freqs']
    mask_detunings = np.in1d(detunings, detunings_fit_req)
else:
    mask_detunings = np.ones(len(detunings), dtype=bool)
detunings_fit = detunings[mask_detunings]
if fixed_v:
    ylc = scans_signals[scan['NumericIndex'], mask_detunings, signal_column]
else:
    ylc = scans_signals[scan['NumericIndex'], mask_detunings, signal_column, delay_iid]

axl0.cla()
axl1.cla()
axl1.set_xlabel('Detuning (Hz)')
axl0.set_ylabel('Signal')

axl0.plot(
    detunings_fit,
    ylc,
    linestyle='-', marker='o', markersize=3)

line_fits_c = SimFitClass.Fit.fit_line(
    detunings_fit, ylc, np.ones(len(detunings_fit)),
    out_func=logger.info,
    fit_func_ids=active_fit_func_ids,
    )
fit_result = line_fits_c[fit_func_id]
sr_fit = SimFitClass.Fit.fit_result_to_series(
    fit_result, SimFitClass.fit_funcs[fit_func_id])
cfr_c = line_fits_c[fit_func_id]["Popt"][0]

print(f'{fit_func_id} fit, using freq. list {freq_list_id_fit_req}')
print(scan[['AlphaOffset', 'V', '2SnPLaserPower', 'DeltapDecay1', 'DeltapDecay2']])
print(f'Calculated CFR from {fit_func_id} fit: {cfr_c:.12f} Hz')
print(
    f'Comparison of CFR with reference value: {cfr_comparison:.12f}'
    +f' (abs. {cfr_comparison-cfr_c:.2e}, rel. {(cfr_comparison-cfr_c)/cfr_comparison:.2e})')
print(f'Calculated signal at zero detuning: {ylc[zero_detuning_column]:.12f}')
print(
  'Comparison of signal at zero detuning with reference value:'
  +f' abs. {signal_comparison-ylc[zero_detuning_column]:.2e}'
  +f', rel. {(signal_comparison-ylc[zero_detuning_column])/signal_comparison:.2e}'
  )
print(f'Calculated amplitude from {fit_func_id} fit: {sr_fit["Amp_Value"]:.5f}')

using SharedArrays
using Printf
using Statistics
using Einsum
import Dates
import DataFrames
import CSV
import NPZ
import Glob

# Load modules for all workers
@everywhere dir_user_pkg = Base.Filesystem.joinpath(@__DIR__, "../")
@everywhere if !(dir_user_pkg in LOAD_PATH)
    push!(LOAD_PATH, dir_user_pkg)
end
@everywhere using H2SnPOBEs

# Input arguments
grid_uid = parsed_args["GridUID"]
job_id = parsed_args["JobID"]
grids_dir = parsed_args["GridResultsDir"]
verbose = parsed_args["verbose"]
garbage_collect_on_workers = parsed_args["gc_workers"]

# Load grid metadata and metadata of grid scans
grid, dfScans_all_jobs = H2SnPOBEs.Grids.load_grid(grids_dir, grid_uid)
# Select scans for requested job ID
dfScans = DataFrames.filter(:"JobID" => isequal(job_id), dfScans_all_jobs)
println(
	"Processing job $(job_id) of grid with UID \"$(grid_uid)\"" *
	", containing $(DataFrames.nrow(dfScans)) scan(s)")

# Set OBEs
obe_id = grid["OBEID"]
obe = H2SnPOBEs.OBEs.set_obe(obe_id)

# Set OBE parameters for this grid
p = H2SnPOBEs.OBEParameters()
H2SnPOBEs.OBEs.set_obe_parameters!(obe, p)
p.Isotope = grid["Isotope"]
p.FS = grid["FS"]
p.CalcFreqSampling = grid["CalcFreqSampling"]
if p.CalcFreqSampling == "Fixed"
    p.CalcFixedFreqListID = grid["CalcFixedFreqListID"]
end
p.LaserWaistRadius = grid["2SnPLaserWaistRadius"]
p.LaserBeamProfile = grid["2SnPLaserBeamProfile"]
z_min = grid["DetectionBoundaryStart"]
z_max = grid["DetectionBoundaryEnd"]
p.StartPos = Array([0, 0, z_min])
p.SolveAbsTol = grid["SolveAbsTol"]
p.SolveRelTol = grid["SolveRelTol"]
p.SolveInitialStepSize = grid["SolveInitialStepSize"]
p.SolveMaxIters = grid["SolveMaxIters"]
p.Solver = grid["Solver"]

initial_state = obe.get_initial_state(p)
signal_columns = [
    obe.signals[signal]["EquationIndex"]
    for signal in obe.info["SignalsToSave"]]
num_signals = length(obe.info["SignalsToSave"])
population_columns = [
    obe.states[state]["PopulationEquationIndex"]
    for state in obe.info["PopulationsToSave"]]
num_populations = length(obe.info["PopulationsToSave"])
# Number of momentum-changing back decays
num_delta_p_decays = obe.info["NDeltapDecay"]

# Determine whether or not to use trajectory set
traj_uid = grid["TrajUID"]
use_trajs = ~isempty(traj_uid)

# Load and prepare trajectory set
if use_trajs
	dir_trajs = parsed_args["TrajsDir"]
	dir_traj_metadata = Base.Filesystem.joinpath(dir_trajs, "metadata")
	dir_traj_sets = Base.Filesystem.joinpath(dir_trajs, "sets")

	# Scan for files containing trajectory sets metadata
	metadata_filepaths = Glob.glob("* Params.dat", dir_traj_metadata)
	metadata_filenames = [
	    Base.Filesystem.basename(filepath) for filepath in metadata_filepaths]
	println("Found $(length(metadata_filepaths)) trajectory set metadata file(s)")

	# Load trajectory sets metadata
	dfTrajParams = H2SnPOBEs.Trajs.load_trajs_metadata(metadata_filepaths)

	# Find numerical index of requested trajectory set
	grid_iid = DataFrames.findfirst(==(traj_uid), dfTrajParams.TrajUID)
	if isnothing(grid_iid)
	    error("Could not find metadata for trajectory set with UID \"$(traj_uid)\"")
	end
	traj_param = dfTrajParams[grid_iid, :]
	num_delays = traj_param["NDelays"]

	# Load trajectory set from disk
	traj_set = H2SnPOBEs.Trajs.load_traj_set(traj_uid, traj_param, dir_traj_sets)
	positions = traj_set["Positions"]
	vels = traj_set["Vels"]
	exc_probs_2s_int_delay_sum = traj_set["2SExcProbs_IntDelaySum"]

	# Find AVP of trajectory set
	alphas, vs, offsets, powers_scaling = H2SnPOBEs.Trajs.convert_trajs_to_avp(
		positions, vels, grid["DetectionDistance"], grid["2SnPLaserWaistRadius"])
else
	num_delays = 1

	alphas = [NaN]
	vs = [NaN]
	powers_scaling = [1.]
end

# Prepare DataFrame for numerical statistics
dfStats = DataFrames.DataFrame(
    SimScanUID=dfScans[:, "SimScanUID"],
    SolveFEvs=zeros(DataFrames.nrow(dfScans)),
    ScanCalcTime=zeros(DataFrames.nrow(dfScans)),
    Timestamp=Array{String}(undef, DataFrames.nrow(dfScans)),
    )
freq_sampling_metadata = H2SnPOBEs.get_freq_sampling_metadata(p)
num_detunings = freq_sampling_metadata["NFreqsUnique"]
# Initialize arrays shared with workers
signals = SharedArray{Float64}(num_detunings, num_signals)
populations = SharedArray{Float64}(num_detunings, num_populations)
fevs = SharedArray{Float64}(num_detunings)
# Initialize local arrays, only present on main process
scans_detunings = zeros(DataFrames.nrow(dfScans), num_detunings)
scans_signals = zeros(DataFrames.nrow(dfScans), num_detunings, num_signals, num_delays)
scans_populations = zeros(DataFrames.nrow(dfScans), num_detunings, num_populations, num_delays)
start_time_calc = time()
start_time_lines = time()
num_lines_calc = 0
for (i_scan, scan) in enumerate(DataFrames.eachrow(dfScans))
    if verbose
        println(
			"Processing simulation scan $(i_scan) with UID \"$(scan.SimScanUID)\"")
    end

	p.CalcFreqListID = scan["CalcFreqListID"]
	detunings = H2SnPOBEs.get_freq_list(p)
	scans_detunings[i_scan, :] = detunings

	# dc electric field strength (V/m) from scan metadata
	p.DCEFieldx = scan["DCEFieldx"]
	p.DCEFieldy = scan["DCEFieldy"]
	p.DCEFieldz = scan["DCEFieldz"]

	# Get points at which to average over back decay momentum change
	# (only of relevance to DMS model).
	# If only n momentum-changing back decays are included in the OBE used, for back decay m > n
	# the momentum change is set to NaN and the loops for m > n below reduce to a single execution.
	if num_delta_p_decays > 0
		num_dpd1_points, dpd1_nodes, dpd1_weights = H2SnPOBEs.get_dpd_points(
			grid, scan, obe.info, n=1)
	else
		num_dpd1_points = 1
		dpd1_nodes = NaN
		dpd1_weights = [1.]
	end
	if num_delta_p_decays > 1
		num_dpd2_points, dpd2_nodes, dpd2_weights = H2SnPOBEs.get_dpd_points(
			grid, scan, obe.info, n=2)
	else
		num_dpd2_points = 1
		dpd2_nodes = NaN
		dpd2_weights = [1.]
	end
	if num_delta_p_decays > 2
		error(
			"Only up to two momentum-changing back decays, as given by `NDeltapDecay`," *
			" currently implemented")
	end

	# If using a trajectory set as input, use speed and alpha angle for the trajectories as input.
	# Additionally, an offset on the alpha angle as defined for each scan in the metadata is added.
	# If not using a trajectory set as input, use speed and alpha angle as defined for each scan
	# in the scan metadata.
	if use_trajs
		alphas_offset = alphas .+ scan["AlphaOffset"]
		i_traj_first = scan["TrajOffset"]+1
		i_traj_last = scan["TrajOffset"]+scan["NTrajs"]
	    println("Processing subset $(i_traj_first) through $(i_traj_last) of trajectories")
	else
		global vs = [scan["V"]]
		alphas_offset = [scan["AlphaOffset"]]
		i_traj_first = 1
		i_traj_last = 1
	end
	num_trajs = i_traj_last-i_traj_first+1

	scans_fevs = zeros(num_detunings)
    scan_calc_time = 0.
	for (i_traj, (v, alpha, power_scaling)) in enumerate(zip(
			vs[i_traj_first:i_traj_last],
			alphas_offset[i_traj_first:i_traj_last],
			powers_scaling[i_traj_first:i_traj_last]))
		i_traj_set = i_traj+i_traj_first-1
		if verbose & use_trajs & (i_traj % 10 == 0)
	        println("Processing trajectory $(i_traj) of subset ($(i_traj_set) of set)")
	    end

		laser_power_2snp = power_scaling*scan["2SnPLaserPower"]

	    p.LaserPower = 1e-6*laser_power_2snp
	    H2SnPOBEs.set_intensity!(p)
	    p.V = v
	    p.AlphaOffset = alpha
	    H2SnPOBEs.set_velocity!(p)
	    t_max = (z_max-z_min)/p.Vel[3]
	    tspan = (0.0, t_max)

	    start_time_line = time()
	    if num_lines_calc == 1
	        global start_time_lines = time()
	    end

		traj_signals = zeros(num_detunings, num_signals)
		traj_populations = zeros(num_detunings, num_populations)

		for (i_dpd2, (dpd2_node, dpd2_weight)) in enumerate(zip(dpd2_nodes, dpd2_weights))
			p.DeltapDecay2 = dpd2_node
			for (i_dpd1, (dpd1_node, dpd1_weight)) in enumerate(zip(dpd1_nodes, dpd1_weights))
				p.DeltapDecay1 = dpd1_node

				# Broadcast OBE parameters `p`
				# (otherwise local changes to fields of `p` will not propagate to the helpers)
				@eval @everywhere p=$p

				@sync @distributed for i = 1:num_detunings
					p.Detuning = detunings[i]
					sol_ = H2SnPOBEs.Solver.solve(
						obe, p, initial_state, tspan)
					signals[i, :] = sol_.u[end][signal_columns]
					populations[i, :] = sol_.u[end][population_columns]
					fevs[i] = sol_.destats.nf
				end

				traj_signals[:, :] += signals*dpd1_weight*dpd2_weight
				traj_populations[:, :] += populations*dpd1_weight*dpd2_weight
				scans_fevs += fevs
			end
		end

		# Add weighting of delays with 2S excitation probability
		# If not using a trajectory set as input, this has no meaning and a weighting of one is used
		if use_trajs
			exc_prob_2s_int_delay_sum = exc_probs_2s_int_delay_sum[i_traj_set, :]
		else
			exc_prob_2s_int_delay_sum = [1.]
		end
		@einsum (
			scan_signals[i, j, k] :=
			traj_signals[i, j] * exc_prob_2s_int_delay_sum[k])
		@einsum (
			scan_populations[i, j, k] :=
			traj_populations[i, j] * exc_prob_2s_int_delay_sum[k])
		scans_signals[i_scan, :, :, :] += scan_signals
		scans_populations[i_scan, :, :, :] += scan_populations

		traj_calc_time = time()-start_time_line
	    scan_calc_time += traj_calc_time
	    if verbose & use_trajs & (i_traj % 10 == 0)
	        print("")
	        println("Integration of trajectory took $(@sprintf("%.2f", traj_calc_time)) s")
	        println("System memory remaining: $(@sprintf("%.0f", Sys.free_memory()/2^20)) MiB")
	    end

		# Force garbage collection
		if (i_traj + (i_scan-1)*num_trajs) % garbage_collect_on_workers == 0
			print("")
			println("Forcing garbage collection")
			@everywhere GC.gc()
		end

		global num_lines_calc += 1

	end

	if verbose
		print("")
		println("Integration of scan took $(@sprintf("%.2f", scan_calc_time)) s")
		println("System memory remaining: $(@sprintf("%.0f", Sys.free_memory()/2^20)) MiB")
	end

	# Add numerical statistics to DataFrame
    dfStats[i_scan, "SolveFEvs"] = mean(scans_fevs)/num_trajs/num_dpd1_points
    dfStats[i_scan, "ScanCalcTime"] = scan_calc_time/num_trajs
    dfStats[i_scan, "Timestamp"] = string(Dates.now())

end

if verbose
    print("")
end
"""
Convert time in s `sec` to hours (int), minutes (int), and seconds (int).
`sec` is rounded to the nearest integer before conversion.
"""
function convert_from_seconds(sec)
	sec = Int(round(sec))
    x, seconds = divrem(sec, 60)
    hours, minutes = divrem(x, 60)
    return hours, minutes, seconds
end
hours, minutes, seconds = convert_from_seconds(time()-start_time_calc)
println(
	"Integration took $(@sprintf("%.2f", time()-start_time_calc)) s"
	* " ($(lpad(hours, 2, "0")):$(lpad(minutes, 2, "0")):$(lpad(seconds, 2, "0")))")
if num_lines_calc > 1
    println(
        "Integration for all but first line took " *
        "$(@sprintf("%.2f", (time()-start_time_lines)/num_lines_calc)) s" *
		" on average")
end

# Write results
for filename in DataFrames.unique(dfScans_all_jobs[!, "Filename"])
	scan_indices = isequal.(dfScans[!, "Filename"], filename)
	lines_filepath = Base.Filesystem.joinpath(
		grids_dir, grid_uid, "Lines", filename)
	if sum(scan_indices) == 0
		continue
	end
	println("Saving $(sum(scan_indices)) result(s) to file \"$(filename)\"")
	if use_trajs
		NPZ.npzwrite(
			lines_filepath,
		    Dict(
		        "Detunings" => scans_detunings[scan_indices, :],
				"Signals" => scans_signals[scan_indices, :, :, :],
		        "Populations" => scans_populations[scan_indices, :, :, :]))
	else
		NPZ.npzwrite(
			lines_filepath,
			Dict(
				"Detunings" => scans_detunings[scan_indices, :],
				"Signals" => scans_signals[scan_indices, :, :, 1],
				"Populations" => scans_populations[scan_indices, :, :, 1]))
	end
end

# Write statistics
CSV.write(
    Base.Filesystem.joinpath(
        grids_dir, grid_uid, "Stats", "$(grid_uid)_$(job_id)_stats.dat"),
    dfStats)

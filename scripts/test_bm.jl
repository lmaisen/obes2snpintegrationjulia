using Printf
import NPZ

dir_user_pkg = Base.Filesystem.joinpath(@__DIR__, "../")
if !(dir_user_pkg in LOAD_PATH)
    push!(LOAD_PATH, dir_user_pkg)
end
import H2SnPOBEs

obe_id = "bm2s6p12CDonWS"
obe = H2SnPOBEs.OBEs.set_obe(obe_id)

alpha_offset = 2.
v = 200.
w0 = 2.11e-3
# w0 = 2.2e-3
z_min = -7.5e-3
start_pos = Array([0, 0, z_min])
dpd1 = -1.
laser_power = 15e-6
# dc electric field strength (V/m)
dc_efield_z = 1

p = H2SnPOBEs.OBEParameters()
H2SnPOBEs.OBEs.set_obe_parameters!(obe, p)

p.CalcFreqSampling = "Fixed"
p.CalcFixedFreqListID = "Span80MHzPoints79"
# p.CalcFixedFreqListID = "Span128MHzPoints255"
p.CalcFreqListID = p.CalcFixedFreqListID
p.AlphaOffset = alpha_offset
p.StartPos = start_pos
p.V = v
H2SnPOBEs.set_velocity!(p)
p.LaserWaistRadius = w0
p.DeltapDecay1 = dpd1
p.LaserBeamProfile = "Gaussian"
p.Detuning = 0.
p.LaserPower = laser_power
p.DCEFieldz = dc_efield_z
H2SnPOBEs.set_intensity!(p)

detunings = H2SnPOBEs.get_freq_list(p)
t_max = -2*z_min/p.Vel[3]

tspan = (0.0, t_max)
initial_state = obe.get_initial_state(p)
signal_columns = [
    obe.signals[signal]["EquationIndex"]
    for signal in obe.info["SignalsToSave"]]
num_signals = length(obe.info["SignalsToSave"])
population_columns = [
    obe.states[state]["PopulationEquationIndex"]
    for state in obe.info["PopulationsToSave"]]
num_populations = length(obe.info["PopulationsToSave"])

signal_index = obe.info["NEqs"]-obe.info["NSignalEquations"]+1

# Find initial state of derivatives
dydt_initial = obe.derivs(similar(initial_state), initial_state, p, 0.)
println("There are $(length(dydt_initial[dydt_initial .!= 0])) nonzero initial derivatives")

p.SolveAbsTol = 1e-13
p.SolveRelTol = p.SolveAbsTol/2
p.SolveInitialStepSize = 1e-12
p.SolveMaxIters = 2e7
p.Solver = "radau"

# Test results for zero detuning and first signal column
# bm2s4p12CDon
# signal_comparison = 0.006493440083153124
# bm2s6p12CDon
signal_comparison = 0.005363339776568997

# Integration at single detuning
function test_integration(; p=p)
    start_time = time()
    sol2 = H2SnPOBEs.Solver.solve(obe, p, initial_state, tspan)
    print("")
    println("Integration took $(@sprintf("%.2f", time()-start_time)) s")
    # println(sol2.u[end][signal_columns[1]])
    println(@sprintf(
        "%.2e", (signal_comparison-sol2.u[end][signal_columns[1]])/signal_comparison))
        # println(@sprintf(
        #     "%.2e", (sol.u[end][signal_columns[1]]-sol2.u[end][signal_columns[1]])/sol.u[end][signal_columns[1]]))
    return sol2
end

# sol2 = test_integration()

# # Save results for single detuning
# NPZ.npzwrite(
#     Base.Filesystem.joinpath(
#         "../Lines", "test_obe_0.npz"),
#     Dict(
#         "Detunings" => p.Detuning, "Signals" => sol.u[end][signal_columns],
#         "Populations" => sol.u[end][population_columns]))

# start_time = time()
# a = zeros(length(detunings), obe.info["NEqs"])
# for i = 1:length(detunings)
#     p.Detuning = detunings[i]
#     sol_ = H2SnPOBEs.Solver.solve(obe, p, initial_state, tspan)
#     a[i, :] = sol_.u[end]
# end
# print("")
# println("Integration took $(@sprintf("%.2f", time()-start_time)) s")
# println(a[1, signal_index])
#
# # Save results for single scan
# NPZ.npzwrite(
#     Base.Filesystem.joinpath(
#         "../Lines/bm2s4p12CDon", "06042021_test_bm_2002_$(p.Solver)_$(p.SolveAbsTol).npz"),
#     Dict(
#         "Detunings" => detunings, "Signals" => a[:, signal_columns],
#         "Populations" => a[:, population_columns]))

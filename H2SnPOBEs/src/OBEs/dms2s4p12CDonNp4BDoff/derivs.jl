function derivs(dydt, y, p, t)

    pos = get_pos(p, t)
    IpSqrt = sqrt(get_intensity(p, pos))

    BetaK = p.beta_k
    Vrec = p.RecoilVelocity
    VrecSqr = p.RecoilVelocity^2
    dpd1 = p.DeltapDecay1
    dpd2 = p.DeltapDecay2

    ImSqrt = IpSqrt # Intensity of the second (minus) wave
    OmRp0 = IpSqrt # Complex base of Omega Plus - real part
    OmIp0 = 0 # Complex base of Omega Plus - imaginary part
    OmRm0 = ImSqrt # Complex base of Omega Minus - real part
    OmIm0 = 0 # Complex base of Omega Minus - imaginary part

    # Set detuning Delta in angular frequency,
    # adding recoil shift since recoil is included in OBEs
    Delta = 2*pi*(p.Detuning + p.RecoilShift)
    # Transverse (x) velocity along laser beams
    vx = p.Vel[1]

	OmR1 = -943248.66156242*OmRm0
	OmI1 = -943248.66156242*OmIm0
	OmR2 = -943248.66156242*OmRp0
	OmI2 = -943248.66156242*OmIp0
	OmR3 = -943248.66156242*OmRp0
	OmI3 = -943248.66156242*OmIp0
	OmR4 = -943248.66156242*OmRm0
	OmI4 = -943248.66156242*OmIm0
	OmR5 = -943248.66156242*OmRm0
	OmI5 = -943248.66156242*OmIm0
	OmR6 = -943248.66156242*OmRp0
	OmI6 = -943248.66156242*OmIp0
	OmR7 = -943248.66156242*OmRp0
	OmI7 = -943248.66156242*OmIp0
	OmR8 = -943248.66156242*OmRm0
	OmI8 = -943248.66156242*OmIm0
	
	dydt[1] = -(OmR1*y[14]) + OmI1*y[15] - OmR2*y[16] + OmI2*y[17]
	dydt[2] = OmR1*y[14] - OmI1*y[15] - 8.11817638145224e7*y[2] + OmR3*y[32] + OmI3*y[33]
	dydt[3] = 8.11817638145224e7*y[2]
	dydt[4] = OmR2*y[16] - OmI2*y[17] - 8.11817638145224e7*y[4] + OmR5*y[52] + OmI5*y[53]
	dydt[5] = 8.11817638145224e7*y[4]
	dydt[6] = -(OmR3*y[32]) - OmI3*y[33] - OmR4*y[68] + OmI4*y[69]
	dydt[7] = -(OmR5*y[52]) - OmI5*y[53] - OmR6*y[78] + OmI6*y[79]
	dydt[8] = OmR4*y[68] - OmI4*y[69] + OmR7*y[86] + OmI7*y[87] - 8.11817638145224e7*y[8]
	dydt[9] = 8.11817638145224e7*y[8]
	dydt[10] = -8.11817638145224e7*y[10] + OmR6*y[78] - OmI6*y[79] + OmR8*y[94] + OmI8*y[95]
	dydt[11] = 8.11817638145224e7*y[10]
	dydt[12] = -(OmR7*y[86]) - OmI7*y[87]
	dydt[13] = -(OmR8*y[94]) - OmI8*y[95]
	dydt[14] = -4.05908819072612e7*y[14] + Delta*y[15] - BetaK*VrecSqr*y[15] + 2*BetaK*Vrec*vx*y[15] + (OmR3*y[18])/2. + (OmI3*y[19])/2. + (OmR1*y[1])/2. - (OmR1*y[2])/2. - (OmR2*y[30])/2. + (OmI2*y[31])/2.
	dydt[15] = -(Delta*y[14]) + BetaK*VrecSqr*y[14] - 2*BetaK*Vrec*vx*y[14] - 4.05908819072612e7*y[15] - (OmI3*y[18])/2. + (OmR3*y[19])/2. - (OmI1*y[1])/2. + (OmI1*y[2])/2. + (OmI2*y[30])/2. + (OmR2*y[31])/2.
	dydt[16] = -4.05908819072612e7*y[16] + Delta*y[17] - BetaK*VrecSqr*y[17] - 2*BetaK*Vrec*vx*y[17] + (OmR2*y[1])/2. + (OmR5*y[20])/2. + (OmI5*y[21])/2. - (OmR1*y[30])/2. - (OmI1*y[31])/2. - (OmR2*y[4])/2.
	dydt[17] = -(Delta*y[16]) + BetaK*VrecSqr*y[16] + 2*BetaK*Vrec*vx*y[16] - 4.05908819072612e7*y[17] - (OmI2*y[1])/2. - (OmI5*y[20])/2. + (OmR5*y[21])/2. + (OmI1*y[30])/2. - (OmR1*y[31])/2. + (OmI2*y[4])/2.
	dydt[18] = -0.5*(OmR3*y[14]) + (OmI3*y[15])/2. - 4*BetaK*VrecSqr*y[19] + 4*BetaK*Vrec*vx*y[19] - (OmR4*y[22])/2. + (OmI4*y[23])/2. - (OmR1*y[32])/2. - (OmI1*y[33])/2. - (OmR2*y[50])/2. - (OmI2*y[51])/2.
	dydt[19] = -0.5*(OmI3*y[14]) - (OmR3*y[15])/2. + 4*BetaK*VrecSqr*y[18] - 4*BetaK*Vrec*vx*y[18] - (OmI4*y[22])/2. - (OmR4*y[23])/2. + (OmI1*y[32])/2. - (OmR1*y[33])/2. + (OmI2*y[50])/2. - (OmR2*y[51])/2.
	dydt[20] = -0.5*(OmR5*y[16]) + (OmI5*y[17])/2. - 4*BetaK*VrecSqr*y[21] - 4*BetaK*Vrec*vx*y[21] - (OmR6*y[24])/2. + (OmI6*y[25])/2. - (OmR1*y[34])/2. - (OmI1*y[35])/2. - (OmR2*y[52])/2. - (OmI2*y[53])/2.
	dydt[21] = -0.5*(OmI5*y[16]) - (OmR5*y[17])/2. + 4*BetaK*VrecSqr*y[20] + 4*BetaK*Vrec*vx*y[20] - (OmI6*y[24])/2. - (OmR6*y[25])/2. + (OmI1*y[34])/2. - (OmR1*y[35])/2. + (OmI2*y[52])/2. - (OmR2*y[53])/2.
	dydt[22] = (OmR4*y[18])/2. + (OmI4*y[19])/2. - 4.05908819072612e7*y[22] + Delta*y[23] - 9*BetaK*VrecSqr*y[23] + 6*BetaK*Vrec*vx*y[23] + (OmR7*y[26])/2. + (OmI7*y[27])/2. - (OmR1*y[36])/2. - (OmI1*y[37])/2. - (OmR2*y[54])/2. - (OmI2*y[55])/2.
	dydt[23] = -0.5*(OmI4*y[18]) + (OmR4*y[19])/2. - Delta*y[22] + 9*BetaK*VrecSqr*y[22] - 6*BetaK*Vrec*vx*y[22] - 4.05908819072612e7*y[23] - (OmI7*y[26])/2. + (OmR7*y[27])/2. + (OmI1*y[36])/2. - (OmR1*y[37])/2. + (OmI2*y[54])/2. - (OmR2*y[55])/2.
	dydt[24] = (OmR6*y[20])/2. + (OmI6*y[21])/2. - 4.05908819072612e7*y[24] + Delta*y[25] - 9*BetaK*VrecSqr*y[25] - 6*BetaK*Vrec*vx*y[25] + (OmR8*y[28])/2. + (OmI8*y[29])/2. - (OmR1*y[38])/2. - (OmI1*y[39])/2. - (OmR2*y[56])/2. - (OmI2*y[57])/2.
	dydt[25] = -0.5*(OmI6*y[20]) + (OmR6*y[21])/2. - Delta*y[24] + 9*BetaK*VrecSqr*y[24] + 6*BetaK*Vrec*vx*y[24] - 4.05908819072612e7*y[25] - (OmI8*y[28])/2. + (OmR8*y[29])/2. + (OmI1*y[38])/2. - (OmR1*y[39])/2. + (OmI2*y[56])/2. - (OmR2*y[57])/2.
	dydt[26] = -0.5*(OmR7*y[22]) + (OmI7*y[23])/2. - 16*BetaK*VrecSqr*y[27] + 8*BetaK*Vrec*vx*y[27] - (OmR1*y[40])/2. - (OmI1*y[41])/2. - (OmR2*y[58])/2. - (OmI2*y[59])/2.
	dydt[27] = -0.5*(OmI7*y[22]) - (OmR7*y[23])/2. + 16*BetaK*VrecSqr*y[26] - 8*BetaK*Vrec*vx*y[26] + (OmI1*y[40])/2. - (OmR1*y[41])/2. + (OmI2*y[58])/2. - (OmR2*y[59])/2.
	dydt[28] = -0.5*(OmR8*y[24]) + (OmI8*y[25])/2. - 16*BetaK*VrecSqr*y[29] - 8*BetaK*Vrec*vx*y[29] - (OmR1*y[42])/2. - (OmI1*y[43])/2. - (OmR2*y[60])/2. - (OmI2*y[61])/2.
	dydt[29] = -0.5*(OmI8*y[24]) - (OmR8*y[25])/2. + 16*BetaK*VrecSqr*y[28] + 8*BetaK*Vrec*vx*y[28] + (OmI1*y[42])/2. - (OmR1*y[43])/2. + (OmI2*y[60])/2. - (OmR2*y[61])/2.
	dydt[30] = (OmR2*y[14])/2. - (OmI2*y[15])/2. + (OmR1*y[16])/2. - (OmI1*y[17])/2. - 8.11817638145224e7*y[30] - 4*BetaK*Vrec*vx*y[31] + (OmR5*y[34])/2. + (OmI5*y[35])/2. + (OmR3*y[50])/2. + (OmI3*y[51])/2.
	dydt[31] = -0.5*(OmI2*y[14]) - (OmR2*y[15])/2. + (OmI1*y[16])/2. + (OmR1*y[17])/2. + 4*BetaK*Vrec*vx*y[30] - 8.11817638145224e7*y[31] - (OmI5*y[34])/2. + (OmR5*y[35])/2. + (OmI3*y[50])/2. - (OmR3*y[51])/2.
	dydt[32] = (OmR1*y[18])/2. - (OmI1*y[19])/2. - (OmR3*y[2])/2. - 4.05908819072612e7*y[32] - Delta*y[33] - 3*BetaK*VrecSqr*y[33] + 2*BetaK*Vrec*vx*y[33] - (OmR4*y[36])/2. + (OmI4*y[37])/2. + (OmR3*y[6])/2.
	dydt[33] = (OmI1*y[18])/2. + (OmR1*y[19])/2. - (OmI3*y[2])/2. + Delta*y[32] + 3*BetaK*VrecSqr*y[32] - 2*BetaK*Vrec*vx*y[32] - 4.05908819072612e7*y[33] - (OmI4*y[36])/2. - (OmR4*y[37])/2. + (OmI3*y[6])/2.
	dydt[34] = (OmR1*y[20])/2. - (OmI1*y[21])/2. - (OmR5*y[30])/2. + (OmI5*y[31])/2. - 4.05908819072612e7*y[34] - Delta*y[35] - 3*BetaK*VrecSqr*y[35] - 6*BetaK*Vrec*vx*y[35] - (OmR6*y[38])/2. + (OmI6*y[39])/2. + (OmR3*y[66])/2. - (OmI3*y[67])/2.
	dydt[35] = (OmI1*y[20])/2. + (OmR1*y[21])/2. - (OmI5*y[30])/2. - (OmR5*y[31])/2. + Delta*y[34] + 3*BetaK*VrecSqr*y[34] + 6*BetaK*Vrec*vx*y[34] - 4.05908819072612e7*y[35] - (OmI6*y[38])/2. - (OmR6*y[39])/2. + (OmI3*y[66])/2. + (OmR3*y[67])/2.
	dydt[36] = (OmR1*y[22])/2. - (OmI1*y[23])/2. + (OmR4*y[32])/2. + (OmI4*y[33])/2. - 8.11817638145224e7*y[36] - 8*BetaK*VrecSqr*y[37] + 4*BetaK*Vrec*vx*y[37] + (OmR7*y[40])/2. + (OmI7*y[41])/2. + (OmR3*y[68])/2. - (OmI3*y[69])/2.
	dydt[37] = (OmI1*y[22])/2. + (OmR1*y[23])/2. - (OmI4*y[32])/2. + (OmR4*y[33])/2. + 8*BetaK*VrecSqr*y[36] - 4*BetaK*Vrec*vx*y[36] - 8.11817638145224e7*y[37] - (OmI7*y[40])/2. + (OmR7*y[41])/2. + (OmI3*y[68])/2. + (OmR3*y[69])/2.
	dydt[38] = (OmR1*y[24])/2. - (OmI1*y[25])/2. + (OmR6*y[34])/2. + (OmI6*y[35])/2. - 8.11817638145224e7*y[38] - 8*BetaK*VrecSqr*y[39] - 8*BetaK*Vrec*vx*y[39] + (OmR8*y[42])/2. + (OmI8*y[43])/2. + (OmR3*y[70])/2. - (OmI3*y[71])/2.
	dydt[39] = (OmI1*y[24])/2. + (OmR1*y[25])/2. - (OmI6*y[34])/2. + (OmR6*y[35])/2. + 8*BetaK*VrecSqr*y[38] + 8*BetaK*Vrec*vx*y[38] - 8.11817638145224e7*y[39] - (OmI8*y[42])/2. + (OmR8*y[43])/2. + (OmI3*y[70])/2. + (OmR3*y[71])/2.
	dydt[40] = (OmR1*y[26])/2. - (OmI1*y[27])/2. - (OmR7*y[36])/2. + (OmI7*y[37])/2. - 4.05908819072612e7*y[40] - Delta*y[41] - 15*BetaK*VrecSqr*y[41] + 6*BetaK*Vrec*vx*y[41] + (OmR3*y[72])/2. - (OmI3*y[73])/2.
	dydt[41] = (OmI1*y[26])/2. + (OmR1*y[27])/2. - (OmI7*y[36])/2. - (OmR7*y[37])/2. + Delta*y[40] + 15*BetaK*VrecSqr*y[40] - 6*BetaK*Vrec*vx*y[40] - 4.05908819072612e7*y[41] + (OmI3*y[72])/2. + (OmR3*y[73])/2.
	dydt[42] = (OmR1*y[28])/2. - (OmI1*y[29])/2. - (OmR8*y[38])/2. + (OmI8*y[39])/2. - 4.05908819072612e7*y[42] - Delta*y[43] - 15*BetaK*VrecSqr*y[43] - 10*BetaK*Vrec*vx*y[43] + (OmR3*y[74])/2. - (OmI3*y[75])/2.
	dydt[43] = (OmI1*y[28])/2. + (OmR1*y[29])/2. - (OmI8*y[38])/2. - (OmR8*y[39])/2. + Delta*y[42] + 15*BetaK*VrecSqr*y[42] + 10*BetaK*Vrec*vx*y[42] - 4.05908819072612e7*y[43] + (OmI3*y[74])/2. + (OmR3*y[75])/2.
	dydt[44] = 8.11817638145224e7*y[30] - 4*BetaK*Vrec*vx*y[45]
	dydt[45] = 8.11817638145224e7*y[31] + 4*BetaK*Vrec*vx*y[44]
	dydt[46] = 8.11817638145224e7*y[36] - 8*BetaK*VrecSqr*y[47] + 4*BetaK*Vrec*vx*y[47]
	dydt[47] = 8.11817638145224e7*y[37] + 8*BetaK*VrecSqr*y[46] - 4*BetaK*Vrec*vx*y[46]
	dydt[48] = 8.11817638145224e7*y[38] - 8*BetaK*VrecSqr*y[49] - 8*BetaK*Vrec*vx*y[49]
	dydt[49] = 8.11817638145224e7*y[39] + 8*BetaK*VrecSqr*y[48] + 8*BetaK*Vrec*vx*y[48]
	dydt[50] = (OmR2*y[18])/2. - (OmI2*y[19])/2. - (OmR3*y[30])/2. - (OmI3*y[31])/2. - 4.05908819072612e7*y[50] - Delta*y[51] - 3*BetaK*VrecSqr*y[51] + 6*BetaK*Vrec*vx*y[51] - (OmR4*y[54])/2. + (OmI4*y[55])/2. + (OmR5*y[66])/2. + (OmI5*y[67])/2.
	dydt[51] = (OmI2*y[18])/2. + (OmR2*y[19])/2. - (OmI3*y[30])/2. + (OmR3*y[31])/2. + Delta*y[50] + 3*BetaK*VrecSqr*y[50] - 6*BetaK*Vrec*vx*y[50] - 4.05908819072612e7*y[51] - (OmI4*y[54])/2. - (OmR4*y[55])/2. + (OmI5*y[66])/2. - (OmR5*y[67])/2.
	dydt[52] = (OmR2*y[20])/2. - (OmI2*y[21])/2. - (OmR5*y[4])/2. - 4.05908819072612e7*y[52] - Delta*y[53] - 3*BetaK*VrecSqr*y[53] - 2*BetaK*Vrec*vx*y[53] - (OmR6*y[56])/2. + (OmI6*y[57])/2. + (OmR5*y[7])/2.
	dydt[53] = (OmI2*y[20])/2. + (OmR2*y[21])/2. - (OmI5*y[4])/2. + Delta*y[52] + 3*BetaK*VrecSqr*y[52] + 2*BetaK*Vrec*vx*y[52] - 4.05908819072612e7*y[53] - (OmI6*y[56])/2. - (OmR6*y[57])/2. + (OmI5*y[7])/2.
	dydt[54] = (OmR2*y[22])/2. - (OmI2*y[23])/2. + (OmR4*y[50])/2. + (OmI4*y[51])/2. - 8.11817638145224e7*y[54] - 8*BetaK*VrecSqr*y[55] + 8*BetaK*Vrec*vx*y[55] + (OmR7*y[58])/2. + (OmI7*y[59])/2. + (OmR5*y[76])/2. - (OmI5*y[77])/2.
	dydt[55] = (OmI2*y[22])/2. + (OmR2*y[23])/2. - (OmI4*y[50])/2. + (OmR4*y[51])/2. + 8*BetaK*VrecSqr*y[54] - 8*BetaK*Vrec*vx*y[54] - 8.11817638145224e7*y[55] - (OmI7*y[58])/2. + (OmR7*y[59])/2. + (OmI5*y[76])/2. + (OmR5*y[77])/2.
	dydt[56] = (OmR2*y[24])/2. - (OmI2*y[25])/2. + (OmR6*y[52])/2. + (OmI6*y[53])/2. - 8.11817638145224e7*y[56] - 8*BetaK*VrecSqr*y[57] - 4*BetaK*Vrec*vx*y[57] + (OmR8*y[60])/2. + (OmI8*y[61])/2. + (OmR5*y[78])/2. - (OmI5*y[79])/2.
	dydt[57] = (OmI2*y[24])/2. + (OmR2*y[25])/2. - (OmI6*y[52])/2. + (OmR6*y[53])/2. + 8*BetaK*VrecSqr*y[56] + 4*BetaK*Vrec*vx*y[56] - 8.11817638145224e7*y[57] - (OmI8*y[60])/2. + (OmR8*y[61])/2. + (OmI5*y[78])/2. + (OmR5*y[79])/2.
	dydt[58] = (OmR2*y[26])/2. - (OmI2*y[27])/2. - (OmR7*y[54])/2. + (OmI7*y[55])/2. - 4.05908819072612e7*y[58] - Delta*y[59] - 15*BetaK*VrecSqr*y[59] + 10*BetaK*Vrec*vx*y[59] + (OmR5*y[80])/2. - (OmI5*y[81])/2.
	dydt[59] = (OmI2*y[26])/2. + (OmR2*y[27])/2. - (OmI7*y[54])/2. - (OmR7*y[55])/2. + Delta*y[58] + 15*BetaK*VrecSqr*y[58] - 10*BetaK*Vrec*vx*y[58] - 4.05908819072612e7*y[59] + (OmI5*y[80])/2. + (OmR5*y[81])/2.
	dydt[60] = (OmR2*y[28])/2. - (OmI2*y[29])/2. - (OmR8*y[56])/2. + (OmI8*y[57])/2. - 4.05908819072612e7*y[60] - Delta*y[61] - 15*BetaK*VrecSqr*y[61] - 6*BetaK*Vrec*vx*y[61] + (OmR5*y[82])/2. - (OmI5*y[83])/2.
	dydt[61] = (OmI2*y[28])/2. + (OmR2*y[29])/2. - (OmI8*y[56])/2. - (OmR8*y[57])/2. + Delta*y[60] + 15*BetaK*VrecSqr*y[60] + 6*BetaK*Vrec*vx*y[60] - 4.05908819072612e7*y[61] + (OmI5*y[82])/2. + (OmR5*y[83])/2.
	dydt[62] = 8.11817638145224e7*y[54] - 8*BetaK*VrecSqr*y[63] + 8*BetaK*Vrec*vx*y[63]
	dydt[63] = 8.11817638145224e7*y[55] + 8*BetaK*VrecSqr*y[62] - 8*BetaK*Vrec*vx*y[62]
	dydt[64] = 8.11817638145224e7*y[56] - 8*BetaK*VrecSqr*y[65] - 4*BetaK*Vrec*vx*y[65]
	dydt[65] = 8.11817638145224e7*y[57] + 8*BetaK*VrecSqr*y[64] + 4*BetaK*Vrec*vx*y[64]
	dydt[66] = -0.5*(OmR3*y[34]) - (OmI3*y[35])/2. - (OmR5*y[50])/2. - (OmI5*y[51])/2. - 8*BetaK*Vrec*vx*y[67] - (OmR6*y[70])/2. + (OmI6*y[71])/2. - (OmR4*y[76])/2. + (OmI4*y[77])/2.
	dydt[67] = (OmI3*y[34])/2. - (OmR3*y[35])/2. - (OmI5*y[50])/2. + (OmR5*y[51])/2. + 8*BetaK*Vrec*vx*y[66] - (OmI6*y[70])/2. - (OmR6*y[71])/2. + (OmI4*y[76])/2. + (OmR4*y[77])/2.
	dydt[68] = -0.5*(OmR3*y[36]) - (OmI3*y[37])/2. - 4.05908819072612e7*y[68] + Delta*y[69] - 5*BetaK*VrecSqr*y[69] + 2*BetaK*Vrec*vx*y[69] + (OmR4*y[6])/2. + (OmR7*y[72])/2. + (OmI7*y[73])/2. - (OmR4*y[8])/2.
	dydt[69] = (OmI3*y[36])/2. - (OmR3*y[37])/2. - Delta*y[68] + 5*BetaK*VrecSqr*y[68] - 2*BetaK*Vrec*vx*y[68] - 4.05908819072612e7*y[69] - (OmI4*y[6])/2. - (OmI7*y[72])/2. + (OmR7*y[73])/2. + (OmI4*y[8])/2.
	dydt[70] = -0.5*(OmR3*y[38]) - (OmI3*y[39])/2. + (OmR6*y[66])/2. + (OmI6*y[67])/2. - 4.05908819072612e7*y[70] + Delta*y[71] - 5*BetaK*VrecSqr*y[71] - 10*BetaK*Vrec*vx*y[71] + (OmR8*y[74])/2. + (OmI8*y[75])/2. - (OmR4*y[84])/2. - (OmI4*y[85])/2.
	dydt[71] = (OmI3*y[38])/2. - (OmR3*y[39])/2. - (OmI6*y[66])/2. + (OmR6*y[67])/2. - Delta*y[70] + 5*BetaK*VrecSqr*y[70] + 10*BetaK*Vrec*vx*y[70] - 4.05908819072612e7*y[71] - (OmI8*y[74])/2. + (OmR8*y[75])/2. + (OmI4*y[84])/2. - (OmR4*y[85])/2.
	dydt[72] = -0.5*(OmR3*y[40]) - (OmI3*y[41])/2. - (OmR7*y[68])/2. + (OmI7*y[69])/2. - 12*BetaK*VrecSqr*y[73] + 4*BetaK*Vrec*vx*y[73] - (OmR4*y[86])/2. - (OmI4*y[87])/2.
	dydt[73] = (OmI3*y[40])/2. - (OmR3*y[41])/2. - (OmI7*y[68])/2. - (OmR7*y[69])/2. + 12*BetaK*VrecSqr*y[72] - 4*BetaK*Vrec*vx*y[72] + (OmI4*y[86])/2. - (OmR4*y[87])/2.
	dydt[74] = -0.5*(OmR3*y[42]) - (OmI3*y[43])/2. - (OmR8*y[70])/2. + (OmI8*y[71])/2. - 12*BetaK*VrecSqr*y[75] - 12*BetaK*Vrec*vx*y[75] - (OmR4*y[88])/2. - (OmI4*y[89])/2.
	dydt[75] = (OmI3*y[42])/2. - (OmR3*y[43])/2. - (OmI8*y[70])/2. - (OmR8*y[71])/2. + 12*BetaK*VrecSqr*y[74] + 12*BetaK*Vrec*vx*y[74] + (OmI4*y[88])/2. - (OmR4*y[89])/2.
	dydt[76] = -0.5*(OmR5*y[54]) - (OmI5*y[55])/2. + (OmR4*y[66])/2. - (OmI4*y[67])/2. - 4.05908819072612e7*y[76] + Delta*y[77] - 5*BetaK*VrecSqr*y[77] + 10*BetaK*Vrec*vx*y[77] + (OmR7*y[80])/2. + (OmI7*y[81])/2. - (OmR6*y[84])/2. + (OmI6*y[85])/2.
	dydt[77] = (OmI5*y[54])/2. - (OmR5*y[55])/2. - (OmI4*y[66])/2. - (OmR4*y[67])/2. - Delta*y[76] + 5*BetaK*VrecSqr*y[76] - 10*BetaK*Vrec*vx*y[76] - 4.05908819072612e7*y[77] - (OmI7*y[80])/2. + (OmR7*y[81])/2. + (OmI6*y[84])/2. + (OmR6*y[85])/2.
	dydt[78] = -0.5*(OmR6*y[10]) - (OmR5*y[56])/2. - (OmI5*y[57])/2. - 4.05908819072612e7*y[78] + Delta*y[79] - 5*BetaK*VrecSqr*y[79] - 2*BetaK*Vrec*vx*y[79] + (OmR6*y[7])/2. + (OmR8*y[82])/2. + (OmI8*y[83])/2.
	dydt[79] = (OmI6*y[10])/2. + (OmI5*y[56])/2. - (OmR5*y[57])/2. - Delta*y[78] + 5*BetaK*VrecSqr*y[78] + 2*BetaK*Vrec*vx*y[78] - 4.05908819072612e7*y[79] - (OmI6*y[7])/2. - (OmI8*y[82])/2. + (OmR8*y[83])/2.
	dydt[80] = -0.5*(OmR5*y[58]) - (OmI5*y[59])/2. - (OmR7*y[76])/2. + (OmI7*y[77])/2. - 12*BetaK*VrecSqr*y[81] + 12*BetaK*Vrec*vx*y[81] - (OmR6*y[92])/2. - (OmI6*y[93])/2.
	dydt[81] = (OmI5*y[58])/2. - (OmR5*y[59])/2. - (OmI7*y[76])/2. - (OmR7*y[77])/2. + 12*BetaK*VrecSqr*y[80] - 12*BetaK*Vrec*vx*y[80] + (OmI6*y[92])/2. - (OmR6*y[93])/2.
	dydt[82] = -0.5*(OmR5*y[60]) - (OmI5*y[61])/2. - (OmR8*y[78])/2. + (OmI8*y[79])/2. - 12*BetaK*VrecSqr*y[83] - 4*BetaK*Vrec*vx*y[83] - (OmR6*y[94])/2. - (OmI6*y[95])/2.
	dydt[83] = (OmI5*y[60])/2. - (OmR5*y[61])/2. - (OmI8*y[78])/2. - (OmR8*y[79])/2. + 12*BetaK*VrecSqr*y[82] + 4*BetaK*Vrec*vx*y[82] + (OmI6*y[94])/2. - (OmR6*y[95])/2.
	dydt[84] = (OmR4*y[70])/2. - (OmI4*y[71])/2. + (OmR6*y[76])/2. - (OmI6*y[77])/2. - 8.11817638145224e7*y[84] - 12*BetaK*Vrec*vx*y[85] + (OmR8*y[88])/2. + (OmI8*y[89])/2. + (OmR7*y[92])/2. + (OmI7*y[93])/2.
	dydt[85] = (OmI4*y[70])/2. + (OmR4*y[71])/2. - (OmI6*y[76])/2. - (OmR6*y[77])/2. + 12*BetaK*Vrec*vx*y[84] - 8.11817638145224e7*y[85] - (OmI8*y[88])/2. + (OmR8*y[89])/2. + (OmI7*y[92])/2. - (OmR7*y[93])/2.
	dydt[86] = (OmR7*y[12])/2. + (OmR4*y[72])/2. - (OmI4*y[73])/2. - 4.05908819072612e7*y[86] - Delta*y[87] - 7*BetaK*VrecSqr*y[87] + 2*BetaK*Vrec*vx*y[87] - (OmR7*y[8])/2.
	dydt[87] = (OmI7*y[12])/2. + (OmI4*y[72])/2. + (OmR4*y[73])/2. + Delta*y[86] + 7*BetaK*VrecSqr*y[86] - 2*BetaK*Vrec*vx*y[86] - 4.05908819072612e7*y[87] - (OmI7*y[8])/2.
	dydt[88] = (OmR4*y[74])/2. - (OmI4*y[75])/2. - (OmR8*y[84])/2. + (OmI8*y[85])/2. - 4.05908819072612e7*y[88] - Delta*y[89] - 7*BetaK*VrecSqr*y[89] - 14*BetaK*Vrec*vx*y[89] + (OmR7*y[96])/2. - (OmI7*y[97])/2.
	dydt[89] = (OmI4*y[74])/2. + (OmR4*y[75])/2. - (OmI8*y[84])/2. - (OmR8*y[85])/2. + Delta*y[88] + 7*BetaK*VrecSqr*y[88] + 14*BetaK*Vrec*vx*y[88] - 4.05908819072612e7*y[89] + (OmI7*y[96])/2. + (OmR7*y[97])/2.
	dydt[90] = 8.11817638145224e7*y[84] - 12*BetaK*Vrec*vx*y[91]
	dydt[91] = 8.11817638145224e7*y[85] + 12*BetaK*Vrec*vx*y[90]
	dydt[92] = (OmR6*y[80])/2. - (OmI6*y[81])/2. - (OmR7*y[84])/2. - (OmI7*y[85])/2. - 4.05908819072612e7*y[92] - Delta*y[93] - 7*BetaK*VrecSqr*y[93] + 14*BetaK*Vrec*vx*y[93] + (OmR8*y[96])/2. + (OmI8*y[97])/2.
	dydt[93] = (OmI6*y[80])/2. + (OmR6*y[81])/2. - (OmI7*y[84])/2. + (OmR7*y[85])/2. + Delta*y[92] + 7*BetaK*VrecSqr*y[92] - 14*BetaK*Vrec*vx*y[92] - 4.05908819072612e7*y[93] + (OmI8*y[96])/2. - (OmR8*y[97])/2.
	dydt[94] = -0.5*(OmR8*y[10]) + (OmR8*y[13])/2. + (OmR6*y[82])/2. - (OmI6*y[83])/2. - 4.05908819072612e7*y[94] - Delta*y[95] - 7*BetaK*VrecSqr*y[95] - 2*BetaK*Vrec*vx*y[95]
	dydt[95] = -0.5*(OmI8*y[10]) + (OmI8*y[13])/2. + (OmI6*y[82])/2. + (OmR6*y[83])/2. + Delta*y[94] + 7*BetaK*VrecSqr*y[94] + 2*BetaK*Vrec*vx*y[94] - 4.05908819072612e7*y[95]
	dydt[96] = -0.5*(OmR7*y[88]) - (OmI7*y[89])/2. - (OmR8*y[92])/2. - (OmI8*y[93])/2. - 16*BetaK*Vrec*vx*y[97]
	dydt[97] = (OmI7*y[88])/2. - (OmR7*y[89])/2. - (OmI8*y[92])/2. + (OmR8*y[93])/2. + 16*BetaK*Vrec*vx*y[96]
	dydt[98] = 6.81146610302929e7*y[10] + 6.81146610302929e7*y[2] + 6.81146610302929e7*y[4] + 6.81146610302929e7*y[8]
	dydt[99] = 0
	dydt[100] = 6.81146610302929e7*y[10] + 6.81146610302929e7*y[2] + 6.81146610302929e7*y[4] + 6.81146610302929e7*y[8]
	dydt[101] = 0

    return dydt

end
module H2SnPStandingWave

using H2SnPOBEs.Helpers

info = Dict(
    "Isotope"=>"H",
    "NEqs"=>5,
    "NSignals"=>1,
    "Backdecay"=>true,
    )

function get_initial_state(p)
    initial_state = zeros(info.NEqs)
    initial_state[1] = 1.
    return initial_state
end

function derivs(dydt, y, p, t)

    pos = get_pos(p, t)
    rabi_freq = get_rabi_frequency(p, pos)
    domega = p.Detuning
    gammal = p.gamma_l
    gammad = p.gamma_d
    gammas = p.gamma_s
    k = p.LaserWavenumber

    # Two counter-propagating beams
    Omega_r = rabi_freq * 2 * cos(k*pos[1])
    Omega_i = 0

    # Derivatives
    dydt[1] = - Omega_r * y[3] + Omega_i * y[2] + gammad * y[4]  # Population of ground state
    dydt[2] = domega * y[3] - Omega_i/2 * (y[1]-y[4]) - (gammal+gammad)/2 * y[2]  # Real part of coherence
    dydt[3] = - domega * y[2] + Omega_r/2 * (y[1]-y[4]) - (gammal+gammad)/2 * y[3]  # Imaginary part of coherence
    dydt[4] = Omega_r * y[3] - Omega_i * y[2] - (gammal+gammad) * y[4]  # Population of excited state
    dydt[5] = gammas * y[4]

    return dydt

end

end

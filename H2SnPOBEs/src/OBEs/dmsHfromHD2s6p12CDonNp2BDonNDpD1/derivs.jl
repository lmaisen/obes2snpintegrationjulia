function derivs(dydt, y, p, t)

    pos = get_pos(p, t)
    IpSqrt = sqrt(get_intensity(p, pos))

    BetaK = p.beta_k
    Vrec = p.RecoilVelocity
    VrecSqr = p.RecoilVelocity^2
    dpd1 = p.DeltapDecay1
    dpd2 = p.DeltapDecay2

    ImSqrt = IpSqrt # Intensity of the second (minus) wave
    OmRp0 = IpSqrt # Complex base of Omega Plus - real part
    OmIp0 = 0 # Complex base of Omega Plus - imaginary part
    OmRm0 = ImSqrt # Complex base of Omega Minus - real part
    OmIm0 = 0 # Complex base of Omega Minus - imaginary part

    # Set detuning Delta in angular frequency,
    # adding recoil shift since recoil is included in OBEs
    Delta = 2*pi*(p.Detuning + p.RecoilShift)
    # Transverse (x) velocity along laser beams
    vx = p.Vel[1]

	OmR1 = -397496.07535679*OmRm0
	OmI1 = -397496.07535679*OmIm0
	OmR2 = -397496.07535679*OmRp0
	OmI2 = -397496.07535679*OmIp0
	OmR3 = -397496.07535679*OmRp0
	OmI3 = -397496.07535679*OmIp0
	OmR4 = -397496.07535679*OmRm0
	OmI4 = -397496.07535679*OmIm0
	OmR5 = -397496.07535679*OmRp0
	OmI5 = -397496.07535679*OmIp0
	OmR6 = -397496.07535679*OmRm0
	OmI6 = -397496.07535679*OmIm0
	OmR7 = -397496.07535679*OmRm0
	OmI7 = -397496.07535679*OmIm0
	OmR8 = -397496.07535679*OmRp0
	OmI8 = -397496.07535679*OmIp0
	OmR9 = -397496.07535679*OmRm0
	OmI9 = -397496.07535679*OmIm0
	OmR10 = -397496.07535679*OmRp0
	OmI10 = -397496.07535679*OmIp0
	OmR11 = -397496.07535679*OmRp0
	OmI11 = -397496.07535679*OmIp0
	OmR12 = -397496.07535679*OmRm0
	OmI12 = -397496.07535679*OmIm0
	
	dydt[1] = -(OmR1*y[16]) + OmI1*y[17] - OmR2*y[18] + OmI2*y[19]
	dydt[2] = OmR3*y[24] + OmI3*y[25] + OmR7*y[26] + OmI7*y[27] - 2.44728622582024e7*y[2]
	dydt[3] = 2.44728622582024e7*y[2]
	dydt[4] = -(OmR3*y[24]) - OmI3*y[25] - OmR4*y[38] + OmI4*y[39] + 964286.732278158*y[5]
	dydt[5] = OmR1*y[16] - OmI1*y[17] + OmR11*y[44] + OmI11*y[45] - 2.44728622582024e7*y[5]
	dydt[6] = 2.35085755259243e7*y[5]
	dydt[7] = -(OmR7*y[26]) - OmI7*y[27] - OmR8*y[52] + OmI8*y[53] + 964286.732278158*y[8]
	dydt[8] = OmR2*y[18] - OmI2*y[19] + OmR12*y[56] + OmI12*y[57] - 2.44728622582024e7*y[8]
	dydt[9] = 2.35085755259243e7*y[8]
	dydt[10] = -(OmR11*y[44]) - OmI11*y[45]
	dydt[11] = -2.44728622582024e7*y[11] + OmR4*y[38] - OmI4*y[39]
	dydt[12] = 2.44728622582024e7*y[11]
	dydt[13] = -(OmR12*y[56]) - OmI12*y[57]
	dydt[14] = -2.44728622582024e7*y[14] + OmR8*y[52] - OmI8*y[53]
	dydt[15] = 2.44728622582024e7*y[14]
	dydt[16] = -1.22364311291012e7*y[16] + Delta*y[17] - BetaK*VrecSqr*y[17] + 2*BetaK*Vrec*vx*y[17] + (OmR1*y[1])/2. + (OmR11*y[20])/2. + (OmI11*y[21])/2. - (OmR2*y[42])/2. + (OmI2*y[43])/2. - (OmR1*y[5])/2.
	dydt[17] = -(Delta*y[16]) + BetaK*VrecSqr*y[16] - 2*BetaK*Vrec*vx*y[16] - 1.22364311291012e7*y[17] - (OmI1*y[1])/2. - (OmI11*y[20])/2. + (OmR11*y[21])/2. + (OmI2*y[42])/2. + (OmR2*y[43])/2. + (OmI1*y[5])/2.
	dydt[18] = -1.22364311291012e7*y[18] + Delta*y[19] - BetaK*VrecSqr*y[19] - 2*BetaK*Vrec*vx*y[19] + (OmR2*y[1])/2. + (OmR12*y[22])/2. + (OmI12*y[23])/2. - (OmR1*y[42])/2. - (OmI1*y[43])/2. - (OmR2*y[8])/2.
	dydt[19] = -(Delta*y[18]) + BetaK*VrecSqr*y[18] + 2*BetaK*Vrec*vx*y[18] - 1.22364311291012e7*y[19] - (OmI2*y[1])/2. - (OmI12*y[22])/2. + (OmR12*y[23])/2. + (OmI1*y[42])/2. - (OmR1*y[43])/2. + (OmI2*y[8])/2.
	dydt[20] = -(OmR11*y[16])/2. + (OmI11*y[17])/2. - 4*BetaK*VrecSqr*y[21] + 4*BetaK*Vrec*vx*y[21] - (OmR1*y[44])/2. - (OmI1*y[45])/2. - (OmR2*y[54])/2. - (OmI2*y[55])/2.
	dydt[21] = -(OmI11*y[16])/2. - (OmR11*y[17])/2. + 4*BetaK*VrecSqr*y[20] - 4*BetaK*Vrec*vx*y[20] + (OmI1*y[44])/2. - (OmR1*y[45])/2. + (OmI2*y[54])/2. - (OmR2*y[55])/2.
	dydt[22] = -(OmR12*y[18])/2. + (OmI12*y[19])/2. - 4*BetaK*VrecSqr*y[23] - 4*BetaK*Vrec*vx*y[23] - (OmR1*y[46])/2. - (OmI1*y[47])/2. - (OmR2*y[56])/2. - (OmI2*y[57])/2.
	dydt[23] = -(OmI12*y[18])/2. - (OmR12*y[19])/2. + 4*BetaK*VrecSqr*y[22] + 4*BetaK*Vrec*vx*y[22] + (OmI1*y[46])/2. - (OmR1*y[47])/2. + (OmI2*y[56])/2. - (OmR2*y[57])/2.
	dydt[24] = -1.22364311291012e7*y[24] - Delta*y[25] - BetaK*VrecSqr*y[25] + 2*BetaK*dpd1*VrecSqr*y[25] + 2*BetaK*Vrec*vx*y[25] - (OmR4*y[28])/2. + (OmI4*y[29])/2. - (OmR3*y[2])/2. + (OmR7*y[36])/2. + (OmI7*y[37])/2. + (OmR3*y[4])/2.
	dydt[25] = Delta*y[24] + BetaK*VrecSqr*y[24] - 2*BetaK*dpd1*VrecSqr*y[24] - 2*BetaK*Vrec*vx*y[24] - 1.22364311291012e7*y[25] - (OmI4*y[28])/2. - (OmR4*y[29])/2. - (OmI3*y[2])/2. + (OmI7*y[36])/2. - (OmR7*y[37])/2. + (OmI3*y[4])/2.
	dydt[26] = -1.22364311291012e7*y[26] - Delta*y[27] - BetaK*VrecSqr*y[27] - 2*BetaK*dpd1*VrecSqr*y[27] - 2*BetaK*Vrec*vx*y[27] - (OmR7*y[2])/2. - (OmR8*y[30])/2. + (OmI8*y[31])/2. + (OmR3*y[36])/2. - (OmI3*y[37])/2. + (OmR7*y[7])/2.
	dydt[27] = Delta*y[26] + BetaK*VrecSqr*y[26] + 2*BetaK*dpd1*VrecSqr*y[26] + 2*BetaK*Vrec*vx*y[26] - 1.22364311291012e7*y[27] - (OmI7*y[2])/2. - (OmI8*y[30])/2. - (OmR8*y[31])/2. + (OmI3*y[36])/2. + (OmR3*y[37])/2. + (OmI7*y[7])/2.
	dydt[28] = (OmR4*y[24])/2. + (OmI4*y[25])/2. - 2.44728622582024e7*y[28] - 4*BetaK*VrecSqr*y[29] + 4*BetaK*dpd1*VrecSqr*y[29] + 4*BetaK*Vrec*vx*y[29] + (OmR3*y[38])/2. - (OmI3*y[39])/2. + (OmR7*y[50])/2. - (OmI7*y[51])/2.
	dydt[29] = -(OmI4*y[24])/2. + (OmR4*y[25])/2. + 4*BetaK*VrecSqr*y[28] - 4*BetaK*dpd1*VrecSqr*y[28] - 4*BetaK*Vrec*vx*y[28] - 2.44728622582024e7*y[29] + (OmI3*y[38])/2. + (OmR3*y[39])/2. + (OmI7*y[50])/2. + (OmR7*y[51])/2.
	dydt[30] = (OmR8*y[26])/2. + (OmI8*y[27])/2. - 2.44728622582024e7*y[30] - 4*BetaK*VrecSqr*y[31] - 4*BetaK*dpd1*VrecSqr*y[31] - 4*BetaK*Vrec*vx*y[31] + (OmR3*y[40])/2. - (OmI3*y[41])/2. + (OmR7*y[52])/2. - (OmI7*y[53])/2.
	dydt[31] = -(OmI8*y[26])/2. + (OmR8*y[27])/2. + 4*BetaK*VrecSqr*y[30] + 4*BetaK*dpd1*VrecSqr*y[30] + 4*BetaK*Vrec*vx*y[30] - 2.44728622582024e7*y[31] + (OmI3*y[40])/2. + (OmR3*y[41])/2. + (OmI7*y[52])/2. + (OmR7*y[53])/2.
	dydt[32] = 2.44728622582024e7*y[28] - 4*BetaK*VrecSqr*y[33] + 4*BetaK*Vrec*vx*y[33]
	dydt[33] = 2.44728622582024e7*y[29] + 4*BetaK*VrecSqr*y[32] - 4*BetaK*Vrec*vx*y[32]
	dydt[34] = 2.44728622582024e7*y[30] - 4*BetaK*VrecSqr*y[35] - 4*BetaK*Vrec*vx*y[35]
	dydt[35] = 2.44728622582024e7*y[31] + 4*BetaK*VrecSqr*y[34] + 4*BetaK*Vrec*vx*y[34]
	dydt[36] = -(OmR7*y[24])/2. - (OmI7*y[25])/2. - (OmR3*y[26])/2. - (OmI3*y[27])/2. - 4*BetaK*dpd1*VrecSqr*y[37] - 4*BetaK*Vrec*vx*y[37] - (OmR8*y[40])/2. + (OmI8*y[41])/2. + 964286.732278158*y[42] - (OmR4*y[50])/2. + (OmI4*y[51])/2.
	dydt[37] = -(OmI7*y[24])/2. + (OmR7*y[25])/2. + (OmI3*y[26])/2. - (OmR3*y[27])/2. + 4*BetaK*dpd1*VrecSqr*y[36] + 4*BetaK*Vrec*vx*y[36] - (OmI8*y[40])/2. - (OmR8*y[41])/2. + 964286.732278158*y[43] + (OmI4*y[50])/2. + (OmR4*y[51])/2.
	dydt[38] = -(OmR4*y[11])/2. - (OmR3*y[28])/2. - (OmI3*y[29])/2. - 1.22364311291012e7*y[38] + Delta*y[39] - 3*BetaK*VrecSqr*y[39] + 2*BetaK*dpd1*VrecSqr*y[39] + 2*BetaK*Vrec*vx*y[39] + (OmR4*y[4])/2.
	dydt[39] = (OmI4*y[11])/2. + (OmI3*y[28])/2. - (OmR3*y[29])/2. - Delta*y[38] + 3*BetaK*VrecSqr*y[38] - 2*BetaK*dpd1*VrecSqr*y[38] - 2*BetaK*Vrec*vx*y[38] - 1.22364311291012e7*y[39] - (OmI4*y[4])/2.
	dydt[40] = -(OmR3*y[30])/2. - (OmI3*y[31])/2. + (OmR8*y[36])/2. + (OmI8*y[37])/2. - 1.22364311291012e7*y[40] + Delta*y[41] - 3*BetaK*VrecSqr*y[41] - 6*BetaK*dpd1*VrecSqr*y[41] - 6*BetaK*Vrec*vx*y[41] - (OmR4*y[60])/2. - (OmI4*y[61])/2.
	dydt[41] = (OmI3*y[30])/2. - (OmR3*y[31])/2. - (OmI8*y[36])/2. + (OmR8*y[37])/2. - Delta*y[40] + 3*BetaK*VrecSqr*y[40] + 6*BetaK*dpd1*VrecSqr*y[40] + 6*BetaK*Vrec*vx*y[40] - 1.22364311291012e7*y[41] + (OmI4*y[60])/2. - (OmR4*y[61])/2.
	dydt[42] = (OmR2*y[16])/2. - (OmI2*y[17])/2. + (OmR1*y[18])/2. - (OmI1*y[19])/2. - 2.44728622582024e7*y[42] - 4*BetaK*Vrec*vx*y[43] + (OmR12*y[46])/2. + (OmI12*y[47])/2. + (OmR11*y[54])/2. + (OmI11*y[55])/2.
	dydt[43] = -(OmI2*y[16])/2. - (OmR2*y[17])/2. + (OmI1*y[18])/2. + (OmR1*y[19])/2. + 4*BetaK*Vrec*vx*y[42] - 2.44728622582024e7*y[43] - (OmI12*y[46])/2. + (OmR12*y[47])/2. + (OmI11*y[54])/2. - (OmR11*y[55])/2.
	dydt[44] = (OmR11*y[10])/2. + (OmR1*y[20])/2. - (OmI1*y[21])/2. - 1.22364311291012e7*y[44] - Delta*y[45] - 3*BetaK*VrecSqr*y[45] + 2*BetaK*Vrec*vx*y[45] - (OmR11*y[5])/2.
	dydt[45] = (OmI11*y[10])/2. + (OmI1*y[20])/2. + (OmR1*y[21])/2. + Delta*y[44] + 3*BetaK*VrecSqr*y[44] - 2*BetaK*Vrec*vx*y[44] - 1.22364311291012e7*y[45] - (OmI11*y[5])/2.
	dydt[46] = (OmR1*y[22])/2. - (OmI1*y[23])/2. - (OmR12*y[42])/2. + (OmI12*y[43])/2. - 1.22364311291012e7*y[46] - Delta*y[47] - 3*BetaK*VrecSqr*y[47] - 6*BetaK*Vrec*vx*y[47] + (OmR11*y[58])/2. - (OmI11*y[59])/2.
	dydt[47] = (OmI1*y[22])/2. + (OmR1*y[23])/2. - (OmI12*y[42])/2. - (OmR12*y[43])/2. + Delta*y[46] + 3*BetaK*VrecSqr*y[46] + 6*BetaK*Vrec*vx*y[46] - 1.22364311291012e7*y[47] + (OmI11*y[58])/2. + (OmR11*y[59])/2.
	dydt[48] = 2.35085755259243e7*y[42] - 4*BetaK*dpd1*VrecSqr*y[49] - 4*BetaK*Vrec*vx*y[49]
	dydt[49] = 2.35085755259243e7*y[43] + 4*BetaK*dpd1*VrecSqr*y[48] + 4*BetaK*Vrec*vx*y[48]
	dydt[50] = -(OmR7*y[28])/2. - (OmI7*y[29])/2. + (OmR4*y[36])/2. - (OmI4*y[37])/2. - 1.22364311291012e7*y[50] + Delta*y[51] - 3*BetaK*VrecSqr*y[51] + 6*BetaK*dpd1*VrecSqr*y[51] + 6*BetaK*Vrec*vx*y[51] - (OmR8*y[60])/2. + (OmI8*y[61])/2.
	dydt[51] = (OmI7*y[28])/2. - (OmR7*y[29])/2. - (OmI4*y[36])/2. - (OmR4*y[37])/2. - Delta*y[50] + 3*BetaK*VrecSqr*y[50] - 6*BetaK*dpd1*VrecSqr*y[50] - 6*BetaK*Vrec*vx*y[50] - 1.22364311291012e7*y[51] + (OmI8*y[60])/2. + (OmR8*y[61])/2.
	dydt[52] = -(OmR8*y[14])/2. - (OmR7*y[30])/2. - (OmI7*y[31])/2. - 1.22364311291012e7*y[52] + Delta*y[53] - 3*BetaK*VrecSqr*y[53] - 2*BetaK*dpd1*VrecSqr*y[53] - 2*BetaK*Vrec*vx*y[53] + (OmR8*y[7])/2.
	dydt[53] = (OmI8*y[14])/2. + (OmI7*y[30])/2. - (OmR7*y[31])/2. - Delta*y[52] + 3*BetaK*VrecSqr*y[52] + 2*BetaK*dpd1*VrecSqr*y[52] + 2*BetaK*Vrec*vx*y[52] - 1.22364311291012e7*y[53] - (OmI8*y[7])/2.
	dydt[54] = (OmR2*y[20])/2. - (OmI2*y[21])/2. - (OmR11*y[42])/2. - (OmI11*y[43])/2. - 1.22364311291012e7*y[54] - Delta*y[55] - 3*BetaK*VrecSqr*y[55] + 6*BetaK*Vrec*vx*y[55] + (OmR12*y[58])/2. + (OmI12*y[59])/2.
	dydt[55] = (OmI2*y[20])/2. + (OmR2*y[21])/2. - (OmI11*y[42])/2. + (OmR11*y[43])/2. + Delta*y[54] + 3*BetaK*VrecSqr*y[54] - 6*BetaK*Vrec*vx*y[54] - 1.22364311291012e7*y[55] + (OmI12*y[58])/2. - (OmR12*y[59])/2.
	dydt[56] = (OmR12*y[13])/2. + (OmR2*y[22])/2. - (OmI2*y[23])/2. - 1.22364311291012e7*y[56] - Delta*y[57] - 3*BetaK*VrecSqr*y[57] - 2*BetaK*Vrec*vx*y[57] - (OmR12*y[8])/2.
	dydt[57] = (OmI12*y[13])/2. + (OmI2*y[22])/2. + (OmR2*y[23])/2. + Delta*y[56] + 3*BetaK*VrecSqr*y[56] + 2*BetaK*Vrec*vx*y[56] - 1.22364311291012e7*y[57] - (OmI12*y[8])/2.
	dydt[58] = -(OmR11*y[46])/2. - (OmI11*y[47])/2. - (OmR12*y[54])/2. - (OmI12*y[55])/2. - 8*BetaK*Vrec*vx*y[59]
	dydt[59] = (OmI11*y[46])/2. - (OmR11*y[47])/2. - (OmI12*y[54])/2. + (OmR12*y[55])/2. + 8*BetaK*Vrec*vx*y[58]
	dydt[60] = (OmR4*y[40])/2. - (OmI4*y[41])/2. + (OmR8*y[50])/2. - (OmI8*y[51])/2. - 2.44728622582024e7*y[60] - 8*BetaK*dpd1*VrecSqr*y[61] - 8*BetaK*Vrec*vx*y[61]
	dydt[61] = (OmI4*y[40])/2. + (OmR4*y[41])/2. - (OmI8*y[50])/2. - (OmR8*y[51])/2. + 8*BetaK*dpd1*VrecSqr*y[60] + 8*BetaK*Vrec*vx*y[60] - 2.44728622582024e7*y[61]
	dydt[62] = 2.44728622582024e7*y[60] - 8*BetaK*Vrec*vx*y[63]
	dydt[63] = 2.44728622582024e7*y[61] + 8*BetaK*Vrec*vx*y[62]
	dydt[64] = 1.97075185920488e7*y[11] + 1.97075185920488e7*y[14] + 1.97075185920488e7*y[2] + 1.97075185920488e7*y[5] + 1.97075185920488e7*y[8]
	dydt[65] = 964286.732278158*y[5] + 964286.732278158*y[8]
	dydt[66] = 1.97075185920488e7*y[5] + 1.97075185920488e7*y[8]
	dydt[67] = 1.97075185920488e7*y[11] + 1.97075185920488e7*y[14] + 1.97075185920488e7*y[2]
	dydt[68] = 1.97075185920488e7*y[11] + 1.97075185920488e7*y[14] + 1.97075185920488e7*y[2]
	dydt[69] = 0

    return dydt

end
function derivs(dydt, y, p, t)

    pos = get_pos(p, t)
    IpSqrt = sqrt(get_intensity(p, pos))

    BetaK = p.beta_k
    Vrec = p.RecoilVelocity
    VrecSqr = p.RecoilVelocity^2
    dpd1 = p.DeltapDecay1
    dpd2 = p.DeltapDecay2

    ImSqrt = IpSqrt # Intensity of the second (minus) wave
    OmRp0 = IpSqrt # Complex base of Omega Plus - real part
    OmIp0 = 0 # Complex base of Omega Plus - imaginary part
    OmRm0 = ImSqrt # Complex base of Omega Minus - real part
    OmIm0 = 0 # Complex base of Omega Minus - imaginary part

    # Set detuning Delta in angular frequency,
    # adding recoil shift since recoil is included in OBEs
    Delta = 2*pi*(p.Detuning + p.RecoilShift)
    # Transverse (x) velocity along laser beams
    vx = p.Vel[1]

	OmR1 = -397496.07535679*OmRm0
	OmI1 = -397496.07535679*OmIm0
	OmR2 = -397496.07535679*OmRp0
	OmI2 = -397496.07535679*OmIp0
	OmR3 = -397496.07535679*OmRp0
	OmI3 = -397496.07535679*OmIp0
	OmR4 = -397496.07535679*OmRm0
	OmI4 = -397496.07535679*OmIm0
	OmR5 = -397496.07535679*OmRp0
	OmI5 = -397496.07535679*OmIp0
	OmR6 = -397496.07535679*OmRm0
	OmI6 = -397496.07535679*OmIm0
	OmR7 = -397496.07535679*OmRm0
	OmI7 = -397496.07535679*OmIm0
	OmR8 = -397496.07535679*OmRp0
	OmI8 = -397496.07535679*OmIp0
	OmR9 = -397496.07535679*OmRm0
	OmI9 = -397496.07535679*OmIm0
	OmR10 = -397496.07535679*OmRp0
	OmI10 = -397496.07535679*OmIp0
	OmR11 = -397496.07535679*OmRp0
	OmI11 = -397496.07535679*OmIp0
	OmR12 = -397496.07535679*OmRm0
	OmI12 = -397496.07535679*OmIm0
	
	dydt[1] = -(OmR1*y[21]) + OmI1*y[22] - OmR2*y[23] + OmI2*y[24]
	dydt[2] = OmR3*y[29] - 2.44728622582024e7*y[2] + OmI3*y[30] + OmR7*y[31] + OmI7*y[32]
	dydt[3] = 2.44728622582024e7*y[2] + 6.5691792564229e6*y[4]
	dydt[4] = OmR5*y[41] + OmI5*y[42] + OmR9*y[43] + OmI9*y[44] - 6.5691792564229e6*y[4]
	dydt[5] = -(OmR3*y[29]) - OmI3*y[30] - OmR4*y[51] + OmI4*y[52] + 964286.732278158*y[6]
	dydt[6] = OmR1*y[21] - OmI1*y[22] + OmR11*y[57] + OmI11*y[58] - 2.54371489904806e7*y[6]
	dydt[7] = 2.35085755259243e7*y[6]
	dydt[8] = -(OmR5*y[41]) - OmI5*y[42] - OmR6*y[65] + OmI6*y[66] + 964286.732278158*y[6]
	dydt[9] = 964286.732278158*y[10] - OmR7*y[31] - OmI7*y[32] - OmR8*y[71] + OmI8*y[72]
	dydt[10] = -2.54371489904806e7*y[10] + OmR2*y[23] - OmI2*y[24] + OmR12*y[75] + OmI12*y[76]
	dydt[11] = 2.35085755259243e7*y[10]
	dydt[12] = 964286.732278158*y[10] - OmR9*y[43] - OmI9*y[44] - OmR10*y[79] + OmI10*y[80]
	dydt[13] = -(OmR11*y[57]) - OmI11*y[58]
	dydt[14] = -2.44728622582024e7*y[14] + OmR4*y[51] - OmI4*y[52]
	dydt[15] = 2.44728622582024e7*y[14] + 6.5691792564229e6*y[16]
	dydt[16] = -6.5691792564229e6*y[16] + OmR6*y[65] - OmI6*y[66]
	dydt[17] = -(OmR12*y[75]) - OmI12*y[76]
	dydt[18] = -2.44728622582024e7*y[18] + OmR8*y[71] - OmI8*y[72]
	dydt[19] = 2.44728622582024e7*y[18] + 6.5691792564229e6*y[20]
	dydt[20] = -6.5691792564229e6*y[20] + OmR10*y[79] - OmI10*y[80]
	dydt[21] = (OmR1*y[1])/2. - 1.27185744952403e7*y[21] + Delta*y[22] - BetaK*VrecSqr*y[22] + 2*BetaK*Vrec*vx*y[22] + (OmR11*y[25])/2. + (OmI11*y[26])/2. - (OmR2*y[55])/2. + (OmI2*y[56])/2. - (OmR1*y[6])/2.
	dydt[22] = -(OmI1*y[1])/2. - Delta*y[21] + BetaK*VrecSqr*y[21] - 2*BetaK*Vrec*vx*y[21] - 1.27185744952403e7*y[22] - (OmI11*y[25])/2. + (OmR11*y[26])/2. + (OmI2*y[55])/2. + (OmR2*y[56])/2. + (OmI1*y[6])/2.
	dydt[23] = -(OmR2*y[10])/2. + (OmR2*y[1])/2. - 1.27185744952403e7*y[23] + Delta*y[24] - BetaK*VrecSqr*y[24] - 2*BetaK*Vrec*vx*y[24] + (OmR12*y[27])/2. + (OmI12*y[28])/2. - (OmR1*y[55])/2. - (OmI1*y[56])/2.
	dydt[24] = (OmI2*y[10])/2. - (OmI2*y[1])/2. - Delta*y[23] + BetaK*VrecSqr*y[23] + 2*BetaK*Vrec*vx*y[23] - 1.27185744952403e7*y[24] - (OmI12*y[27])/2. + (OmR12*y[28])/2. + (OmI1*y[55])/2. - (OmR1*y[56])/2.
	dydt[25] = -(OmR11*y[21])/2. + (OmI11*y[22])/2. - 4*BetaK*VrecSqr*y[26] + 4*BetaK*Vrec*vx*y[26] - (OmR1*y[57])/2. - (OmI1*y[58])/2. - (OmR2*y[73])/2. - (OmI2*y[74])/2.
	dydt[26] = -(OmI11*y[21])/2. - (OmR11*y[22])/2. + 4*BetaK*VrecSqr*y[25] - 4*BetaK*Vrec*vx*y[25] + (OmI1*y[57])/2. - (OmR1*y[58])/2. + (OmI2*y[73])/2. - (OmR2*y[74])/2.
	dydt[27] = -(OmR12*y[23])/2. + (OmI12*y[24])/2. - 4*BetaK*VrecSqr*y[28] - 4*BetaK*Vrec*vx*y[28] - (OmR1*y[59])/2. - (OmI1*y[60])/2. - (OmR2*y[75])/2. - (OmI2*y[76])/2.
	dydt[28] = -(OmI12*y[23])/2. - (OmR12*y[24])/2. + 4*BetaK*VrecSqr*y[27] + 4*BetaK*Vrec*vx*y[27] + (OmI1*y[59])/2. - (OmR1*y[60])/2. + (OmI2*y[75])/2. - (OmR2*y[76])/2.
	dydt[29] = -1.22364311291012e7*y[29] - (OmR3*y[2])/2. - Delta*y[30] - BetaK*VrecSqr*y[30] + 2*BetaK*dpd1*VrecSqr*y[30] + 2*BetaK*Vrec*vx*y[30] - (OmR4*y[33])/2. + (OmI4*y[34])/2. + (OmR7*y[49])/2. + (OmI7*y[50])/2. + (OmR3*y[5])/2.
	dydt[30] = Delta*y[29] + BetaK*VrecSqr*y[29] - 2*BetaK*dpd1*VrecSqr*y[29] - 2*BetaK*Vrec*vx*y[29] - (OmI3*y[2])/2. - 1.22364311291012e7*y[30] - (OmI4*y[33])/2. - (OmR4*y[34])/2. + (OmI7*y[49])/2. - (OmR7*y[50])/2. + (OmI3*y[5])/2.
	dydt[31] = -(OmR7*y[2])/2. - 1.22364311291012e7*y[31] - Delta*y[32] - BetaK*VrecSqr*y[32] - 2*BetaK*dpd1*VrecSqr*y[32] - 2*BetaK*Vrec*vx*y[32] - (OmR8*y[35])/2. + (OmI8*y[36])/2. + (OmR3*y[49])/2. - (OmI3*y[50])/2. + (OmR7*y[9])/2.
	dydt[32] = -(OmI7*y[2])/2. + Delta*y[31] + BetaK*VrecSqr*y[31] + 2*BetaK*dpd1*VrecSqr*y[31] + 2*BetaK*Vrec*vx*y[31] - 1.22364311291012e7*y[32] - (OmI8*y[35])/2. - (OmR8*y[36])/2. + (OmI3*y[49])/2. + (OmR3*y[50])/2. + (OmI7*y[9])/2.
	dydt[33] = (OmR4*y[29])/2. + (OmI4*y[30])/2. - 2.44728622582024e7*y[33] - 4*BetaK*VrecSqr*y[34] + 4*BetaK*dpd1*VrecSqr*y[34] + 4*BetaK*Vrec*vx*y[34] + (OmR3*y[51])/2. - (OmI3*y[52])/2. + (OmR7*y[69])/2. - (OmI7*y[70])/2.
	dydt[34] = -(OmI4*y[29])/2. + (OmR4*y[30])/2. + 4*BetaK*VrecSqr*y[33] - 4*BetaK*dpd1*VrecSqr*y[33] - 4*BetaK*Vrec*vx*y[33] - 2.44728622582024e7*y[34] + (OmI3*y[51])/2. + (OmR3*y[52])/2. + (OmI7*y[69])/2. + (OmR7*y[70])/2.
	dydt[35] = (OmR8*y[31])/2. + (OmI8*y[32])/2. - 2.44728622582024e7*y[35] - 4*BetaK*VrecSqr*y[36] - 4*BetaK*dpd1*VrecSqr*y[36] - 4*BetaK*Vrec*vx*y[36] + (OmR3*y[53])/2. - (OmI3*y[54])/2. + (OmR7*y[71])/2. - (OmI7*y[72])/2.
	dydt[36] = -(OmI8*y[31])/2. + (OmR8*y[32])/2. + 4*BetaK*VrecSqr*y[35] + 4*BetaK*dpd1*VrecSqr*y[35] + 4*BetaK*Vrec*vx*y[35] - 2.44728622582024e7*y[36] + (OmI3*y[53])/2. + (OmR3*y[54])/2. + (OmI7*y[71])/2. + (OmR7*y[72])/2.
	dydt[37] = 2.44728622582024e7*y[33] - 4*BetaK*VrecSqr*y[38] + 4*BetaK*Vrec*vx*y[38] + 6.5691792564229e6*y[45]
	dydt[38] = 2.44728622582024e7*y[34] + 4*BetaK*VrecSqr*y[37] - 4*BetaK*Vrec*vx*y[37] + 6.5691792564229e6*y[46]
	dydt[39] = 2.44728622582024e7*y[35] - 4*BetaK*VrecSqr*y[40] - 4*BetaK*Vrec*vx*y[40] + 6.5691792564229e6*y[47]
	dydt[40] = 2.44728622582024e7*y[36] + 4*BetaK*VrecSqr*y[39] + 4*BetaK*Vrec*vx*y[39] + 6.5691792564229e6*y[48]
	dydt[41] = -3.2845896282115e6*y[41] - Delta*y[42] - BetaK*VrecSqr*y[42] + 2*BetaK*dpd2*VrecSqr*y[42] + 2*BetaK*Vrec*vx*y[42] - (OmR6*y[45])/2. + (OmI6*y[46])/2. - (OmR5*y[4])/2. + (OmR9*y[63])/2. + (OmI9*y[64])/2. + (OmR5*y[8])/2.
	dydt[42] = Delta*y[41] + BetaK*VrecSqr*y[41] - 2*BetaK*dpd2*VrecSqr*y[41] - 2*BetaK*Vrec*vx*y[41] - 3.2845896282115e6*y[42] - (OmI6*y[45])/2. - (OmR6*y[46])/2. - (OmI5*y[4])/2. + (OmI9*y[63])/2. - (OmR9*y[64])/2. + (OmI5*y[8])/2.
	dydt[43] = (OmR9*y[12])/2. - 3.2845896282115e6*y[43] - Delta*y[44] - BetaK*VrecSqr*y[44] - 2*BetaK*dpd2*VrecSqr*y[44] - 2*BetaK*Vrec*vx*y[44] - (OmR10*y[47])/2. + (OmI10*y[48])/2. - (OmR9*y[4])/2. + (OmR5*y[63])/2. - (OmI5*y[64])/2.
	dydt[44] = (OmI9*y[12])/2. + Delta*y[43] + BetaK*VrecSqr*y[43] + 2*BetaK*dpd2*VrecSqr*y[43] + 2*BetaK*Vrec*vx*y[43] - 3.2845896282115e6*y[44] - (OmI10*y[47])/2. - (OmR10*y[48])/2. - (OmI9*y[4])/2. + (OmI5*y[63])/2. + (OmR5*y[64])/2.
	dydt[45] = (OmR6*y[41])/2. + (OmI6*y[42])/2. - 6.5691792564229e6*y[45] - 4*BetaK*VrecSqr*y[46] + 4*BetaK*dpd2*VrecSqr*y[46] + 4*BetaK*Vrec*vx*y[46] + (OmR5*y[65])/2. - (OmI5*y[66])/2. + (OmR9*y[77])/2. - (OmI9*y[78])/2.
	dydt[46] = -(OmI6*y[41])/2. + (OmR6*y[42])/2. + 4*BetaK*VrecSqr*y[45] - 4*BetaK*dpd2*VrecSqr*y[45] - 4*BetaK*Vrec*vx*y[45] - 6.5691792564229e6*y[46] + (OmI5*y[65])/2. + (OmR5*y[66])/2. + (OmI9*y[77])/2. + (OmR9*y[78])/2.
	dydt[47] = (OmR10*y[43])/2. + (OmI10*y[44])/2. - 6.5691792564229e6*y[47] - 4*BetaK*VrecSqr*y[48] - 4*BetaK*dpd2*VrecSqr*y[48] - 4*BetaK*Vrec*vx*y[48] + (OmR5*y[67])/2. - (OmI5*y[68])/2. + (OmR9*y[79])/2. - (OmI9*y[80])/2.
	dydt[48] = -(OmI10*y[43])/2. + (OmR10*y[44])/2. + 4*BetaK*VrecSqr*y[47] + 4*BetaK*dpd2*VrecSqr*y[47] + 4*BetaK*Vrec*vx*y[47] - 6.5691792564229e6*y[48] + (OmI5*y[67])/2. + (OmR5*y[68])/2. + (OmI9*y[79])/2. + (OmR9*y[80])/2.
	dydt[49] = -(OmR7*y[29])/2. - (OmI7*y[30])/2. - (OmR3*y[31])/2. - (OmI3*y[32])/2. - 4*BetaK*dpd1*VrecSqr*y[50] - 4*BetaK*Vrec*vx*y[50] - (OmR8*y[53])/2. + (OmI8*y[54])/2. + 964286.732278158*y[55] - (OmR4*y[69])/2. + (OmI4*y[70])/2.
	dydt[50] = -(OmI7*y[29])/2. + (OmR7*y[30])/2. + (OmI3*y[31])/2. - (OmR3*y[32])/2. + 4*BetaK*dpd1*VrecSqr*y[49] + 4*BetaK*Vrec*vx*y[49] - (OmI8*y[53])/2. - (OmR8*y[54])/2. + 964286.732278158*y[56] + (OmI4*y[69])/2. + (OmR4*y[70])/2.
	dydt[51] = -(OmR4*y[14])/2. - (OmR3*y[33])/2. - (OmI3*y[34])/2. - 1.22364311291012e7*y[51] + Delta*y[52] - 3*BetaK*VrecSqr*y[52] + 2*BetaK*dpd1*VrecSqr*y[52] + 2*BetaK*Vrec*vx*y[52] + (OmR4*y[5])/2.
	dydt[52] = (OmI4*y[14])/2. + (OmI3*y[33])/2. - (OmR3*y[34])/2. - Delta*y[51] + 3*BetaK*VrecSqr*y[51] - 2*BetaK*dpd1*VrecSqr*y[51] - 2*BetaK*Vrec*vx*y[51] - 1.22364311291012e7*y[52] - (OmI4*y[5])/2.
	dydt[53] = -(OmR3*y[35])/2. - (OmI3*y[36])/2. + (OmR8*y[49])/2. + (OmI8*y[50])/2. - 1.22364311291012e7*y[53] + Delta*y[54] - 3*BetaK*VrecSqr*y[54] - 6*BetaK*dpd1*VrecSqr*y[54] - 6*BetaK*Vrec*vx*y[54] - (OmR4*y[83])/2. - (OmI4*y[84])/2.
	dydt[54] = (OmI3*y[35])/2. - (OmR3*y[36])/2. - (OmI8*y[49])/2. + (OmR8*y[50])/2. - Delta*y[53] + 3*BetaK*VrecSqr*y[53] + 6*BetaK*dpd1*VrecSqr*y[53] + 6*BetaK*Vrec*vx*y[53] - 1.22364311291012e7*y[54] + (OmI4*y[83])/2. - (OmR4*y[84])/2.
	dydt[55] = (OmR2*y[21])/2. - (OmI2*y[22])/2. + (OmR1*y[23])/2. - (OmI1*y[24])/2. - 2.54371489904806e7*y[55] - 4*BetaK*Vrec*vx*y[56] + (OmR12*y[59])/2. + (OmI12*y[60])/2. + (OmR11*y[73])/2. + (OmI11*y[74])/2.
	dydt[56] = -(OmI2*y[21])/2. - (OmR2*y[22])/2. + (OmI1*y[23])/2. + (OmR1*y[24])/2. + 4*BetaK*Vrec*vx*y[55] - 2.54371489904806e7*y[56] - (OmI12*y[59])/2. + (OmR12*y[60])/2. + (OmI11*y[73])/2. - (OmR11*y[74])/2.
	dydt[57] = (OmR11*y[13])/2. + (OmR1*y[25])/2. - (OmI1*y[26])/2. - 1.27185744952403e7*y[57] - Delta*y[58] - 3*BetaK*VrecSqr*y[58] + 2*BetaK*Vrec*vx*y[58] - (OmR11*y[6])/2.
	dydt[58] = (OmI11*y[13])/2. + (OmI1*y[25])/2. + (OmR1*y[26])/2. + Delta*y[57] + 3*BetaK*VrecSqr*y[57] - 2*BetaK*Vrec*vx*y[57] - 1.27185744952403e7*y[58] - (OmI11*y[6])/2.
	dydt[59] = (OmR1*y[27])/2. - (OmI1*y[28])/2. - (OmR12*y[55])/2. + (OmI12*y[56])/2. - 1.27185744952403e7*y[59] - Delta*y[60] - 3*BetaK*VrecSqr*y[60] - 6*BetaK*Vrec*vx*y[60] + (OmR11*y[81])/2. - (OmI11*y[82])/2.
	dydt[60] = (OmI1*y[27])/2. + (OmR1*y[28])/2. - (OmI12*y[55])/2. - (OmR12*y[56])/2. + Delta*y[59] + 3*BetaK*VrecSqr*y[59] + 6*BetaK*Vrec*vx*y[59] - 1.27185744952403e7*y[60] + (OmI11*y[81])/2. + (OmR11*y[82])/2.
	dydt[61] = 2.35085755259243e7*y[55] - 4*BetaK*dpd1*VrecSqr*y[62] - 4*BetaK*Vrec*vx*y[62]
	dydt[62] = 2.35085755259243e7*y[56] + 4*BetaK*dpd1*VrecSqr*y[61] + 4*BetaK*Vrec*vx*y[61]
	dydt[63] = -(OmR9*y[41])/2. - (OmI9*y[42])/2. - (OmR5*y[43])/2. - (OmI5*y[44])/2. + 964286.732278158*y[55] - 4*BetaK*dpd2*VrecSqr*y[64] - 4*BetaK*Vrec*vx*y[64] - (OmR10*y[67])/2. + (OmI10*y[68])/2. - (OmR6*y[77])/2. + (OmI6*y[78])/2.
	dydt[64] = -(OmI9*y[41])/2. + (OmR9*y[42])/2. + (OmI5*y[43])/2. - (OmR5*y[44])/2. + 964286.732278158*y[56] + 4*BetaK*dpd2*VrecSqr*y[63] + 4*BetaK*Vrec*vx*y[63] - (OmI10*y[67])/2. - (OmR10*y[68])/2. + (OmI6*y[77])/2. + (OmR6*y[78])/2.
	dydt[65] = -(OmR6*y[16])/2. - (OmR5*y[45])/2. - (OmI5*y[46])/2. - 3.2845896282115e6*y[65] + Delta*y[66] - 3*BetaK*VrecSqr*y[66] + 2*BetaK*dpd2*VrecSqr*y[66] + 2*BetaK*Vrec*vx*y[66] + (OmR6*y[8])/2.
	dydt[66] = (OmI6*y[16])/2. + (OmI5*y[45])/2. - (OmR5*y[46])/2. - Delta*y[65] + 3*BetaK*VrecSqr*y[65] - 2*BetaK*dpd2*VrecSqr*y[65] - 2*BetaK*Vrec*vx*y[65] - 3.2845896282115e6*y[66] - (OmI6*y[8])/2.
	dydt[67] = -(OmR5*y[47])/2. - (OmI5*y[48])/2. + (OmR10*y[63])/2. + (OmI10*y[64])/2. - 3.2845896282115e6*y[67] + Delta*y[68] - 3*BetaK*VrecSqr*y[68] - 6*BetaK*dpd2*VrecSqr*y[68] - 6*BetaK*Vrec*vx*y[68] - (OmR6*y[87])/2. - (OmI6*y[88])/2.
	dydt[68] = (OmI5*y[47])/2. - (OmR5*y[48])/2. - (OmI10*y[63])/2. + (OmR10*y[64])/2. - Delta*y[67] + 3*BetaK*VrecSqr*y[67] + 6*BetaK*dpd2*VrecSqr*y[67] + 6*BetaK*Vrec*vx*y[67] - 3.2845896282115e6*y[68] + (OmI6*y[87])/2. - (OmR6*y[88])/2.
	dydt[69] = -(OmR7*y[33])/2. - (OmI7*y[34])/2. + (OmR4*y[49])/2. - (OmI4*y[50])/2. - 1.22364311291012e7*y[69] + Delta*y[70] - 3*BetaK*VrecSqr*y[70] + 6*BetaK*dpd1*VrecSqr*y[70] + 6*BetaK*Vrec*vx*y[70] - (OmR8*y[83])/2. + (OmI8*y[84])/2.
	dydt[70] = (OmI7*y[33])/2. - (OmR7*y[34])/2. - (OmI4*y[49])/2. - (OmR4*y[50])/2. - Delta*y[69] + 3*BetaK*VrecSqr*y[69] - 6*BetaK*dpd1*VrecSqr*y[69] - 6*BetaK*Vrec*vx*y[69] - 1.22364311291012e7*y[70] + (OmI8*y[83])/2. + (OmR8*y[84])/2.
	dydt[71] = -(OmR8*y[18])/2. - (OmR7*y[35])/2. - (OmI7*y[36])/2. - 1.22364311291012e7*y[71] + Delta*y[72] - 3*BetaK*VrecSqr*y[72] - 2*BetaK*dpd1*VrecSqr*y[72] - 2*BetaK*Vrec*vx*y[72] + (OmR8*y[9])/2.
	dydt[72] = (OmI8*y[18])/2. + (OmI7*y[35])/2. - (OmR7*y[36])/2. - Delta*y[71] + 3*BetaK*VrecSqr*y[71] + 2*BetaK*dpd1*VrecSqr*y[71] + 2*BetaK*Vrec*vx*y[71] - 1.22364311291012e7*y[72] - (OmI8*y[9])/2.
	dydt[73] = (OmR2*y[25])/2. - (OmI2*y[26])/2. - (OmR11*y[55])/2. - (OmI11*y[56])/2. - 1.27185744952403e7*y[73] - Delta*y[74] - 3*BetaK*VrecSqr*y[74] + 6*BetaK*Vrec*vx*y[74] + (OmR12*y[81])/2. + (OmI12*y[82])/2.
	dydt[74] = (OmI2*y[25])/2. + (OmR2*y[26])/2. - (OmI11*y[55])/2. + (OmR11*y[56])/2. + Delta*y[73] + 3*BetaK*VrecSqr*y[73] - 6*BetaK*Vrec*vx*y[73] - 1.27185744952403e7*y[74] + (OmI12*y[81])/2. - (OmR12*y[82])/2.
	dydt[75] = -(OmR12*y[10])/2. + (OmR12*y[17])/2. + (OmR2*y[27])/2. - (OmI2*y[28])/2. - 1.27185744952403e7*y[75] - Delta*y[76] - 3*BetaK*VrecSqr*y[76] - 2*BetaK*Vrec*vx*y[76]
	dydt[76] = -(OmI12*y[10])/2. + (OmI12*y[17])/2. + (OmI2*y[27])/2. + (OmR2*y[28])/2. + Delta*y[75] + 3*BetaK*VrecSqr*y[75] + 2*BetaK*Vrec*vx*y[75] - 1.27185744952403e7*y[76]
	dydt[77] = -(OmR9*y[45])/2. - (OmI9*y[46])/2. + (OmR6*y[63])/2. - (OmI6*y[64])/2. - 3.2845896282115e6*y[77] + Delta*y[78] - 3*BetaK*VrecSqr*y[78] + 6*BetaK*dpd2*VrecSqr*y[78] + 6*BetaK*Vrec*vx*y[78] - (OmR10*y[87])/2. + (OmI10*y[88])/2.
	dydt[78] = (OmI9*y[45])/2. - (OmR9*y[46])/2. - (OmI6*y[63])/2. - (OmR6*y[64])/2. - Delta*y[77] + 3*BetaK*VrecSqr*y[77] - 6*BetaK*dpd2*VrecSqr*y[77] - 6*BetaK*Vrec*vx*y[77] - 3.2845896282115e6*y[78] + (OmI10*y[87])/2. + (OmR10*y[88])/2.
	dydt[79] = (OmR10*y[12])/2. - (OmR10*y[20])/2. - (OmR9*y[47])/2. - (OmI9*y[48])/2. - 3.2845896282115e6*y[79] + Delta*y[80] - 3*BetaK*VrecSqr*y[80] - 2*BetaK*dpd2*VrecSqr*y[80] - 2*BetaK*Vrec*vx*y[80]
	dydt[80] = -(OmI10*y[12])/2. + (OmI10*y[20])/2. + (OmI9*y[47])/2. - (OmR9*y[48])/2. - Delta*y[79] + 3*BetaK*VrecSqr*y[79] + 2*BetaK*dpd2*VrecSqr*y[79] + 2*BetaK*Vrec*vx*y[79] - 3.2845896282115e6*y[80]
	dydt[81] = -(OmR11*y[59])/2. - (OmI11*y[60])/2. - (OmR12*y[73])/2. - (OmI12*y[74])/2. - 8*BetaK*Vrec*vx*y[82]
	dydt[82] = (OmI11*y[59])/2. - (OmR11*y[60])/2. - (OmI12*y[73])/2. + (OmR12*y[74])/2. + 8*BetaK*Vrec*vx*y[81]
	dydt[83] = (OmR4*y[53])/2. - (OmI4*y[54])/2. + (OmR8*y[69])/2. - (OmI8*y[70])/2. - 2.44728622582024e7*y[83] - 8*BetaK*dpd1*VrecSqr*y[84] - 8*BetaK*Vrec*vx*y[84]
	dydt[84] = (OmI4*y[53])/2. + (OmR4*y[54])/2. - (OmI8*y[69])/2. - (OmR8*y[70])/2. + 8*BetaK*dpd1*VrecSqr*y[83] + 8*BetaK*Vrec*vx*y[83] - 2.44728622582024e7*y[84]
	dydt[85] = 2.44728622582024e7*y[83] - 8*BetaK*Vrec*vx*y[86] + 6.5691792564229e6*y[87]
	dydt[86] = 2.44728622582024e7*y[84] + 8*BetaK*Vrec*vx*y[85] + 6.5691792564229e6*y[88]
	dydt[87] = (OmR6*y[67])/2. - (OmI6*y[68])/2. + (OmR10*y[77])/2. - (OmI10*y[78])/2. - 6.5691792564229e6*y[87] - 8*BetaK*dpd2*VrecSqr*y[88] - 8*BetaK*Vrec*vx*y[88]
	dydt[88] = (OmI6*y[67])/2. + (OmR6*y[68])/2. - (OmI10*y[77])/2. - (OmR10*y[78])/2. + 8*BetaK*dpd2*VrecSqr*y[87] + 8*BetaK*Vrec*vx*y[87] - 6.5691792564229e6*y[88]
	dydt[89] = 1.97075185920488e7*y[10] + 1.97075185920488e7*y[14] + 1.97075185920488e7*y[16] + 1.97075185920488e7*y[18] + 1.97075185920488e7*y[20] + 1.97075185920488e7*y[2] + 1.97075185920488e7*y[4] + 1.97075185920488e7*y[6]
	dydt[90] = 1.92857346455632e6*y[10] + 1.92857346455632e6*y[6]
	dydt[91] = 1.97075185920488e7*y[10] + 1.97075185920488e7*y[6]
	dydt[92] = 1.97075185920488e7*y[14] + 1.97075185920488e7*y[16] + 1.97075185920488e7*y[18] + 1.97075185920488e7*y[20] + 1.97075185920488e7*y[2] + 1.97075185920488e7*y[4]
	dydt[93] = 1.97075185920488e7*y[14] + 1.97075185920488e7*y[18] + 1.97075185920488e7*y[2]
	dydt[94] = 1.97075185920488e7*y[16] + 1.97075185920488e7*y[20] + 1.97075185920488e7*y[4]

    return dydt

end
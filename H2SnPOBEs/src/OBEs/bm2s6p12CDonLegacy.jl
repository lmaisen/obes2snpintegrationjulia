module bm2s6p12CDonLegacy

const obe_dir = "bm2s6p12CDonLegacy"

include(Base.Filesystem.joinpath(
    @__DIR__, "import_obe_from_mathematica.jl"))

end

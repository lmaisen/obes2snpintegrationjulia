function derivs(dydt, y, p, t)

    pos = get_pos(p, t)
    intensity_sqrt = sqrt(get_intensity(p, pos))

    # Two counter-propagating beams with equal intensity
    OmR0 = intensity_sqrt * 2 * cos(p.LaserWavenumber*pos[1])
    OmI0 = 0

    # Set detuning Delta in angular frequency,
    # not adding recoil shift since recoil is not included in OBEs
    Delta = 2*pi*p.Detuning

    # dc electric field strengths (V/m)
	Ex = p.DCEFieldx
	Ey = p.DCEFieldy
	Ez = p.DCEFieldz

	OmR1 = 562144.34075965*OmR0
	OmI1 = 562144.34075965*OmI0
	
	dydt[1] = 1.92311337652438e6*y[2] - OmR1*y[4] + OmI1*y[5]
	dydt[2] = -2.44728999573143e7*y[2] + OmR1*y[4] - OmI1*y[5]
	dydt[3] = 2.25497865807899e7*y[2]
	dydt[4] = (OmR1*y[1])/2. - (OmR1*y[2])/2. - 1.22364499786571e7*y[4] + Delta*y[5]
	dydt[5] = -0.5*(OmI1*y[1]) + (OmI1*y[2])/2. - Delta*y[4] - 1.22364499786571e7*y[5]
	dydt[6] = 1.97075374416047e7*y[2]
	dydt[7] = 1.92311337652438e6*y[2]
	dydt[8] = 1.97075374416047e7*y[2]
	dydt[9] = 0

    return dydt

end
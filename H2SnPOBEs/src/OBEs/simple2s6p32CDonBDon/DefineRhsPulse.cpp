// OBE DMS (discrete momentum space) equations for 2S-6P3/2 with cross-damping terms
// Using hydrogen energies from Horbatsch2016 and fundamental constants from CODATA2018 (to convert intensity)
// Assuming two laser beams with collinear linear polarizations
// Rabi frequency: Real parts OmRp0, OmRm0 and imaginary parts OmIp0, OmIm0 for beam with positive, negative propagation direction, respectively, with dimension of intensity^(1/2) and unit (W/m^2)^(1/2)
// x represents time t (According to Numerical Recipes)
Doub OmR1 = 562144.34075965*OmR0;
Doub OmI1 = 562144.34075965*OmI0;

dydx[0] = 1.92311337652438e6*y[1] - OmR1*y[3] + OmI1*y[4];
dydx[1] = -2.44728999573143e7*y[1] + OmR1*y[3] - OmI1*y[4];
dydx[2] = 2.25497865807899e7*y[1];
dydx[3] = (OmR1*y[0])/2. - (OmR1*y[1])/2. - 1.22364499786571e7*y[3] + Delta*y[4];
dydx[4] = -0.5*(OmI1*y[0]) + (OmI1*y[1])/2. - Delta*y[3] - 1.22364499786571e7*y[4];
dydx[5] = 1.97075374416047e7*y[1];
dydx[6] = 1.92311337652438e6*y[1];
dydx[7] = 1.97075374416047e7*y[1];
dydx[8] = 0;
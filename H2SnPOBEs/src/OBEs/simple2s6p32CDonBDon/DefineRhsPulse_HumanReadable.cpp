// OBE DMS (discrete momentum space) equations for 2S-6P3/2 with cross-damping terms
// Using hydrogen energies from Horbatsch2016 and fundamental constants from CODATA2018 (to convert intensity)
// Assuming two laser beams with collinear linear polarizations
// Rabi frequency: Real parts OmRp0, OmRm0 and imaginary parts OmIp0, OmIm0 for beam with positive, negative propagation direction, respectively, with dimension of intensity^(1/2) and unit (W/m^2)^(1/2)
// x represents time t (According to Numerical Recipes)
Doub OmR1 = 562144.34075965*OmR0;
Doub OmI1 = 562144.34075965*OmI0;

Doub Pr1x1 = y[0];
Doub Pr2x2 = y[1];
Doub Pr3x3 = y[2];
Doub Pr1x2 = y[3];
Doub Pi1x2 = y[4];
Doub Ps1x1 = y[5];
Doub Ps2x2 = y[6];
Doub Ps3x3 = y[7];
Doub Ps4x4 = y[8];
Doub dPr1x1 = OmI1*Pi1x2 - OmR1*Pr1x2 + 1.92311337652438e6*Pr2x2;
Doub dPr2x2 = -(OmI1*Pi1x2) + OmR1*Pr1x2 - 2.44728999573143e7*Pr2x2;
Doub dPr3x3 = 2.25497865807899e7*Pr2x2;
Doub dPr1x2 = Delta*Pi1x2 + (OmR1*Pr1x1)/2. - 1.22364499786571e7*Pr1x2 - (OmR1*Pr2x2)/2.;
Doub dPi1x2 = -1.22364499786571e7*Pi1x2 - (OmI1*Pr1x1)/2. - Delta*Pr1x2 + (OmI1*Pr2x2)/2.;
Doub dPs1x1 = 1.97075374416047e7*Pr2x2;
Doub dPs2x2 = 1.92311337652438e6*Pr2x2;
Doub dPs3x3 = 1.97075374416047e7*Pr2x2;
Doub dPs4x4 = 0;
dydx[0] = dPr1x1;
dydx[1] = dPr2x2;
dydx[2] = dPr3x3;
dydx[3] = dPr1x2;
dydx[4] = dPi1x2;
dydx[5] = dPs1x1;
dydx[6] = dPs2x2;
dydx[7] = dPs3x3;
dydx[8] = dPs4x4;
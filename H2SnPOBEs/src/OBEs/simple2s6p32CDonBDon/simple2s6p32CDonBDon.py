# OBE DMS (discrete momentum space) equations for 2S-6P3/2 with cross-damping terms
# Using hydrogen energies from Horbatsch2016 and fundamental constants from CODATA2018 (to convert intensity)
# Assuming two laser beams with collinear linear polarizations
# Rabi frequency: Real parts OmRp0, OmRm0 and imaginary parts OmIp0, OmIm0 for beam with positive, negative propagation direction, respectively, with dimension of intensity^(1/2) and unit (W/m^2)^(1/2)
# x represents time t (According to Numerical Recipes)

import numpy as np

#OmR1 = 562144.34075965*OmR0
#OmI1 = 562144.34075965*OmI0

#cdef np.ndarray[DTYPE_t, ndim=1] dydt=np.zeros(9, dtype=DTYPE)
#dydt[0] = 1.92311337652438e6*y[1] - OmR1*y[3] + OmI1*y[4]
#dydt[1] = -2.44728999573143e7*y[1] + OmR1*y[3] - OmI1*y[4]
#dydt[2] = 2.25497865807899e7*y[1]
#dydt[3] = (OmR1*y[0])/2. - (OmR1*y[1])/2. - 1.22364499786571e7*y[3] + Delta*y[4]
#dydt[4] = -0.5*(OmI1*y[0]) + (OmI1*y[1])/2. - Delta*y[3] - 1.22364499786571e7*y[4]
#dydt[5] = 1.97075374416047e7*y[1]
#dydt[6] = 1.92311337652438e6*y[1]
#dydt[7] = 1.97075374416047e7*y[1]
#dydt[8] = 0

num_eqs = 9
num_signals = 4
num_states_to_save = 3
backdecay_on = True

# Dictionary states contains dictionary with quantum numbers and metadata on state i as states[i]
states = {}
states[1] = {'n': 2, 'P': 0, 'J': 0.5, 'F': 0., 'm': 0., 'Energy': -5.164939028111531e15, 'Momentum': '0', 'NBackdecays': 0}
states[2] = {'n': 6, 'P': 1, 'J': 1.5, 'F': 1., 'm': 0., 'Energy': -5.738751097951356e14, 'Momentum': '0', 'NBackdecays': 0}
states[3] = {'n': 1, 'P': 0, 'J': 0.5, 'F': 0., 'm': 0., 'Energy': -2.0659665722838864e16, 'Momentum': '0', 'NBackdecays': 0}

# Array rho_to_dydt contains the mapping of the time derivate of the density matrix d/dt rho[i, j] for states i, j
# to the real and imaginary part of the OBEs as d/dt rho[i, j] = rho_to_dydt[i, j, 0] + 1j*rho_to_dydt[i, j, 1]
rho_to_dydt = np.zeros((4, 4, 2))
rho_to_dydt[:] = -1
rho_to_dydt[1, 1, 0] = 0
rho_to_dydt[2, 2, 0] = 1
rho_to_dydt[3, 3, 0] = 2
rho_to_dydt[1, 2, 0] = 3
rho_to_dydt[1, 2, 1] = 4
rho_to_dydt = rho_to_dydt.astype(int)

# Description of signal equations
signal_set_id = 'OBEDMS2S6P'
signal_equations = {}
signal_equations['Lyman'] = 5
signal_equations['Balmer'] = 6
signal_equations['LymanNoBD'] = 7
signal_equations['LymanBDCorrOnly'] = 8
signal_equations['LymanBD1'] = 8
signal_equations['Lyman-epsilon'] = 5
signal_equations['Balmer-delta'] = 6
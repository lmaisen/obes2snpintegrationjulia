import JSON
using H2SnPOBEs.Helpers

info = open(Base.Filesystem.joinpath(@__DIR__, obe_dir, "info.json"), "r") do io
    JSON.parse(io)
end

parameters = open(Base.Filesystem.joinpath(@__DIR__, obe_dir, "parameters.json"), "r") do io
    JSON.parse(io)
end

states = open(Base.Filesystem.joinpath(@__DIR__, obe_dir, "states.json"), "r") do io
    JSON.parse(io)
end

signals = open(Base.Filesystem.joinpath(@__DIR__, obe_dir, "signals.json"), "r") do io
    JSON.parse(io)
end

initial_state = open(Base.Filesystem.joinpath(@__DIR__, obe_dir, "initial.json"), "r") do io
    JSON.parse(io)
end
# Making sure that data type is Float64, otherwise integration throws an exception
initial_state = convert(Array{Float64}, initial_state)

function get_initial_state(p)
    return initial_state
end

# Include OBEs
include(Base.Filesystem.joinpath(
    @__DIR__, obe_dir, "derivs.jl"))

module H2SnPOBEs

# Include module containing helper function definitions
include("Helpers.jl")
using .Helpers

# Include module containing frequency sampling definitions
include("FreqSampling.jl")
using .FreqSampling

# Include module containing functions pertaining to photon momentum
include("PhotonMomentum.jl")
using .PhotonMomentum

# Include module containing functions pertaining to simulation grids
include("Grids.jl")
import .Grids

# Include module containing functions pertaining to trajectories
include("Trajs.jl")
import .Trajs

# Include modules containing OBE definitions
module OBEs

include("OBEs/H2SnPStandingWave.jl")
include("OBEs/dms2s4p12CDonNp4BDoff.jl")
include("OBEs/dms2s4p12CDonNp4BDonNDpD1.jl")
include("OBEs/dms2s4p32CDonNp4BDoff.jl")
include("OBEs/dms2s4p32CDonNp4BDonNDpD1.jl")
include("OBEs/dms2s6p12CDoffNp4BDonNDpD1.jl")
include("OBEs/dms2s6p12CDonNp4BDoff.jl")
include("OBEs/dms2s6p12CDonNp4BDonNDpD1.jl")
include("OBEs/dms2s6p32CDonNp4BDoff.jl")
include("OBEs/dms2s6p32CDonNp4BDonNDpD1.jl")
include("OBEs/dmsHD2s6p12CDonNp2BDonNDpD1.jl")
include("OBEs/dmsHfromHD2s6p12CDonNp2BDonNDpD1.jl")
include("OBEs/dmsD2s6p12CDonNp2BDonNDpD1.jl")
include("OBEs/dmsD2s6p32CDonNp2BDonNDpD1.jl")
include("OBEs/dmsD2s6p32from12CDonNp2BDonNDpD1.jl")
include("OBEs/bm2s4p12CDon.jl")
include("OBEs/bm2s4p12CDon2002.jl")
include("OBEs/bm2s4p12CDonWS.jl")
include("OBEs/bm2s4p12CDon2002WS.jl")
include("OBEs/bm2s4p32CDon.jl")
include("OBEs/bm2s6p12CDon.jl")
include("OBEs/bm2s6p12CDonWS.jl")
include("OBEs/bm2s6p12CDonLegacy.jl")
include("OBEs/bm2s6p12CDon2002.jl")
include("OBEs/bm2s6p12CDon2002WS.jl")
include("OBEs/bm2s6p12CDonMaxl2m1Ez.jl")
include("OBEs/bm2s6p12CDonMaxl2m2Ex.jl")
include("OBEs/bm2s6p32CDon.jl")
include("OBEs/bm2s6p32CDonMaxl2m1Ez.jl")
include("OBEs/bm2s6p32CDonMaxl5m1Ez.jl")
include("OBEs/bm2s6p32CDonJ32m1Ez.jl")
include("OBEs/bm2s6p32CDonJ1232m1Ez.jl")
include("OBEs/bm2s6p32CDonJ123252m1Ez.jl")
include("OBEs/bm2s6p32CDonMaxl2m2Ex.jl")
include("OBEs/bm2s6p32CDonMaxl2m3Ex.jl")
include("OBEs/bm2s6p32CDonMaxl2m3HEn26Ex.jl")
include("OBEs/bm2s6p32CDonMaxl2m3HEn262P6SDEx.jl")
include("OBEs/bm2s6p32CDonJ32m2Ex.jl")
include("OBEs/simple2s6p12CDonBDon.jl")
include("OBEs/simple2s6p32CDonBDon.jl")

"""
Return module containing OBEs
corresponding to OBE ID obe_id
"""
function set_obe(obe_id)
    if obe_id == "H2SnPStandingWave"
        obe = H2SnPStandingWave
	elseif obe_id == "dms2s4p12CDonNp4BDoff"
		obe = dms2s4p12CDonNp4BDoff
    elseif obe_id == "dms2s4p12CDonNp4BDonNDpD1"
        obe = dms2s4p12CDonNp4BDonNDpD1
	elseif obe_id == "dms2s4p32CDonNp4BDoff"
        obe = dms2s4p32CDonNp4BDoff
    elseif obe_id == "dms2s4p32CDonNp4BDonNDpD1"
        obe = dms2s4p32CDonNp4BDonNDpD1
	elseif obe_id == "dms2s6p12CDoffNp4BDonNDpD1"
        obe = dms2s6p12CDoffNp4BDonNDpD1
	elseif obe_id == "dms2s6p12CDonNp4BDoff"
		obe = dms2s6p12CDonNp4BDoff
    elseif obe_id == "dms2s6p12CDonNp4BDonNDpD1"
        obe = dms2s6p12CDonNp4BDonNDpD1
	elseif obe_id == "dms2s6p32CDonNp4BDoff"
        obe = dms2s6p32CDonNp4BDoff
    elseif obe_id == "dms2s6p32CDonNp4BDonNDpD1"
        obe = dms2s6p32CDonNp4BDonNDpD1
	elseif obe_id == "dmsHD2s6p12CDonNp2BDonNDpD1"
        obe = dmsHD2s6p12CDonNp2BDonNDpD1
	elseif obe_id == "dmsHfromHD2s6p12CDonNp2BDonNDpD1"
        obe = dmsHfromHD2s6p12CDonNp2BDonNDpD1
	elseif obe_id == "dmsD2s6p12CDonNp2BDonNDpD1"
        obe = dmsD2s6p12CDonNp2BDonNDpD1
	elseif obe_id == "dmsD2s6p32CDonNp2BDonNDpD1"
        obe = dmsD2s6p32CDonNp2BDonNDpD1
	elseif obe_id == "dmsD2s6p32from12CDonNp2BDonNDpD1"
        obe = dmsD2s6p32from12CDonNp2BDonNDpD1
    elseif obe_id == "bm2s4p12CDon"
        obe = bm2s4p12CDon
    elseif obe_id == "bm2s4p12CDon2002"
        obe = bm2s4p12CDon2002
    elseif obe_id == "bm2s4p12CDonWS"
        obe = bm2s4p12CDonWS
    elseif obe_id == "bm2s4p12CDon2002WS"
        obe = bm2s4p12CDon2002WS
    elseif obe_id == "bm2s4p32CDon"
        obe = bm2s4p32CDon
    elseif obe_id == "bm2s6p12CDon"
        obe = bm2s6p12CDon
    elseif obe_id == "bm2s6p12CDonWS"
        obe = bm2s6p12CDonWS
    elseif obe_id == "bm2s6p12CDonLegacy"
        obe = bm2s6p12CDonLegacy
    elseif obe_id == "bm2s6p12CDon2002"
        obe = bm2s6p12CDon2002
    elseif obe_id == "bm2s6p12CDon2002WS"
        obe = bm2s6p12CDon2002WS
	elseif obe_id == "bm2s6p12CDonMaxl2m1Ez"
        obe = bm2s6p12CDonMaxl2m1Ez
	elseif obe_id == "bm2s6p12CDonMaxl2m2Ex"
        obe = bm2s6p12CDonMaxl2m2Ex
    elseif obe_id == "bm2s6p32CDon"
        obe = bm2s6p32CDon
	elseif obe_id == "bm2s6p32CDonMaxl2m1Ez"
        obe = bm2s6p32CDonMaxl2m1Ez
	elseif obe_id == "bm2s6p32CDonMaxl5m1Ez"
        obe = bm2s6p32CDonMaxl5m1Ez
	elseif obe_id == "bm2s6p32CDonJ32m1Ez"
        obe = bm2s6p32CDonJ32m1Ez
	elseif obe_id == "bm2s6p32CDonJ1232m1Ez"
        obe = bm2s6p32CDonJ1232m1Ez
	elseif obe_id == "bm2s6p32CDonJ123252m1Ez"
        obe = bm2s6p32CDonJ123252m1Ez
	elseif obe_id == "bm2s6p32CDonMaxl2m2Ex"
        obe = bm2s6p32CDonMaxl2m2Ex
	elseif obe_id == "bm2s6p32CDonMaxl2m3Ex"
        obe = bm2s6p32CDonMaxl2m3Ex
	elseif obe_id == "bm2s6p32CDonMaxl2m3HEn26Ex"
        obe = bm2s6p32CDonMaxl2m3HEn26Ex		
	elseif obe_id == "bm2s6p32CDonMaxl2m3HEn262P6SDEx"
        obe = bm2s6p32CDonMaxl2m3HEn262P6SDEx				
	elseif obe_id == "bm2s6p32CDonJ32m2Ex"
        obe = bm2s6p32CDonJ32m2Ex
	elseif obe_id == "simple2s6p12CDonBDon"
        obe = simple2s6p12CDonBDon
	elseif obe_id == "simple2s6p32CDonBDon"
        obe = simple2s6p32CDonBDon		
    else
        error("Unknown OBE with ID \"$(obe_id)\"")
    end
    return obe
end

"""
Set parameters included in OBE definition
"""
function set_obe_parameters!(obe, p)
    for (key, param) in obe.parameters
        setproperty!(p, Symbol(key), param)
    end
    return p
end

end

module Solver

using DifferentialEquations
import ODEInterfaceDiffEq

"""
Get solver method for solver defined in p.Solver
"""
function get_solver_func(p)
    if p.Solver == "radau"
        solver_func = ODEInterfaceDiffEq.radau
    elseif p.Solver == "dop853"
        solver_func = ODEInterfaceDiffEq.dop853
    elseif p.Solver == "Vern7"
        solver_func = DifferentialEquations.Vern7
    else
        error("Unknown solver \"$(p.Solver)\"")
    end
    return solver_func
end

function solve(obe, p, initial_state, tspan)
    solver_func = get_solver_func(p)
    prob = DifferentialEquations.ODEProblem(
        obe.derivs, initial_state, tspan, p)
    sol = DifferentialEquations.solve(
        prob, solver_func(), saveat=tspan[end],
        reltol=p.SolveRelTol, abstol=p.SolveAbsTol,
        dt=p.SolveInitialStepSize, maxiters=p.SolveMaxIters)
    return sol
end

end

end

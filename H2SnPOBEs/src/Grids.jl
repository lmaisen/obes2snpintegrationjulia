module Grids

import DataFrames
import CSV

export load_grid

"""
Load DataFrames containing metadata of simulation grid with UID `grid_uid` and its scans from files
in directory `grids_dir`.
While the file containing the grid metadata can in principle contain multiple rows, only the first
row is used here by convention. The metadata of all scans is loaded.
Returns a `DataFrameRow` `grids` and a `DataFrame` `dfScans`, which contain the metadata of the
simulation grid and the scans of the grid, respectively.
"""
function load_grid(grids_dir, grid_uid)

    # Load DataFrames containing metadata
    input = Base.Filesystem.joinpath(
        grids_dir, grid_uid, "Metadata", "$(grid_uid)_grids.dat")
    dfGrids = DataFrames.DataFrame(CSV.File(input))
    input = Base.Filesystem.joinpath(
        grids_dir, grid_uid, "Metadata", "$(grid_uid)_scans.dat")
    dfScans = DataFrames.DataFrame(CSV.File(input))
    # Replace missing values with default values.
    # This is necessary because NaN or an empty string is represented as missing
    # data in the CSV files.
    dfScans[!, "DeltapDecay1"] = DataFrames.coalesce.(dfScans[!, "DeltapDecay1"], NaN)
    dfScans[!, "DeltapDecay2"] = DataFrames.coalesce.(dfScans[!, "DeltapDecay2"], NaN)
    dfScans[!, "DeltapDecay3"] = DataFrames.coalesce.(dfScans[!, "DeltapDecay3"], NaN)
    dfGrids[!, "DeltapDecayAvg"] = DataFrames.coalesce.(dfGrids[!, "DeltapDecayAvg"], "")
    dfGrids[!, "TrajUID"] = DataFrames.coalesce.(dfGrids[!, "TrajUID"], "")
    # Make sure all required columns of `dfScans` exists. If they are not found in the imported
    # DataFrame, add them and fill them with NaN.
    columns = ["DCEFieldx", "DCEFieldy", "DCEFieldz"]
    for column in columns
        # Check if the column exists, and if not, add it
        if !hasproperty(dfScans, column)
            dfScans[!, column] = fill(NaN, DataFrames.nrow(dfScans))
        else
            dfScans[!, column] = DataFrames.coalesce.(dfScans[!, column], NaN)
        end
    end

    # Use first grid definition in DataFrame of grid metadata
    grid = dfGrids[1, :]

    return grid, dfScans

end

end

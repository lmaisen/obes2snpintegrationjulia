module PhotonMomentum

using FastGaussQuadrature

export get_dpd_points

"""
Normalized probability distribution for having a spontaneous photon with
angular momentum 0 ħ along the z-axis, i.e, π decay,
and a projected linear momentum ħ`k` along the x-axis.

For 2S-nP, this describes the situation where the quantization axis is chosen to
be along the z-axis and the laser propagation direction is taken be along the
x-axis. The projection of the momentum onto the propagation direction (x-axis)
of a spontaneously emitted photon with an angular momentum projection of 0 ħ
along the quantization axis is then given by this equation. The decay
corresponds to π decay and Δm = 0.

See Eq. (3.44) of
Maisenbacher, Lothar.
“Precision Spectroscopy of the 2S-nP Transitions in Atomic Hydrogen.”
PhD, Ludwig-Maximilians-Universität München, 2020.
(Note that in the published version of this thesis, there is a mistake in this
equation: the `k` is not squared; it should be squared, as below)

Equivalently, the quantization axis/z-axis can be chosen to be along the laser
propagation direction. Then, a decay with an angular momentum change of 0 ħ
perpendicular to the propagation direction, and thus along the polarization
direction, corresponds to a σ+/- decay with an angular momentum change of
+/- 1 ħ along the z-axis. The projection of the momentum of the emitted
photon onto the propagation direction is given by Mølmer et al.,
Eq. (74), and is identical to the equation given here.

See:
Mølmer, Klaus, Yvan Castin, and Jean Dalibard.
“Monte Carlo Wave-Function Method in Quantum Optics.”
Journal of the Optical Society of America B 10, no. 3 (1993): 524–38.
https://doi.org/10.1364/JOSAB.10.000524.
"""
function get_proj_photon_k_prob_pi_decay(k)

    return 3/8 * (1 .+ k.^2)

end

"""
Normalized probability distribution for having a spontaneous photon with
angular momentum +/-1 ħ along the z-axis, i.e, σ+/- decay,
and a projected linear momentum ħ`k` along the x-axis.

For 2S-nP, this describes the situation where the quantization axis is chosen to
be along the z-axis and the laser propagation direction is taken be along the
x-axis. The projection of the momentum onto the propagation direction (x-axis)
of a spontaneously emitted photon with an angular momentum projection of +/- 1 ħ
along the quantization axis is then given by this equation. The decay
corresponds to σ+/- decay and Δm = +/-1.
"""
function get_proj_photon_k_prob_sigma_decay(k)

    return 3/16 * (3 .- k.^2)

end

"""
Get points at which momentum change of n-th back decay ('DpDn') will be
sampled in order to average over the momentum change.
Each point consists of a node, i.e., the value of momentum change, and the
weight of that point.
Only of relevance to OBEs of the DMS model.

Needs the `DataFrameRow`s `grid` and `scan`, containing the metadata of the
grid and scan to be calculated, respectively,
and the dictionary `obe_info`, containing the OBE metadata
(`obe.info` for given OBE module `obe`, which in turn is loaded from 'info.json' from OBE dir).
The integer `n` defines the order, or manifold, of the back decay, i.e., the averaging is
over the n-th back decay. By default, `n` is set to 1.

Returns `(num_dpd_points, dpd_nodes, dpd_weights)`, where
`num_dpd_points` is the number of points,
`dpd_nodes` is an array of nodes,
and `dpd_weights` is an array of weights.
"""
function get_dpd_points(grid, scan, obe_info; n::Int=1)

    if grid["DeltapDecayAvg"] != ""
        # Average over back decay momentum change
        if grid["DeltapDecayAvg"] == "GQ"
            # Chose nodes and weights according to Gaussian quadrature rule
            num_dpd_points = grid["NDeltapDecayAvg"]
            dpd_nodes, dpd_weights = FastGaussQuadrature.gausslegendre(
                num_dpd_points)
        elseif grid["DeltapDecayAvg"] == "Uniform"
            # Uniform sampling/averaging
            num_dpd_points = grid["NDeltapDecayAvg"]
            dpd_nodes = collect(range(-1, 1, num_dpd_points))
            dpd_weights = repeat([1/num_dpd_points], num_dpd_points)
        else
            # Unknown averaging method
            error(
                "Unknown averaging method \"$(grid["DeltapDecayAvg"])\"" *
                " for back decay momentum change")
        end
        # Weigh with emission probability for a photon with given momentum along laser propagation
        # direction.
        # Which emission probabilty distribution to use is given by the OBE metadata.
            if obe_info["PhotonMomentumDistributions"][n] == "pi"
            dpd_weights = get_proj_photon_k_prob_pi_decay(dpd_nodes) .* dpd_weights
        elseif obe_info["PhotonMomentumDistributions"][n] == "sigma"
            dpd_weights = get_proj_photon_k_prob_sigma_decay(dpd_nodes) .* dpd_weights
        else
            error(
                "Unknown photon momentum distribution" *
                " \"$(obe_info["PhotonMomentumDistribution$n"])\"" *
                " for back decay momentum change with `n` = $n")
        end
        # Normalize weights
        dpd_weights = dpd_weights./sum(dpd_weights)
    elseif grid["DeltapDecayAvg"] == ""
        # No averaging over back decay momentum change
        # or not applicable to model
        num_dpd_points = 1
        dpd_nodes = scan["DeltapDecay$(n)"]
        dpd_weights = [1.]
    end

    return num_dpd_points, dpd_nodes, dpd_weights
end

end

module Trajs

import DataFrames
import CSV
import NPZ

export load_trajs_metadata, load_traj_set, convert_trajs_to_avp

"""
Adapted from Python method `pyha.trajs.Trajs.load_trajs_metadata`
of `pyha` package (https://gitlab.mpcdf.mpg.de/lmaisen/pyha).

Load trajectory set metadata from files with paths `metadata_filepaths` (list)
into returned DataFrame `dfTrajParams`.
"""
function load_trajs_metadata(metadata_filepaths)

	dfTrajParams = DataFrames.DataFrame()
	for filepath in metadata_filepaths
	    DataFrames.append!(
	        dfTrajParams, DataFrames.DataFrame(CSV.File(filepath)))
	end
	println("Loaded metadata for $(DataFrames.nrow(dfTrajParams)) trajectory set(s)")

	return dfTrajParams

end

"""
Adapted from Python method `pyha.trajs.Trajs.load_traj_set`
of `pyha` package (https://gitlab.mpcdf.mpg.de/lmaisen/pyha).

Load trajectory set with UID `traj_uid` (string) from disk.
Metadata for this trajectory set is given by `traj_param` (`DataFrameRow`).

Returned is a dict `traj_set`.

`traj_set` contains:
    "Positions": 2D array
        (x, y, z) position of trajectories
    "Vels": 2D array
        (vx, vy, vz) velocities of trajectories
    "2SExcProbs_IntDelaySum": 1D array
        For each experimental delay,
        a sum of the 2S excitation probabilities over the integration delay
        contained in each experimental delay
    "IoniProbs_IntDelaySum": 1D array
        For each experimental delay,
        a sum of the ionization probabilities over the integration delay
        contained in each experimental delay

Note that the keys "TrajUID", "Seed", and "Param", as opposed to the Python
implementation, are not loaded from the NPZ archive and not returned here,
as `NPZ.jl` seems to have difficulties reading non-array data
(in the case of "Seed", the data is a 0-dim array,
which is also handled differently than in Python).
"""
function load_traj_set(traj_uid, traj_param, dir_sets)

	filename_npz = Base.Filesystem.splitext(traj_param["Filename"])[1] * ".npz"
    println("Loading trajectory set NPZ archive \"$(filename_npz)\"...")
	traj_set = NPZ.npzread(
	    Base.Filesystem.joinpath(
	        dir_sets, filename_npz),
	    ["Positions", "Vels", "2SExcProbs_IntDelaySum", "IoniProbs_IntDelaySum"]
	    )
	println(
	    "Read $(size(traj_set["Positions"])[1]) trajectories"
	    * " from \"$(filename_npz)\"")

	return traj_set

end

"""
Adapted from Python method `pyha.trajs.Trajs.convert_trajs_to_avp`
of `pyha` package (https://gitlab.mpcdf.mpg.de/lmaisen/pyha).

Convert trajectory set of N trajectories in 3D (x-y-z),
given by initial positions (`x`, `y`, `z`) on the nozzle `positions` (2D array of size N x 3)
and velocities `vels` (`vx`, `vy`, `vz`) (2D array of size N x 3)
into alpha/speed/laser power (AVP) format in 2D (x, z').
The 3D trajectories can be reduced into 2D trajectories because the intensity over time
seen by atoms crossing a laser beam propagating along the x-axis with a symmetric Gaussian
beam profile along the x- and y-axis is the same independent of the y-speed, as long as the
power in the laser beam `P` is scaled accordingly.
The transformation is given by
	`vx -> vx`,
	`vz -> sqrt(vy^2+vz^2) = vz'`,
	`v -> v`
	`P -> power_scaling P = P'`,
with
	`offset = ((detection_dist-z)*vy+vz*y)/sqrt(vy^2+vz^2)`,
	`power_scaling = exp(-2 offset^2/waist_radius^2)`.
`offset` is the distances from the point of closest approach to center of 2S-nP laser beam,
and `power_scaling` is the effective peak 2S-nP laser power relative to the absolute peak power.
`v` is the speed, `detection_dist` (float) is the distance between the nozzle and the laser
crossing point, and `waist_radius` (float) is the 1/e^2 waist radius of the 2S-nP laser beam.
The AVP format then consists of the angle `alpha` between `vx` and `vz'`, the speed `v`,
and the laser beam power `P'`, with
	`alpha = arctan(vx/vz')`.
Importantly, while `vx = vx'`, `alpha` is *not* identical to the angle between `vx` and `vz` in
3D, given by `arctan(vx/vz) != arctan(vx/vz')`.
Returned here are the angle `alpha` as `alphas` (1D array of size N),
the speeds `v` as `vs` (1D array of size N),
the offsets `offset` as `offsets` (1D array of size N),
and the power scalings `power_scaling` as `powers_scaling` (1D array of size N).
"""
function convert_trajs_to_avp(positions, vels, detection_dist, waist_radius)

	vs = sqrt.(sum(vels.^2, dims=2))
	# z-velocity `vz'` in 2D
	vzd = sqrt.(sum(vels[:, 2:3].^2, dims=2))
    # angle (mrad) `alpha` from z-axis towards x-axis in 2D
    alphas = 1e3*atan.(vels[:, 1]./vzd)
	# Distance (m) from point of closest approach to center of 2S-nP laser beam
	offsets = (
		((detection_dist.-positions[:, 3]).*vels[:, 2]
		 .+vels[:, 3].*positions[:, 2])
		./sqrt.(vels[:, 2].^2 .+vels[:, 3].^2))
	# Effective peak 2S-nP laser power, relative to absolute peak power,
	# as given by offset and laser beam radius
	powers_scaling = exp.(-2 .*offsets.^2 ./waist_radius.^2)

    return alphas, vs, offsets, powers_scaling

end

end

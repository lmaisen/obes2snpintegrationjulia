module FreqSampling

import JSON
using H2SnPOBEs.Helpers

export get_freq_sampling_metadata
export get_freq_list

freq_samplings_metadata = open(Base.Filesystem.joinpath(@__DIR__, "freq_samplings.json"), "r") do io
    JSON.parse(io)
end

freq_lists = open(Base.Filesystem.joinpath(@__DIR__, "freq_lists.json"), "r") do io
    JSON.parse(io)
end

function get_freq_sampling_metadata(p)
    if !(p.CalcFreqSampling in keys(freq_samplings_metadata))
        error("Unknown frequency sampling \"$(p.CalcFreqSampling)\"")
    end
    if p.CalcFreqSampling == "Fixed"
        if p.CalcFixedFreqListID in keys(freq_lists)
            freq_sampling_metadata = freq_lists[p.CalcFixedFreqListID]
        else
            error("Unknown frequency list ID \"$(p.CalcFixedFreqListID)\"")
        end
    else
        freq_sampling_metadata = freq_samplings_metadata[p.CalcFreqSampling]
    end
    return freq_sampling_metadata
end

function get_freq_list(p)
    if p.CalcFreqListID in keys(freq_lists)
        freqs = freq_lists[p.CalcFreqListID]["FreqsUnique"]
    else
        error("Unknown frequency list ID \"$(p.CalcFreqListID)\"")
    end
    return convert(Array{Float64}, freqs)
end

end

module Helpers

using PhysicalConstants.CODATA2018: c_0, ε_0, ħ

export m_h, OBEParameters, OBEInfo
export calc_derived_params!, set_intensity!, set_velocity!
export get_pos, get_intensity, get_rabi_frequency

# Hydrogen mass (kg)
const m_h = 1.673533e-27

module Data
mutable struct OBEParameters
    FS::String
    Isotope::String
    AtomicMass::Float64
    LaserFreq::Float64
    gamma_l::Float64
    gamma_d::Float64
    gamma_s::Float64
    LaserLambda::Float64
    LaserWavenumber::Float64
    Detuning::Float64
    StartPos::Array{Float64, 1}
    V::Float64
    Vel::Array{Float64, 1}
    LaserWaistRadius::Float64
    LaserBeamProfile::String
    LaserPower::Float64
    PeakIntensity::Float64
    DipoleMoment::Float64
    RabiFreqCoeff::Float64
    beta_k::Float64
    RecoilShift::Float64
    RecoilVelocity::Float64
    DeltapDecay1::Float64
    DeltapDecay2::Float64
    AlphaOffset::Float64
    DCEFieldx::Float64
    DCEFieldy::Float64
    DCEFieldz::Float64
    CalcFreqSampling::String
    CalcFixedFreqListID::String
    CalcFreqListID::String
    Solver::String
    SolveAbsTol::Float64
    SolveRelTol::Float64
    SolveInitialStepSize::Float64
    SolveMaxIters::Int
    function OBEParameters()
        return new()
    end
end
end
OBEParameters = Data.OBEParameters

function calc_derived_params!(p)
    p.beta_k = p.AtomicMass/2/float(ħ).val
    p.LaserLambda = float(c_0).val/p.LaserFreq
    p.LaserWavenumber = 2*pi/p.LaserLambda
    p.RabiFreqCoeff = (
        2 * pi * p.DipoleMoment
        * sqrt(2/(float(c_0).val*float(ε_0).val)))
    p.RecoilShift = float(ħ).val*p.LaserWavenumber^2/(2*pi*2*p.AtomicMass)
    p.RecoilVelocity = float(ħ).val*p.LaserWavenumber/p.AtomicMass
end

function set_intensity!(p)
    p.PeakIntensity = 2*p.LaserPower/(pi*p.LaserWaistRadius^2)
end

function set_velocity!(p)
    p.Vel = Array([
        p.V*sin(p.AlphaOffset*1e-3),
        0,
        p.V*cos(p.AlphaOffset*1e-3)])
end

function get_pos(p, t)
    pos = p.StartPos + t * p.Vel
end

function get_intensity(p, pos)
    if p.LaserBeamProfile == "Square"
        if (pos[2]^2+pos[3]^2 < p.LaserWaistRadius^2)
            intensity = p.PeakIntensity/2
        else
            intensity = 0
        end
    elseif p.LaserBeamProfile == "Gaussian"
        intensity = p.PeakIntensity * exp(-2*(pos[2]^2+pos[3]^2)/p.LaserWaistRadius^2)
    else
        error("Unknown laser beam profile \"$(p.LaserBeamProfile)\"")
    end
    return intensity
end

function get_rabi_frequency(p, pos)
    rabi_freq = p.RabiFreqCoeff * sqrt(get_intensity(p, pos))
end

end

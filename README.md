# OBEs2SnPIntegrationJulia

Numerical integration of Optical Bloch equations (OBEs) for the hydrogen 2S-nP system, using Julia programming language.

This repository contains the Julia package `H2SnPOBEs` in the subdirectory `H2SnPOBEs` and scripts using this package in the subdirectory `scripts`. The latter also doubles as a Julia project (which in Julia is somewhat synonymous with environment, and a package itself is a form a of a project).

Written by Lothar Maisenbacher for the 2S-nP hydrogen spectroscopy project at MPQ.

The code has been tested with Julia 1.7.1 on Windows and with Julia 1.7.0 on the cluster 'Cobra' and 'Raven' of the [Max Planck Computing and Data Facility (MPCDF)](https://www.mpcdf.mpg.de/)

## Preparing environment

When using the Julia REPL, it is recommended to work within the environment of the project `scripts`. To activate this project in the Julia package manager (open with `]` from the REPL, exit to REPL with backspace), use from the `scripts` subdirectory:

```julia
activate .
```

The package manager will reflect that the environment has been actived by showing `(scripts)` in front of the command prompt.

To install the dependencies of the project (as given in `Project.toml`), use, again within the package manager:

```julia
instantiate
```

Note that this will install the versions of the dependencies (and their respective dependencies) as given in `Manifest.toml`.

Alternatively, instead of using the package manager, one can import the package manager and run the above commands like so:

```julia
using Pkg
Pkg.activate(".")
Pkg.instantiate()
```

### On MPCDF clusters

On the MPCDF clusters, Julia must be first be loaded into the environment. This is done with (for Julia 1.7, this might need to be adjusted for the avaiable Julia versions)

```
module load julia/1.7
```

To start Julia, simply use `julia`.

To see all available modules, including other versions of Julia, use

```
module available
```

## Package handling

### Import package

In order to import the package `H2SnPOBEs` in Julia, the parent directory of the package must be in the search path for Julia.

One way to achieve this is to first add the parent directory to the enviromental variable `LOAD_PATH` during execution of the script, and then load the package. For the scripts in the subdirectory `scripts`, this done by adding the parent directory, which is also the parent directory of the package `H2SnPOBEs`, like so:

```julia
dir_user_pkg = Base.Filesystem.joinpath(@__DIR__, "../")
if !(dir_user_pkg in LOAD_PATH)
    push!(LOAD_PATH, dir_user_pkg)
end
using H2SnPOBEs
```

###  Precompilation

Usually, Julia will precompile the package when it is imported and no up-to-date precompilation is found, e.g. when importing the package for the first time.

To manually trigger the precompilation of package `H2SnPOBEs`, import the package first, then use the Julia command:

```julia
Base.compilecache(Base.PkgId(H2SnPOBEs))
```

This is e.g. needed when package content has changed, but this change is not picked up by Julia. This has happened to me when changing a JSON file (part of the package and not part of Julia’s files inside the package) inside a package, with the changes only reflected after triggering the precompilation manually.

## Optical Bloch equations

The optical Bloch equations (OBEs) available for integration are stored in the package `H2SnPOBEs` in the directory `H2SnPOBEs/src/OBEs`. All OBEs currently included in the package have been derived with the scripts in [OBEs2SnPDerivation](https://gitlab.mpcdf.mpg.de/lmaisen/dms2s6pmc).

### Adding OBEs

To add a new set of OBEs, create a new subdirectory `<OBEID>` in this directory, where `<OBEID>` is the OBE ID to be used to refer to these OBEs. Place the derivatives to be integrated (`derivs.jl`) and the metadata (`info.json`, `initial.json`, `parameters.json`, `signals.json`, `states.json`) in this directory.

Next, we create a new Julia module by creating an empty file `<OBEID>.jl` in `H2SnPOBEs/src/OBEs`, and then adding the following code to it:

```julia
module <OBEID>

const obe_dir = "<OBEID>"

include(Base.Filesystem.joinpath(
    @__DIR__, "import_obe_from_mathematica.jl"))

end
```

Next, we need to load this newly created module in the module `H2SnPOBEs` as defined in `H2SnPOBEs/src/H2SnPOBEs.jl`. To this end, add an `include` statement in this file in the appropriate section:

```julia
# Include modules containing OBE definitions
...
include("OBEs/<OBEID>.jl")
```

We also need to make the function `set_obe` in the module `H2SnPOBEs` aware of the new OBEs. Add the following `elseif` statement at the appropriate section:

```julia
function set_obe(obe_id)
    ...
	elseif obe_id == "<OBEID>"
        obe = <OBEID>		
    else
    ...
    end
    return obe
end    
```

Finally, we may need to trigger a manual precompilation of `H2SnPOBEs`, as discussed above.

## Running scripts

### Multiple worker processes (workers)

To launch a Julia script with multiple worker processes, or worker for short, the `-p` option is used like so:

```
julia -p n <script_file>
```

where `n` is the number of additional workers. There will also be one main process, and setting using `-p 1` will start one worker process in addition to the main process. This internally loads the `Distributed` package, which handles multiprocessing.

Workers can also be started inside the script, like so:

```julia
using Distributed

addprocs(n)
m = nprocs()
```

where `n` again is the number of worker processes to be started. `nprocs()` returns the number of running processes, including the main process, and thus here `m = n + 1`.

### Defining environment

The scripts in the subdirectory `scripts` (or any other scripts using the packages installed above) must now be run in the environment of the project in `scripts`. To this end, the scripts must be executed with the option `--project=` pointing the subdirectory `scripts` (e.g. if running in that folder, `--project=.`:

```
julia --project=<path_to_project> <script_file>
```

However, any workers, independent of whether they were started using the `-p` option or internally in the script, will again use the default environment and will not respect the one set with the `--project` option. One way to set all workers to the same project as the main process is:

```julia
# Activate project/environment used by main process, as e.g. set by `--project`
# option upon launch, on all workers
# Set `project_main` to currently active project of main process
project_main = Base.active_project()
# Broadcast variable `project_main` to workers
@eval @everywhere project_main=$project_main
# Import package manager on workers
@everywhere import Pkg
# Activate project `project_main` on workers
@everywhere Pkg.activate(project_main)
```

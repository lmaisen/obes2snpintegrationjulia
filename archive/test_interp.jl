using Interpolations
using Plots

@. f(x) = log(x)

@. f(x) = 3*x^2+(1/pi^4)*log((pi-x)^2)+1

# xs = 1:0.2:5
xs = 3.13:0.01:3.16
# xs = exp.(0:0.2:log(5))
A = [f(x) for x in xs]

# linear interpolation
interp_linear = LinearInterpolation(xs, A)
# interp_linear(3) # exactly log(3)
# interp_linear(3.1) # approximately log(3.1)


# xi = 1:0.1:maximum(xs)
# xi = @. exp(0:0.05:log(5))
xi = 3.13:0.0001:3.16
h = plot(xi, f(xi))
h = plot!(xi, interp_linear(xi), color=:red, linestyle=:dot)
# h = plot(xi, f(xi)-interp_linear(xi), line=(:green))
display(h)


# cubic spline interpolation
# interp_cubic = CubicSplineInterpolation(xs, A)
# interp_cubic(3) # exactly log(3)
# interp_cubic(3.1) # approximately log(3.1)

# f(x, y) = x + y
# xs = 0:0.1:1
# ys = 0:0.1:1
#
# x = 0:5
# y = 0:5
# X = x' .* ones(length(y))
# Y = ones(length(x))' .* y
# m = f(X, Y)
#
# interp_linear2 = interpolate((x, y), m, Gridded(Linear()))
